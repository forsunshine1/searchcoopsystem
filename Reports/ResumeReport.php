<?php
  $StudentCode = $_GET['StudentCode'];

  $con = mysqli_connect("localhost","root","","searchcoopdb");
  $con->set_charset("utf8");
  $dbcon = mysqli_query($con,"SELECT * FROM vwResume
                              where StudentCode = $StudentCode
                        ");

  $row = mysqli_fetch_array($dbcon);

  $FirstName = $row['Firstname'];
  $LastName = $row['Lastname'];  
  if ($row['Gender'] == '0') {
    $Gender = 'Mr.';
  }else{
    $Gender = 'Miss';
  }
  $FullName = $Gender .' '.$FirstName .' '.$LastName;
  $Address = $row['Address'];
  $Email = $row['Email'];
  $PhoneNo = $row['Tel'];
  $GPA = $row['Gpa'];
  $Avatar = $row['Avatar'];
  $Objective = $row['Objective'];
  $Experience = $row['Experience'];
  $TechnicalSkills = $row['TechnicalSkills'];
  $Education = $row['Education']; 
  $BranchName = $row['BranchName'];
  $FacultyName = $row['FacultyName'];

  require_once('../TCPDF/tcpdf.php');
  class MYPDF extends TCPDF {

      //Page header
      public function Header() {
          $this->SetFont('angsanaupc', 'B', 16);
          
      }

       public function Footer() {
          $this->SetFont('angsanaupc', 'B', 16);
          
          $this->Cell(210, 0, 'หน้า '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, 'R');
       }

  }

  // create new PDF document
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // remove default header/footer
  $pdf->setHeaderData();
  $pdf->setFooterData();
  $pdf->setPrintHeader(false);
  $pdf->setPrintFooter(false);

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  // set margins
  $pdf->SetMargins(0, 0, 0,0);
  $pdf->SetHeaderMargin(10);
  $pdf->SetFooterMargin(15);

  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  // set some language-dependent strings (optional)
  if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }

  $pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

  // set font
  $pdf->SetFont('angsanaupc', '', 18);
  $pdf->AddPage('P', 'A4');
  $pdf->setPage(true);
  $theader = '<style type="text/css">
                img {
                  border: 1px solid #ddd;
                  border-radius: 4px;
                  padding: 5px;
                  width: 150px;
                   border: 5px solid white;
                    padding: 15px;
                    -webkit-border-image: url(border.png) 30% round; /* Safari 3.1-5 */
                    -o-border-image: url(border.png) 30% round; /* Opera 11-12.1 */
                    border-image: url(border.png) 30% round;
                }
                h1 {
                    font-size: 35pt;
                }
                h2 {
                    font-size: 30pt;
                    line-height:60%;
                    color:#FF9933;
                }
                h4 {
                  line-height: 0%;
                }
                .table-scrollable>.table>thead>tr>th{
                    text-align: center !important;
                }
              </style>';
  $theader .= '<div style="width:100%;background-color:#FF9933;"></div>';

  $theader .= '<table>';
  $theader .= '<tr style="background-color:#e5e3e3">';
  $theader .= '<td colspan="12">';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '<tr style="background-color:#e5e3e3">';
  $theader .= '<td>';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '<td colspan="2" align="center">';
  if ($Avatar != '') {
     $theader .= '<img src="../Avatar/'.$Avatar.'">';
  }
  $theader .= '</td>';
  $theader .= '<td>';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '<td colspan="8">';
  $theader .= '<h1>'.$FullName.'</h1>
              <br>
              <h4>'.$FacultyName.' '.$BranchName.'</h4>
              ';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '<tr style="background-color:#e5e3e3">';
  $theader .= '<td>';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '<td colspan="11">';
  $theader .= '<span>'.$Address.' - Mobile '.$PhoneNo.' - '.$Email.'</span>';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '<tr style="background-color:#e5e3e3">';
  $theader .= '<td colspan="12">';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '</table>';

  $theader .= '<table >';
  $theader .= '<tr style="background-color:white">';
  $theader .= '<td colspan="12">';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '<tr>';
  $theader .= '<td align="right" colspan="5">';
  $theader .= '<h2>Career Objective</h2>';
  $theader .= '</td>';
  $theader .= '<td>';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '<td colspan="6">';
  $theader .= ''.$Objective.'';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '<tr>';
  $theader .= '<td align="right" colspan="5">';
  $theader .= '<h2>Experience</h2>';
  $theader .= '</td>';
  $theader .= '<td>';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '<td colspan="6">';
  $theader .= ''.$Experience.'';
  $theader .= '</td>';
  $theader .= '</tr>';


  $theader .= '<tr style="background-color:white">';
  $theader .= '<td colspan="12">';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '</table>';

  $theader .= '<table>';
  $theader .= '<tr>';
  $theader .= '<td>';
  $theader .= '</td>';
  $theader .= '<td colspan="2">';
  $theader .= '<hr align="center" width="95%">';
  $theader .= '</td>';
  $theader .= '</tr>';
  $theader .= '</table>';

  $theader .= '<table>';

  $theader .= '<tr>';
  $theader .= '<td align="right" colspan="5">';
  $theader .= '<h2>Technical Skills</h2>';
  $theader .= '</td>';
  $theader .= '<td>';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '<td colspan="6">';
  $theader .= ''.$TechnicalSkills.'';
  $theader .= '</td>';
  $theader .= '</tr>';


  $theader .= '</table>';

  $theader .= '<table>';
  $theader .= '<tr>';
  $theader .= '<td>';
  $theader .= '</td>';
  $theader .= '<td colspan="2">';
  $theader .= '<hr align="center" width="95%">';
  $theader .= '</td>';
  $theader .= '</tr>';
  $theader .= '</table>';

  $theader .= '<table>';

  $theader .= '<tr>';
  $theader .= '<td align="right" colspan="5">';
  $theader .= '<h2>Education</h2>';
  $theader .= '</td>';
  $theader .= '<td>';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '<td colspan="6">';
  $theader .= ''.$Education.'';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '<tr style="background-color:white">';
  $theader .= '<td colspan="12">';
  $theader .= '';
  $theader .= '</td>';
  $theader .= '</tr>';

  $theader .= '</table>';
  $tfooter = '';
  // Print text using writeHTMLCell()
  $pdf->writeHTML($theader.$tfooter, true, false, false, false, '');
  // ---------------------------------------------------------
  //Close and output PDF document
  $date = date("d/m/Y");
  list($d_o,$m_o,$Y_o) = split('/',$date);
  $Y_o = $Y_o + 543;
  $pdf->Output(''.$d_o.'-'.$m_o.'-'.$Y_o.'.pdf', 'I');
  mysqli_close($con);

?>
