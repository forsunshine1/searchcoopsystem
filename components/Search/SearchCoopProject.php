<?php 
	include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';

    $limit = 10;  
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
    $start_from = ($page-1) * $limit;  

    if (isset($_GET['Search']) != '' ) {
        if (isset($_GET['keyword']) != "") {
            
            if ($_GET['keyword'] == '') {
                $keyword = "ProjectTitle IS NOT NULL";
            }else{
                $keyword = "ProjectTitle LIKE'%" . $_GET['keyword'] . "%'";
            }
        }else{
            $keyword = "ProjectTitle IS NOT NULL";
        }
        

        $sql = "SELECT *
            FROM vwseachcoopproject 
            WHERE $keyword 
            LIMIT $start_from, $limit"; 

        $result = mysqli_query($con,$sql); 
    }else{
       $sql = "SELECT *
            FROM vwseachcoopproject LIMIT $start_from, $limit"; 
        $result = mysqli_query($con,$sql); 
    }
    
?>
<style type="text/css">
	.job-post-box .fa{
		width:16px;
		text-align:center;
		color:#f60;
	}
	.search-page .search-filter>.search-label{
		margin-top: 20px
	}
	.mt-checkbox, .mt-radio{
		font-size: 12px;
	}
	.btn:not(.btn-sm):not(.btn-lg){
		line-height: 1
	}
</style>
<script type="text/javascript">
    document.title = "ค้นหา"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ค้นหา        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ค้นหา </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="search-page search-content-3">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form accept="#" >
                                            <div class="search-filter ">
                                                <div class="search-label uppercase">คำที่ต้องการค้นหา </div>
                                                <div class="input-icon right">
                                                    <i class="icon-magnifier"></i>
                                                    <input name="keyword" placeholder="คำที่ต้องการค้นหา" type="text" class="form-control"> 
                                                    <input name="Search" value="Search" type="hidden" class="form-control"> 
                                                </div>
                                                <button type="submit" class="btn green bold uppercase btn-block"><span style="font-size: 24px">ค้นหา</span></button>
                                                <div class="search-filter-divider bg-grey-steel"></div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-8" style="background-color: white">
                                        <?php  $i = 1;
                                            while ($row = mysqli_fetch_assoc($result)) {  
                                        ?>
                                        <div class="row" style="border: 1px solid #eee">
                                            <div class="col-md-12">
                                                <div class="tile-container">
                                                    <div class="col-md-12" style="padding-bottom: 1%">
                                                        <div class="col-sm job-post-box">
                                                            <div class="hidden-xs-down">                                         
                                                                <a target="_blank" href="../Home/CoopProjectDail.php?Username=<?php echo base64_encode($row['StudentCode']) ?>"><h3 style="line-height:1">โครงงานสหกิจศึกษา : <?php echo $row['ProjectTitle']; ?></h3></a>       
                                                            </div>
                                                            <div class="job-post-box company">
                                                                <i class="fa fa-user"></i> 
                                                                <?php echo $row['StudentCode'] .' - '.$row['Firstname'] .' '.$row['Lastname']?> 
                                                            </div>
                                                            <div class="job-post-box company">
                                                                <i class="fa fa-building"></i> 
                                                                <?php echo $row['EstablishName']; ?>
                                                            </div>
                                                            <div class="job-post-box company">
                                                                <i class="fa fa-calendar"></i> 
                                                               <?php echo date('d/m/Y', strtotime($row['Created']));?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                            $i++;};  
                                        ?>
                                        
                                    </div>
                                    <div class="search-pagination pagination-rounded">
                                            <?php  
                                                include '../../PHP/ConnectDB.php';
                                                if (isset($_GET['Search']) != '') {
                                                    $sql = "SELECT COUNT(Id) FROM vwseachcoopproject WHERE $keyword"; 
                                                }else{
                                                    $sql = "SELECT COUNT(Id) FROM vwseachcoopproject"; 
                                                }
                                                    if (isset($_GET['keyword']) != "") {
                                                        $keyword = "&keyword=".$_GET['keyword']."";
                                                    }else{
                                                        $keyword = "";
                                                    }
                                                
                                                $Link = "".$keyword."";
                                                $result = mysqli_query($con,$sql);  
                                                $row = mysqli_fetch_row($result); 
                                                $total_records = $row[0];  
                                                $total_pages = ceil($total_records / $limit);  
                                                $pagLink = "<ul class='pagination'>";  
                                                for ($i1=1; $i1<=$total_pages; $i1++) {  
                                                $pagLink .= "<li><a href='SearchCoopProject.php?page=".$i1."&Search=Search".$Link."'>".$i1."</a></li>";  
                                                };  
                                                echo $pagLink . "</ul>";
                                                mysqli_close($con);
                                            ?>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php 
	include '../_Master/_footer.php'; 
?>