<?php 
	include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';

    $limit = 10;  
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
    $start_from = ($page-1) * $limit;  

    if (isset($_GET['Search']) != '' ) {
        if (isset($_GET['keyword']) != "") {
            
            if ($_GET['keyword'] == '') {
                $keyword = "Name IS NOT NULL";
            }else{
                $keyword = "Name LIKE'%" . $_GET['keyword'] . "%'";
            }
        }else{
            $keyword = "Name IS NOT NULL";
        }
        if (isset($_GET['PROVINCE_ID']) != "") { 
             if ($_GET['PROVINCE_ID'] == "-1") {
                $PROVINCE_ID = "PROVINCE_ID IS NOT NULL";
            }else{
                $PROVINCE_ID = "PROVINCE_ID LIKE'%" . $_GET['PROVINCE_ID'] . "%'";
            }
        }else{
            $PROVINCE_ID = "PROVINCE_ID IS NOT NULL";
        }
        if (isset($_GET['AMPHUR_ID']) != "") {
             if ($_GET['AMPHUR_ID'] == "-1") {
                $AMPHUR_ID = "AMPHUR_ID IS NOT NULL";
            }else{
                $AMPHUR_ID = "AMPHUR_ID LIKE'%" . $_GET['AMPHUR_ID'] . "%'";
            }
           
        }else{
            $AMPHUR_ID = "AMPHUR_ID IS NOT NULL";
        }
        if (isset($_GET['DISTRICT_ID']) != "") {
            if ($_GET['DISTRICT_ID'] == "-1") {
                $DISTRICT_ID = "DISTRICT_ID IS NOT NULL";
            }else{
                $DISTRICT_ID = "DISTRICT_ID LIKE'%" . $_GET['DISTRICT_ID'] . "%'";
            }
        }else{
            $DISTRICT_ID = "DISTRICT_ID IS NOT NULL";
        }

        $sql = "SELECT *
            FROM vwsearchestablish 
            WHERE IsActive = 1 AND $keyword AND $PROVINCE_ID AND $AMPHUR_ID AND $DISTRICT_ID
            ORDER BY Created ASC LIMIT $start_from, $limit"; 

        $result = mysqli_query($con,$sql); 
    }else{
       $sql = "SELECT *
            FROM vwsearchestablish WHERE IsActive = 1    ORDER BY Created ASC LIMIT $start_from, $limit"; 
        $result = mysqli_query($con,$sql); 
    }
    
?>
<style type="text/css">
	.job-post-box .fa{
		width:16px;
		text-align:center;
		color:#f60;
	}
	.search-page .search-filter>.search-label{
		margin-top: 20px
	}
	.mt-checkbox, .mt-radio{
		font-size: 12px;
	}
	.btn:not(.btn-sm):not(.btn-lg){
		line-height: 1
	}
</style>
<script type="text/javascript">
    document.title = "ค้นหาสถานประกอบการ"
</script>
<script type="text/javascript">
	function selectProvince(provinceId) {
        if (provinceId != "-1") {
            loadData('amphur',provinceId);
            $("#amphur_dropdown").html("<option value='-1'>");
        }
    };
    function selectAmphur(amphurId) {
        if (amphurId != "-1") {
            loadData('district',amphurId);
            $("#district_dropdown").html("<option value='-1'>");
        }
    };
    function loadData(loadType,amphurId) {
        var dataString ='loadType='+loadType+'&loadId='+amphurId;
        $.ajax({
            type: "POST",
            url:'./loadData.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ค้นหาสถานประกอบการ       
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ค้นหาสถานประกอบการ </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="search-page search-content-3">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form accept="#" >
                                            <div class="search-filter ">
                                                <div class="search-label uppercase">คำที่ต้องการค้นหา </div>
                                                <div class="input-icon right">
                                                    <i class="icon-magnifier"></i>
                                                    <input name="keyword" placeholder="คำที่ต้องการค้นหา" type="text" class="form-control"> 
                                                    <input name="Search" value="Search" type="hidden" class="form-control"> 
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM provinces order by PROVINCE_ID ASC ";  
                                                    $listprovince = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="search-label uppercase">จังหวัด</div>
                                                <select onchange="selectProvince(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="PROVINCE_ID">
                                                     <option value='-1'>Select Province</option>
                                                    <?php while ($row2=mysqli_fetch_assoc($listprovince)) { ?>
                                                        <option value="<?php echo $row2['PROVINCE_ID']?>"><?php echo $row2['PROVINCE_NAME']?></option>
                                                    <?php } ?>
                                                </select>

                                                <div class="search-label uppercase">อำเภอ</div>
                                                <select onchange="selectAmphur(this.options[this.selectedIndex].value)" id="amphur_dropdown" class="form-control select2" name="AMPHUR_ID">
                                                    <option value="-1">Select Amphur</option>
                                                    <span id="amphur_loader"></span>
                                                </select>

                                                <div class="search-label uppercase">แขวง</div>
                                                <select id="district_dropdown" class="form-control select2" name="DISTRICT_ID">
                                                    <option value="-1">Select District</option>
                                                    <span id="district_loader"></span>
                                                </select>
                                                <button type="submit" class="btn green bold uppercase btn-block"><span style="font-size: 24px">ค้นหา</span></button>
                                                <div class="search-filter-divider bg-grey-steel"></div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-8" style="background-color: white">
                                        <?php  $i = 1;
                                            while ($row = mysqli_fetch_assoc($result)) {  
                                        ?>
                                        <div class="row" style="border: 1px solid #eee">
                                            <div class="col-md-12">
                                                <div class="tile-container">
                                                    <div class="col-md-4">
                                                        <a href="javascript:;">
                                                            <img src="../../Logo/<?php echo $row['Path_Logo'] ?>" width="100%" height="140px" style="margin-bottom:10%;margin-top: 10%" />
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="col-sm job-post-box">
                                                            <div class="hidden-xs-down">                                         
                                                                <a target="_blank" href="../Home/EstablishDetail.php?Id=<?php echo $row['Id'] ?>"><h3 style="line-height:1"><?php echo $row['Name']; ?></h3></a>       
                                                            </div>
                                                            <div class="job-post-box company">
                                                                <i class="fa fa-sticky-note-o"></i> 
                                                                <?php echo $row['Description'] ?> 
                                                            </div>
                                                            <div class="job-post-box location">
                                                                <i class="fa fa-map-marker"></i> 
                                                                <?php echo $row['PROVINCE_NAME'] .' '.$row['AMPHUR_NAME'].' '.$row['DISTRICT_NAME']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                            $i++;};  
                                        ?>
                                        <div class="search-pagination pagination-rounded">
                                            <?php  
                                                include '../../PHP/ConnectDB.php';
                                                if (isset($_GET['Search']) != '') {
                                                    $sql = "SELECT COUNT(Id) FROM vwsearchestablish WHERE IsActive = 1 AND $keyword AND $PROVINCE_ID AND $AMPHUR_ID AND $DISTRICT_ID "; 
                                                }else{
                                                    $sql = "SELECT COUNT(Id) FROM vwsearchestablish WHERE IsActive = 1"; 
                                                }
                                                    if (isset($_GET['keyword']) != "") {
                                                        $keyword = "&keyword=".$_GET['keyword']."";
                                                    }else{
                                                        $keyword = "";
                                                    }
                                                    if (isset($_GET['PROVINCE_ID']) != "") {
                                                        $PROVINCE_ID = "&PROVINCE_ID=" . $_GET['PROVINCE_ID'] . "";
                                                    }else{
                                                        $PROVINCE_ID = "";
                                                    }
                                                    if (isset($_GET['AMPHUR_ID']) != "") {
                                                        $AMPHUR_ID = "&AMPHUR_ID=" . $_GET['AMPHUR_ID'] . "";
                                                    }else{
                                                        $AMPHUR_ID = "";
                                                    }
                                                    if (isset($_GET['DISTRICT_ID']) != "") {
                                                        $DISTRICT_ID = "&DISTRICT_ID=" . $_GET['DISTRICT_ID'] . "";
                                                    }else{
                                                        $DISTRICT_ID = "";
                                                    }
                                                $Link = "".$keyword."".$PROVINCE_ID."".$AMPHUR_ID."".$DISTRICT_ID."";
                                                $result = mysqli_query($con,$sql);  
                                                $row = mysqli_fetch_row($result); 
                                                $total_records = $row[0];  
                                                $total_pages = ceil($total_records / $limit);  
                                                $pagLink = "<ul class='pagination'>";  
                                                for ($i1=1; $i1<=$total_pages; $i1++) {  
                                                $pagLink .= "<li><a href='SearchEstablish.php?page=".$i1."&Search=Search".$Link."'>".$i1."</a></li>";  
                                                };  
                                                echo $pagLink . "</ul>";
                                                mysqli_close($con);
                                            ?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php 
	include '../_Master/_footer.php'; 
?>