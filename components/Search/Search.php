<?php 
	include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';

    $limit = 10;  
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
    $start_from = ($page-1) * $limit;  

    if (isset($_GET['Search']) != '' ) {
        if (isset($_GET['keyword']) != "") {
            
            if ($_GET['keyword'] == '') {
                $keyword = "Name IS NOT NULL";
            }else{
                $keyword = "Name LIKE'%" . $_GET['keyword'] . "%'";
            }
        }else{
            $keyword = "Name IS NOT NULL";
        }
        if (isset($_GET['CategoryId']) != "") {
            if ($_GET['CategoryId'] == "-1") {
                $CategoryId = "CategoryId IS NOT NULL";
            }else{
                $CategoryId = "CategoryId LIKE'%" . $_GET['CategoryId'] . "%'";
            }
        }else{
            $CategoryId = "CategoryId IS NOT NULL";
        }
        if (isset($_GET['PositionId']) != "") {
            
            if ($_GET['PositionId'] == "-1") {
                $PositionId = "PositionId IS NOT NULL";
            }else{
                $PositionId = "PositionId LIKE'%" . $_GET['PositionId'] . "%'";
            }
        }else{
            $PositionId = "PositionId IS NOT NULL";
        }
        if (isset($_GET['PROVINCE_ID']) != "") { 
             if ($_GET['PROVINCE_ID'] == "-1") {
                $PROVINCE_ID = "PROVINCE_ID IS NOT NULL";
            }else{
                $PROVINCE_ID = "PROVINCE_ID LIKE'%" . $_GET['PROVINCE_ID'] . "%'";
            }
        }else{
            $PROVINCE_ID = "PROVINCE_ID IS NOT NULL";
        }
        if (isset($_GET['AMPHUR_ID']) != "") {
             if ($_GET['AMPHUR_ID'] == "-1") {
                $AMPHUR_ID = "AMPHUR_ID IS NOT NULL";
            }else{
                $AMPHUR_ID = "AMPHUR_ID LIKE'%" . $_GET['AMPHUR_ID'] . "%'";
            }
           
        }else{
            $AMPHUR_ID = "AMPHUR_ID IS NOT NULL";
        }
        if (isset($_GET['DISTRICT_ID']) != "") {
            if ($_GET['DISTRICT_ID'] == "-1") {
                $DISTRICT_ID = "DISTRICT_ID IS NOT NULL";
            }else{
                $DISTRICT_ID = "DISTRICT_ID LIKE'%" . $_GET['DISTRICT_ID'] . "%'";
            }
        }else{
            $DISTRICT_ID = "DISTRICT_ID IS NOT NULL";
        }
        // if (isset($_GET['DateFrom']) != "") {
        //     $DateFrom = "DateFrom <='" . $_GET['DateFrom'] . "'";
        // }else{
        //     $DateFrom = "DateFrom IS NOT NULL";
        // }
        // if (isset($_GET['DateTo']) != "") {
        //     $DateTo = "DateTo <= ". $_GET['DateTo'] ;
        // }else{
        //     $DateTo = "DateTo IS NOT NULL";
        // }
        if (isset($_GET['Bts_Sukumvit']) != "") {
            $Bts_Sukumvit = "Bts_Sukumvit LIKE'%" . $_GET['Bts_Sukumvit'] . "%'";
        }else{
            $Bts_Sukumvit = "Bts_Sukumvit IS NOT NULL";
        }
        if (isset($_GET['Bts_Seelom']) != "") {
            $Bts_Seelom = "Bts_Seelom LIKE'%" . $_GET['Bts_Seelom'] . "%'";
        }else{
            $Bts_Seelom = "Bts_Seelom IS NOT NULL";
        }
        if (isset($_GET['Mrt_Blue']) != "") {
            $Mrt_Blue = "Mrt_Blue LIKE'%" . $_GET['Mrt_Blue'] . "%'";
        }else{
            $Mrt_Blue = "Mrt_Blue IS NOT NULL";
        }
        if (isset($_GET['Mrt_Purple']) != "") {
            $Mrt_Purple = "Mrt_Purple LIKE'%" . $_GET['Mrt_Purple'] . "%'";
        }else{
            $Mrt_Purple = "Mrt_Purple IS NOT NULL";
        }
        if (isset($_GET['ARL']) != "") {
            $ARL = "ARL LIKE'%" . $_GET['ARL'] . "%'";
        }else{
            $ARL = "ARL IS NOT NULL";
        }
        if (isset($_GET['BRT']) != "") {
            $BRT = "BRT LIKE'%" . $_GET['BRT'] . "%'";
        }else{
            $BRT = "BRT IS NOT NULL";
        }
        if (isset($_GET['Pay']) != "") {
            $Pay = "Pay > 0";
        }else{
            $Pay = "Pay IS NOT NULL";
        }

        $sql = "SELECT *
            FROM vwsearchannounce 
            WHERE IsActive = 1 AND $keyword AND $CategoryId AND $PositionId AND $PROVINCE_ID AND $AMPHUR_ID AND $DISTRICT_ID AND $Bts_Sukumvit AND $Bts_Seelom AND $Mrt_Blue AND $Mrt_Purple AND $ARL AND $BRT AND $Pay
            ORDER BY DateFrom ASC LIMIT $start_from, $limit"; 

        $result = mysqli_query($con,$sql); 
    }else{
       $sql = "SELECT *
            FROM vwsearchannounce WHERE IsActive = 1 ORDER BY DateFrom ASC LIMIT $start_from, $limit"; 
        $result = mysqli_query($con,$sql); 
    }
    
?>
<style type="text/css">
	.job-post-box .fa{
		width:16px;
		text-align:center;
		color:#f60;
	}
	.search-page .search-filter>.search-label{
		margin-top: 20px
	}
	.mt-checkbox, .mt-radio{
		font-size: 12px;
	}
	.btn:not(.btn-sm):not(.btn-lg){
		line-height: 1
	}
</style>
<script type="text/javascript">
    document.title = "ค้นหา"
</script>
<script type="text/javascript">
	function selectProvince(provinceId) {
        if (provinceId != "-1") {
            loadData('amphur',provinceId);
            $("#amphur_dropdown").html("<option value='-1'>");
        }
    };
    function selectAmphur(amphurId) {
        if (amphurId != "-1") {
            loadData('district',amphurId);
            $("#district_dropdown").html("<option value='-1'>");
        }
    };
    function loadData(loadType,amphurId) {
        var dataString ='loadType='+loadType+'&loadId='+amphurId;
        $.ajax({
            type: "POST",
            url:'./loadData.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
    function selectCategory(CategoryId) {
        if (CategoryId != "-1") {
            loadDataPosition('position',CategoryId);
            $("#position_dropdown").html("<option value='-1'>");
        }
    };
    function loadDataPosition(loadType,CategoryId) {
        var dataString ='loadType='+loadType+'&loadId='+CategoryId;
        $.ajax({
            type: "POST",
            url:'./loadDataPosition.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ค้นหา        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ค้นหา </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="search-page search-content-3">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form accept="#" >
                                            <div class="search-filter ">
                                                <div class="search-label uppercase">คำที่ต้องการค้นหา </div>
                                                <div class="input-icon right">
                                                    <i class="icon-magnifier"></i>
                                                    <input name="keyword" placeholder="คำที่ต้องการค้นหา" type="text" class="form-control"> 
                                                    <input name="Search" value="Search" type="hidden" class="form-control"> 
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM category order by Id ASC ";  
                                                    $listcategory = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="search-label uppercase">ประเภท</div>
                                                <select onchange="selectCategory(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="CategoryId">
                                                     <option value='-1'>Select category</option>
                                                    <?php while ($row3=mysqli_fetch_assoc($listcategory)) { ?>
                                                        <option value="<?php echo $row3['Id']?>"><?php echo $row3['Name']?></option>
                                                    <?php } ?>
                                                </select>

                                                <div class="search-label uppercase">ตำแหน่ง</div>
                                                <select id="position_dropdown" class="form-control select2" name="PositionId">
                                                    <option value="-1">Select position</option>
                                                    <span id="position_loader"></span>
                                                </select>

                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM provinces order by PROVINCE_ID ASC ";  
                                                    $listprovince = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="search-label uppercase">จังหวัด</div>
                                                <select onchange="selectProvince(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="PROVINCE_ID">
                                                     <option value='-1'>Select Province</option>
                                                    <?php while ($row2=mysqli_fetch_assoc($listprovince)) { ?>
                                                        <option value="<?php echo $row2['PROVINCE_ID']?>"><?php echo $row2['PROVINCE_NAME']?></option>
                                                    <?php } ?>
                                                </select>

                                                <div class="search-label uppercase">อำเภอ</div>
                                                <select onchange="selectAmphur(this.options[this.selectedIndex].value)" id="amphur_dropdown" class="form-control select2" name="AMPHUR_ID">
                                                    <option value="-1">Select Amphur</option>
                                                    <span id="amphur_loader"></span>
                                                </select>

                                                <div class="search-label uppercase">แขวง</div>
                                                <select id="district_dropdown" class="form-control select2" name="DISTRICT_ID">
                                                    <option value="-1">Select District</option>
                                                    <span id="district_loader"></span>
                                                </select>

                                               <!--  <div class="search-label uppercase">วันที่เริ่มรับสมัคร</div>
                                                <input name="DateFrom" class="form-control" type="date" placeholder="วันที่เริ่มรับสมัคร" /> 
                                                <div class="search-label uppercase">วันที่สิ้นสุดรับสมัคร</div>
                                                <input name="DateTo" class="form-control" type="date" placeholder="วันที่สิ้นสุดรับสมัคร" />  -->
                                                <div class="search-label uppercase">การเดินทาง</div>
                                                <div class="mt-checkbox-list">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวอ่อน
                                                                <input type="checkbox" value="1" name="Bts_Sukumvit">
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีน้ำเงิน
                                                                <input type="checkbox" value="1" name="Mrt_Blue">
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-checkbox mt-checkbox-outline"> ARL
                                                                <input type="checkbox" value="1" name="ARL">
                                                                <span></span>
                                                            </label> 
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวเข้ม
                                                                <input type="checkbox" value="1" name="Bts_Seelom">
                                                                <span></span>
                                                            </label>
                                                            
                                                             <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีม่วง 
                                                                <input type="checkbox" value="1" name="Mrt_Purple">
                                                                <span></span>
                                                            </label>
                                                            
                                                            <label class="mt-checkbox mt-checkbox-outline"> BRT
                                                                <input type="checkbox" value="1" name="BRT">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="search-label uppercase">ค่าตอบแทน</div>
                                                <div class="mt-checkbox-list">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label class="mt-checkbox mt-checkbox-outline"> ค่าตอบแทน
                                                                <input type="checkbox" value="1" name="Pay">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <button type="submit" class="btn green bold uppercase btn-block"><span style="font-size: 24px">ค้นหา</span></button>
                                                <div class="search-filter-divider bg-grey-steel"></div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-8" style="background-color: white">
                                        <?php  $i = 1;
                                            while ($row = mysqli_fetch_assoc($result)) {  
                                        ?>
                                        <div class="row" style="border: 1px solid #eee">
                                            <div class="col-md-12">
                                                <div class="tile-container">
                                                    <div class="col-md-4">
                                                        <a href="javascript:;">
                                                            <img src="../../Logo/<?php echo $row['Path_Logo'] ?>" width="100%" height="140px" style="margin-bottom:10%;margin-top: 10%" />
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="col-sm job-post-box">
                                                            <div class="hidden-xs-down">                                         
                                                                <a href="../Home/AnnounceDetail.php?Announce=<?php echo $row['Id'] ?>"><h3 style="line-height:1"><?php echo $row['Name']; ?></h3></a>       
                                                            </div>
                                                            <div class="job-post-box company">
                                                                <i class="fa fa-user"></i> 
                                                                <?php echo $row['CategoryName'] .' - '.$row['PositionName']; ?> 
                                                            </div>
                                                            <div class="job-post-box company">
                                                                <i class="fa fa-building"></i> 
                                                                <?php echo $row['EstablishName']; ?>
                                                            </div>
                                                            <div class="job-post-box location">
                                                                <i class="fa fa-map-marker"></i> 
                                                                ฝึกงาน <?php echo $row['PROVINCE_NAME']; ?>
                                                            </div>
                                                            <div class="job-post-box language">
                                                                <i class="fa fa-money"></i> <?php if ($row['Pay'] > 0) {
                                                                    echo $row['Pay'];
                                                                }else{
                                                                    echo "ไม่มีค่าตอบแทน";
                                                                }; ?>           
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                            $i++;};  
                                        ?>
                                        <div class="search-pagination pagination-rounded">
                                            <?php  
                                                include '../../PHP/ConnectDB.php';
                                                if (isset($_GET['Search']) != '') {
                                                    $sql = "SELECT COUNT(Id) FROM vwsearchannounce WHERE IsActive = 1 AND $keyword AND $CategoryId AND $PositionId AND $PROVINCE_ID AND $AMPHUR_ID AND $DISTRICT_ID AND $Bts_Sukumvit AND $Bts_Seelom AND $Mrt_Blue AND $Mrt_Purple AND $ARL AND $BRT AND $Pay "; 
                                                }else{
                                                    $sql = "SELECT COUNT(Id) FROM vwsearchannounce WHERE IsActive = 1"; 
                                                }
                                                    if (isset($_GET['keyword']) != "") {
                                                        $keyword = "&keyword=".$_GET['keyword']."";
                                                    }else{
                                                        $keyword = "";
                                                    }
                                                    if (isset($_GET['CategoryId']) != "") {
                                                        $CategoryId = "&CategoryId=" . $_GET['CategoryId'] . "";
                                                    }else{
                                                        $CategoryId = "";
                                                    }
                                                    if (isset($_GET['PositionId']) != "") {
                                                        $PositionId = "&PositionId=" . $_GET['PositionId'] . "";
                                                    }else{
                                                        $PositionId = "";
                                                    }
                                                    if (isset($_GET['PROVINCE_ID']) != "") {
                                                        $PROVINCE_ID = "&PROVINCE_ID=" . $_GET['PROVINCE_ID'] . "";
                                                    }else{
                                                        $PROVINCE_ID = "";
                                                    }
                                                    if (isset($_GET['AMPHUR_ID']) != "") {
                                                        $AMPHUR_ID = "&AMPHUR_ID=" . $_GET['AMPHUR_ID'] . "";
                                                    }else{
                                                        $AMPHUR_ID = "";
                                                    }
                                                    if (isset($_GET['DISTRICT_ID']) != "") {
                                                        $DISTRICT_ID = "&DISTRICT_ID=" . $_GET['DISTRICT_ID'] . "";
                                                    }else{
                                                        $DISTRICT_ID = "";
                                                    }
                                                    // if (isset($_GET['DateFrom']) != "") {
                                                    //     $DateFrom = "DateFrom LIKE'%" . $_GET['DateFrom'] . "%'";
                                                    // }else{
                                                    //     $DateFrom = "";
                                                    // }
                                                    // if (isset($_GET['DateTo']) != "") {
                                                    //     $DateTo = "DateTo LIKE'%" . $_GET['DateTo'] . "%'";
                                                    // }else{
                                                    //     $DateTo = "";
                                                    // }
                                                    if (isset($_GET['Bts_Sukumvit']) != "") {
                                                        $Bts_Sukumvit = "&Bts_Sukumvit=" . $_GET['Bts_Sukumvit'] . "";
                                                    }else{
                                                        $Bts_Sukumvit = "";
                                                    }
                                                    if (isset($_GET['Bts_Seelom']) != "") {
                                                        $Bts_Seelom = "&Bts_Seelom=" . $_GET['Bts_Seelom'] . "";
                                                    }else{
                                                        $Bts_Seelom = "";
                                                    }
                                                    if (isset($_GET['Mrt_Blue']) != "") {
                                                        $Mrt_Blue = "&Mrt_Blue=" . $_GET['Mrt_Blue'] . "";
                                                    }else{
                                                        $Mrt_Blue = "";
                                                    }
                                                    if (isset($_GET['Mrt_Purple']) != "") {
                                                        $Mrt_Purple = "&Mrt_Purple=" . $_GET['Mrt_Purple'] . "";
                                                    }else{
                                                        $Mrt_Purple = "";
                                                    }
                                                    if (isset($_GET['ARL']) != "") {
                                                        $ARL = "&ARL=" . $_GET['ARL'] . "";
                                                    }else{
                                                        $ARL = "";
                                                    }
                                                    if (isset($_GET['BRT']) != "") {
                                                        $BRT = "&BRT=" . $_GET['BRT'] . "";
                                                    }else{
                                                        $BRT = "";
                                                    }
                                                    if (isset($_GET['Pay']) != "") {
                                                        $Pay = "&Pay=" . $_GET['Pay'] . "";
                                                    }else{
                                                        $Pay = "";
                                                    }
                                                
                                                $Link = "".$keyword."".$CategoryId."".$PositionId."".$PROVINCE_ID."".$AMPHUR_ID."".$DISTRICT_ID."".$Bts_Sukumvit."".$Bts_Seelom."".$Mrt_Blue."".$Mrt_Purple."".$ARL."".$BRT."".$Pay."";
                                                $result = mysqli_query($con,$sql);  
                                                $row = mysqli_fetch_row($result); 
                                                $total_records = $row[0];  
                                                $total_pages = ceil($total_records / $limit);  
                                                $pagLink = "<ul class='pagination'>";  
                                                for ($i1=1; $i1<=$total_pages; $i1++) {  
                                                $pagLink .= "<li><a href='Search.php?page=".$i1."&Search=Search".$Link."'>".$i1."</a></li>";  
                                                };  
                                                echo $pagLink . "</ul>";
                                                mysqli_close($con);
                                            ?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php 
	include '../_Master/_footer.php'; 
?>