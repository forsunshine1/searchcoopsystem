<?php 
	include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';

    $limit = 10;  
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
    $start_from = ($page-1) * $limit;  

    if (isset($_GET['Search']) != '' ) {
        if (isset($_GET['keyword']) != "") {
            
            if ($_GET['keyword'] == '') {
                $keyword = "Keyword IS NOT NULL";
            }else{
                $keyword = "Keyword LIKE'%" . $_GET['keyword'] . "%'";
            }
        }else{
            $keyword = "Keyword IS NOT NULL";
        }
        if (isset($_GET['FacultyId']) != "") {
            if ($_GET['FacultyId'] == "-1") {
                $FacultyId = "FacultyId IS NOT NULL";
            }else{
                $FacultyId = "FacultyId LIKE'%" . $_GET['FacultyId'] . "%'";
            }
        }else{
            $FacultyId = "FacultyId IS NOT NULL";
        }
        if (isset($_GET['BranchId']) != "") {
            
            if ($_GET['BranchId'] == "-1") {
                $BranchId = "BranchId IS NOT NULL";
            }else{
                $BranchId = "BranchId LIKE'%" . $_GET['BranchId'] . "%'";
            }
        }else{
            $BranchId = "BranchId IS NOT NULL";
        }

        $sql = "SELECT *
            FROM vwsearchstudent 
            WHERE $keyword AND $FacultyId AND $BranchId 
            ORDER BY Created DESC LIMIT $start_from, $limit"; 

        $result = mysqli_query($con,$sql); 
    }else{
       $sql = "SELECT *
            FROM vwsearchstudent ORDER BY Created DESC LIMIT $start_from, $limit"; 
        $result = mysqli_query($con,$sql); 
    }
    
?>
<style type="text/css">
	.job-post-box .fa{
		width:16px;
		text-align:center;
		color:#f60;
	}
	.search-page .search-filter>.search-label{
		margin-top: 20px
	}
	.mt-checkbox, .mt-radio{
		font-size: 12px;
	}
	.btn:not(.btn-sm):not(.btn-lg){
		line-height: 1
	}
</style>
<script type="text/javascript">
    document.title = "ค้นหา"
</script>
<script type="text/javascript">
    function selectFaculty(FacultyId) {
        if (FacultyId != "-1") {
            loadData('branch',FacultyId);
            $("#branch_dropdown").html("<option value='-1'>");
        }
    };
    function loadData(loadType,FacultyId) {
        var dataString ='loadType='+loadType+'&loadId='+FacultyId;
        $.ajax({
            type: "POST",
            url:'./loadDataPosition.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ค้นหา        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ค้นหา </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="search-page search-content-3">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <form accept="#" >
                                            <div class="search-filter ">
                                                <div class="search-label uppercase">คำที่ต้องการค้นหา </div>
                                                <div class="input-icon right">
                                                    <i class="icon-magnifier"></i>
                                                    <input name="keyword" placeholder="คำที่ต้องการค้นหา" type="text" class="form-control"> 
                                                    <input name="Search" value="Search" type="hidden" class="form-control"> 
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM faculty order by Id ASC ";  
                                                    $listcategory = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="search-label uppercase">คณะ</div>
                                                <select onchange="selectFaculty(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="FacultyId">
                                                     <option value='-1'>Select faculty</option>
                                                    <?php while ($row3=mysqli_fetch_assoc($listcategory)) { ?>
                                                        <option value="<?php echo $row3['Id']?>"><?php echo $row3['Name']?></option>
                                                    <?php } ?>
                                                </select>

                                                <div class="search-label uppercase">สาขา</div>
                                                <select id="branch_dropdown" class="form-control select2" name="BranchId">
                                                    <option value="-1">Select branch</option>
                                                    <span id="branch_loader"></span>
                                                </select>
                                                <button type="submit" class="btn green bold uppercase btn-block"><span style="font-size: 24px">ค้นหา</span></button>
                                                <div class="search-filter-divider bg-grey-steel"></div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-8" style="background-color: white">
                                        <?php  $i = 1;
                                            while ($row = mysqli_fetch_assoc($result)) {  
                                        ?>
                                        <div class="row" style="border: 1px solid #eee">
                                            <div class="col-md-12">
                                                <div class="tile-container">
                                                    <div class="col-md-4">
                                                        <a href="javascript:;">
                                                            <?php if ($row['Avatar'] == ''): ?>
                                                                <img src="../../assets/pages/img/no_image.png" width="90%" height="180px" style="margin-bottom:10%;margin-top: 10%" />
                                                            <?php endif ?>
                                                            <?php if ($row['Avatar'] != ''): ?>
                                                                <img src="../../Avartar/<?php echo $row['Avatar'] ?>" width="90%" height="180px" style="margin-bottom:10%;margin-top: 10%" />
                                                            <?php endif ?>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="col-sm job-post-box">
                                                            <div class="hidden-xs-down">                                         
                                                                <a target="_blank" href="../Account/View.php?Id=<?php echo $row['Id'] ?>"><h3 style="line-height:1"> <?php echo $row['Firstname'] .' '.$row['Lastname']; ?> </h3></a> 
                                                                <a target="_blank" style="float: right;" class="btn red" href="../../Reports/ResumeReport.php?StudentCode=<?php echo $row['Username'] ?>">เรซูเม่</a>       
                                                            </div>
                                                            <div class="job-post-box location">
                                                                <i class="fa fa-building"></i> 
                                                                <?php echo $row['FacultyName'] .' '.$row['BranchName']; ?> 
                                                            </div>
                                                            <div class="job-post-box location">
                                                                <i class="fa fa-heart-o"></i> 
                                                                <?php echo $row['Gpa'] ?> 
                                                            </div>
                                                            <div class="job-post-box location">
                                                                <i class="fa fa-phone"></i> 
                                                                <?php echo $row['Tel'] ?> 
                                                            </div>
                                                            <div class="job-post-box location">
                                                                <i class="fa fa-envelope-o"></i> 
                                                                <?php echo $row['Email'] ?> 
                                                            </div>
                                                            <div class="job-post-box location">
                                                                <i class="fa fa-comment-o"></i> 
                                                                ตำแหน่งที่สนใจ <?php echo $row['LookingDesc'] ?> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php  
                                            $i++;};  
                                        ?>
                                        <div class="search-pagination pagination-rounded">
                                            <?php  
                                                include '../../PHP/ConnectDB.php';
                                                if (isset($_GET['Search']) != '') {
                                                    $sql = "SELECT COUNT(Id) FROM vwsearchstudent WHERE $keyword AND $FacultyId AND $BranchId "; 
                                                }else{
                                                    $sql = "SELECT COUNT(Id) FROM vwsearchstudent"; 
                                                }
                                                    if (isset($_GET['keyword']) != "") {
                                                        $keyword = "&keyword=".$_GET['keyword']."";
                                                    }else{
                                                        $keyword = "";
                                                    }
                                                    if (isset($_GET['FacultyId']) != "") {
                                                        $FacultyId = "&FacultyId=" . $_GET['FacultyId'] . "";
                                                    }else{
                                                        $FacultyId = "";
                                                    }
                                                    if (isset($_GET['BranchId']) != "") {
                                                        $BranchId = "&BranchId=" . $_GET['BranchId'] . "";
                                                    }else{
                                                        $BranchId = "";
                                                    }
                                                    
                                                
                                                $Link = "".$keyword."".$FacultyId."".$BranchId."";
                                                $result = mysqli_query($con,$sql);  
                                                $row = mysqli_fetch_row($result); 
                                                $total_records = $row[0];  
                                                $total_pages = ceil($total_records / $limit);  
                                                $pagLink = "<ul class='pagination'>";  
                                                for ($i1=1; $i1<=$total_pages; $i1++) {  
                                                $pagLink .= "<li><a href='SearchStudent.php?page=".$i1."&Search=Search".$Link."'>".$i1."</a></li>";  
                                                };  
                                                echo $pagLink . "</ul>";
                                                mysqli_close($con);
                                            ?>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php 
	include '../_Master/_footer.php'; 
?>