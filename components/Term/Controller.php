<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Create':
			Create();
			break;
		case 'Delete':
			Delete();
			break;
		case 'Edit':
			Edit();
			break;	
		default:
			# code...
			break;
	}

	function Create()
	{
		include '../../PHP/ConnectDB.php';

		$YearId = $_POST['YearId'];
		$TermNo = $_POST['TermNo'];
		$StartDate = $_POST['StartDate'];
		$EndDate = $_POST['EndDate'];
		$IsActive = $_POST['IsActive'];

		if(isset($_POST['YearId']))
		{
			$valid = "SELECT * FROM term WHERE YearId = '$YearId' AND TermNo = '$TermNo'";

			$validNum = mysqli_num_rows(mysqli_query($con, $valid));
			 
			if($validNum == ''){
				$sql = "INSERT INTO term (YearId,TermNo,StartDate,EndDate,IsActive) VALUES ('$YearId','$TermNo','$StartDate','$EndDate','$IsActive')";

				mysqli_query($con, $sql);
				$response_array['status'] = 'success';  

			}else{
				
				$response_array['status'] = 'duplicate';
			}							

		}else{
		    
		    $response_array['status'] = 'error';
		} 
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$StartDate = $_POST['StartDate'];
		$EndDate = $_POST['EndDate'];
		$IsActive = $_POST['IsActive'];

		if (isset($_POST['Id'])) {
				$Id = $_POST['Id'];
				$sql = mysqli_query($con,"UPDATE term SET StartDate = '$StartDate',EndDate = '$EndDate' , IsActive = '$IsActive' 
				WHERE Id='$Id'");

				$response_array['status'] = 'success';  

		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Delete()
	{
		include '../../PHP/ConnectDB.php';
		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = "DELETE FROM term WHERE Id='$Id'";

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	exit(json_encode($response_array));
 ?>