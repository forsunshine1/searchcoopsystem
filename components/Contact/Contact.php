<?php 
    include '../_Master/_header.php';
?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    $query = mysqli_query($con,"SELECT * FROM contact WHERE Id = '1' ");
    
    // $result = mysqli_query($con,$sql); 
    $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<!-- POST  -->
<script type="text/javascript">
    document.title = "ติดต่อ"
</script>
<style type="text/css">
    .c-content-contact-1>.row .c-body>.c-section{
        text-align: left;
    }
    .c-body{
        margin-top: 0px !important;
    }
</style>
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'ส่งข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Contact.php?Id=1");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'ส่งข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }              
                }
                
            });
        
        });
    });
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ติดต่อ 
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ติดต่อ </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="c-content-contact-1 c-opt-1">
                                    <div class="row" data-auto-height=".c-height">
                                        <div class="col-lg-8 col-md-6 c-desktop">
                                            <div class="col-md-12 " style="padding-left: 0px">
                                                <div class="c-body" style="margin-right: 0px">
                                                    <div class="c-section">
                                                        <h3 >ติดต่อ</h3>
                                                        <div class="c-line-left bg-dark"></div>
                                                    </div>
                                                    <form id="addForm">
                                                        <div class="form-group">
                                                            <input type="hidden" name="func" class="form-control" value="Contact">
                                                            <input type="text" name="Name" placeholder="ชื่อ" class="form-control input-md" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="Email" placeholder="อีเมล" class="form-control input-md" required> 
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="Tel" placeholder="เบอร์โทรศัพท์" class="form-control input-md" required> 
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea rows="8" name="Message" placeholder="ข้อความ ..." class="form-control input-md" required></textarea>
                                                        </div>
                                                        <button type="submit" style="width: 100%" class="btn green">Submit</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="c-body">
                                                <div class="c-section">
                                                    <h3><?php echo $row['Title']; ?></h3>
                                                </div>
                                                <div class="c-section">
                                                    <div class="c-content-label uppercase bg-blue">ที่อยู่</div>
                                                    <p><?php echo $row['Address']; ?></p>
                                                </div>
                                                <div class="c-section">
                                                    <div class="c-content-label uppercase bg-blue">ติดต่อ</div>
                                                    <p>
                                                        <strong>อีเมล</strong> <?php echo $row['Email']; ?>
                                                        <br/>
                                                        <strong>เบอร์โทรศัพท์</strong> <?php echo $row['Tel']; ?>
                                                        <br/>
                                                        <strong>แฟ็กซ์</strong> <?php echo $row['Fax']; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>