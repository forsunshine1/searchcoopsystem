<?php 
    include '../_Master/_header.php';
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false, 
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'Save success';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Login.php")
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'Save failed';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'Duplicate';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }          
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "ลงทะเบียน"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ลงทะเบียน        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ลงทะเบียน </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">ลงทะเบียน </span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT Branch.Id ,branch.Name ,faculty.Name as facultyName FROM faculty INNER JOIN Branch ON Branch.FacultyId = faculty.Id order by FacultyName ASC ";  
                                                    $listbranch = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">คณะ/สาขา</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="BranchId">
                                                            <option value=' '>Please Select</option>
                                                            <?php while ($row2=mysqli_fetch_assoc($listbranch)) { ?>
                                                                <option value=<?php echo $row2['Id']?>><?php echo $row2['facultyName'].' '.$row2['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">รหัสนักศึกษา</label>
                                                    <div class="col-md-4">
                                                        <input type="hidden" name="func" class="form-control" value="Register">
                                                        <input type="text" name="Username" class="form-control" placeholder="รหัสนักศึกษา" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">รหัสผ่าน</label>
                                                    <div class="col-md-4">
                                                        <input type="password" name="Password" class="form-control" placeholder="รหัสผ่าน" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อ</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Firstname" class="form-control" placeholder="ชื่อ" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">นามสกุล</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Lastname" class="form-control" placeholder="นามสกุล" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">อีเมล</label>
                                                    <div class="col-md-4">
                                                        <input type="email" name="Email" class="form-control" placeholder="อีเมล">
                                                    </div>
                                                    <input type="hidden" name="Role" class="form-control" value="1">
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">บันทึก</button>
                                                        <a type="button" href="./index.php" class="btn default">กลับสู่หน้าหลัก</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>