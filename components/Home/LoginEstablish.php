<?php 
    include '../_Master/_header.php';
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#Login').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '../../PHP/Authen.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'เข้าสู่ระบบสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "../home/index.php")
                        $('#myModalAlert').modal('show');          
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'รหัสนักศึกษาหรือรหัสผ่านไม่ถูกต้อง';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                
                        //setTimeout(function() {window.location.reload(true)}, 2000);
                    }    
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "เข้าสู่ระบบ"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                เข้าสู่ระบบ        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>เข้าสู่ระบบ </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">เข้าสู่ระบบ </span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <div class="row">
                                            <div class="col-md-4 col-md-offset-4">
                                                <form class="form-horizontal" id="Login">
                                                    <div class="form-body" style="padding-top: 5%">                                           
                                                        <div class="form-group">                                               
                                                            <div class="col-md-12">
                                                                รหัสนักศึกษา
                                                                <input type="text" name="Username" id="Username" class="form-control" placeholder="รหัสนักศึกษา">
                                                            </div>
                                                        </div>                                          
                                                        <div class="form-group">                                               
                                                            <div class="col-md-12">
                                                                รหัสผ่าน
                                                                <input type="password" name="Password" class="form-control" placeholder="รหัสผ่าน">
                                                            </div>
                                                        </div> 
                                                        <div>
                                                            <div class="md-checkbox md-checkbox-inline">
                                                                <input type="checkbox" id="checkbox113" class="md-check">
                                                                <label for="checkbox113">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> Remember me </label>
                                                            </div>
                                                            <a class="control-lebel" style="float:right;padding-top:7px" data-toggle="modal" href="../ForgotPassword/ForgotPassword.php" >
                                                                Forgot Password
                                                            </a>
                                                        </div>
                                                        <hr>                                         
                                                        <div class="form-group">                                               
                                                            <div class="col-sm-12 text-center">
                                                                <input type="submit" name="SaveRegister" class="btn yellow-crusta" value="Sign In">
                                                                <a class="btn green" href="./Register.php">
                                                                    Register
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>