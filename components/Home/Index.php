<?php 
    include '../_Master/_header.php'; 
    ?>
<style>
    .carousel-control.left,.carousel-control.right{
        background:transparent;
    }
    .parallax { 
    /* The image used */
        background-image: url("../../assets/pages/img/login/Home.png");

        /* Set a specific height */
        height: 500px; 

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .parallax2 { 
    /* The image used */
        background-image: url("../../assets/pages/img/login/resume.jpg");

        /* Set a specific height */
        height: 300px; 

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;

    }
    .select2{
        width: 200px
    }
    .job-post-box .fa{
        width:16px;
        text-align:center;
        color:#f60;
    }
    .search-page .search-filter>.search-label{
        margin-top: 20px
    }
    .mt-checkbox, .mt-radio{
        font-size: 12px;
    }
    .btn:not(.btn-sm):not(.btn-lg){
        line-height: 1
    }
    .carousel-indicators{
        top:200px;
    }
    .carousel-indicators li{
        border:1px solid #ffcc03;
    }
    .carousel-indicators .active{
        background-color: #f3ff0b
    }
    .mt-element-card.mt-card-round .mt-card-item .mt-card-avatar{
        border-radius:0 !important;
    }
</style>
<script type="text/javascript">
    document.title = "หน้าหลัก"
</script>
<script type="text/javascript">
    function redirect(){
        $keyword2 = document.getElementById("keyword").value;
        $PROVINCE_ID2 = document.getElementById("PROVINCE_ID").value;
        $Link = '../Search/Search.php?Search=Search&keyword='+$keyword2+'&PROVINCE_ID='+$PROVINCE_ID2+'';
        window.location.href  = $Link;
        return false;  
    };
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content" style="padding: 0px !important">
                   <div class="search-page search-content-4">
                        <div class="parallax">
                            <div class="container" style="position: absolute;top: 50%;left: 20%;">
                                <form class="form-inline" style="padding-top: 3%;padding-bottom: 3%;text-align: center;background-color:rgba(56, 58, 57, 0.56);width: 80%">
                                    <div class="form-group mx-sm-3">
                                        <input style="width: 300px" type="text" class="form-control" id="keyword" placeholder="คำที่ต้องการค้นหา" name="keyword">
                                    </div>
                                    <?php 
                                        include '../../PHP/ConnectDB.php';

                                        $sql = "SELECT * FROM provinces";  
                                        $listfaculty = mysqli_query($con,$sql); 
                                        
                                        mysqli_close($con);
                                    ?>
                                    <div class="form-group mx-sm-3">
                                        <select style="width: 200px" id="PROVINCE_ID" class=" select2" name="PROVINCE_ID">
                                            <?php while ($row2=mysqli_fetch_assoc($listfaculty)) { ?>
                                                <option value=<?php echo $row2['PROVINCE_ID']?>><?php echo $row2['PROVINCE_NAME']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <button style="width: 100px" type="button" onclick="redirect();" class="btn green">ค้นหา</button>
                                </form>
                            </div>
                        </div>
                        <?php
    
                            include '../../PHP/ConnectDB.php';

                            $sql = "SELECT * FROM establish
                                WHERE IsActive = 1 ORDER BY RAND() LIMIT 4 ";  
                            $establish = mysqli_query($con,$sql); 

                            mysqli_close($con);
                        ?>
                        <div class="container" style="margin-top: 2%;margin-bottom: 2%;">
                            <div class="mt-element-card mt-card-round mt-element-overlay">
                                <div class="row">
                                    <?php  $i = 1;
                                        while ($row2 = mysqli_fetch_assoc($establish)) {  
                                    ?>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="mt-card-item" style="background-color: white">
                                            <div class="mt-card-avatar mt-overlay-1">
                                                <img src="../../Logo/<?php echo $row2['Path_Logo'] ?>" style="height: 120px">
                                            </div>
                                            <div class="mt-card-content">
                                                <h3 class="mt-card-name"><?php echo $row2['Name']; ?></h3>
                                                <p class="mt-card-desc font-grey-mint">รวมงานกับเรา</p>
                                                <hr>
                                                <div class="mt-card-social">
                                                    <a href="" class="btn btn-warning">งานทั้งหมด</a>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php  
                                        $i++;};  
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="container" style="margin-top: 2%;margin-bottom: 2%">    
                            <div class="col-md-8" >
                                <h3><b>ตำแหน่งงานล่าสุด</b></h3>
                                <?php
    
                                    include '../../PHP/ConnectDB.php';

                                    $limit = 5;  
                                    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
                                    $start_from = ($page-1) * $limit;  

                                    $sql = "SELECT * FROM vwsearchannounce
                                        WHERE IsActive = 1 ORDER BY DateFrom LIMIT $start_from, $limit";  
                                    $result = mysqli_query($con,$sql); 

                                    mysqli_close($con);
                                ?>
                                <?php  $i = 1;
                                    while ($row = mysqli_fetch_assoc($result)) {  
                                ?>
                                <div class="row" style="border: 1px solid #eee;background-color: white;">
                                    <div class="col-md-12">
                                        <div class="tile-container">
                                            <div class="col-md-4">
                                                <a href="javascript:;">
                                                    <img src="../../Logo/<?php echo $row['Path_Logo'] ?>" width="100%" height="140px" style="margin-bottom:10%;margin-top: 10%" />
                                                </a>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="col-sm job-post-box">
                                                    <div class="hidden-xs-down">                                         
                                                        <a href="../Home/AnnounceDetail.php?Announce=<?php echo $row['Id'] ?>"><h3 style="line-height:1"><?php echo $row['Name']; ?></h3></a>       
                                                    </div>
                                                    <div class="job-post-box company">
                                                        <i class="fa fa-user"></i> 
                                                        <?php echo $row['CategoryName'] .' - '.$row['PositionName']; ?> 
                                                    </div>
                                                    <div class="job-post-box company">
                                                        <i class="fa fa-building"></i> 
                                                        <?php echo $row['EstablishName']; ?>
                                                    </div>
                                                    <div class="job-post-box location">
                                                        <i class="fa fa-map-marker"></i> 
                                                        ฝึกงาน <?php echo $row['PROVINCE_NAME']; ?>
                                                    </div>
                                                    <div class="job-post-box language">
                                                        <i class="fa fa-money"></i> <?php if ($row['Pay'] > 0) {
                                                            echo $row['Pay'];
                                                        }else{
                                                            echo "ไม่มีค่าตอบแทน";
                                                        }; ?>           
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php  
                                    $i++;};  
                                ?>
                                <div class="search-pagination pagination-rounded">
                                    <?php  
                                        include '../../PHP/ConnectDB.php';
                                        $sql = "SELECT COUNT(Id) FROM vwsearchannounce WHERE IsActive = 1";  
                                        $result = mysqli_query($con,$sql);  
                                        $row = mysqli_fetch_row($result); 
                                        $total_records = $row[0];  
                                        $total_pages = ceil($total_records / $limit);  
                                        $pagLink = "<ul class='pagination'>";  
                                        for ($i1=1; $i1<=$total_pages; $i1++) {  
                                        $pagLink .= "<li><a href='index.php?page=".$i1."'>".$i1."</a></li>";  
                                        };  
                                        echo $pagLink . "</ul>";  
                                        mysqli_close($con);
                                    ?>
                                </div>
                            </div>     
                            <div class="col-md-3 col-md-offset-1" style="height: 300px;">
                                <h3><b>ตำแหน่งงานล่าสุด</b></h3>
                            </div>                            
                        </div>
                        <div class="clearfix"></div>
                        <div class="parallax2">
                            <div style="padding-top: 100px;text-align: center">
                                <h2 style="background: white"><span style="color: #ffcc03">Create</span> Resume</h2>
                                <a href="../ResumePage/ResumePage.php" class="btn btn-lg yellow" style="background-color:#ffcc03;border-color: #ffcc03">RESUME</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>