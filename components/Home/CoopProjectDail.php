<?php 
    include '../_Master/_header.php';
?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Username'])) {
        $StudentCode = base64_decode($_GET['Username']);
        $query = mysqli_query($con,"SELECT * FROM vwseachcoopproject WHERE StudentCode = '$StudentCode' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
    }
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<!-- POST  -->
<script type="text/javascript">
    document.title = "ข้อมูลโครงงานสหกิจศึกษา"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ข้อมูลโครงงานสหกิจศึกษา 
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ข้อมูลโครงงานสหกิจศึกษา </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <h4>โครงงานสหกิจศึกษา : <?php echo $row['ProjectTitle']; ?> - <?php echo $row['EstablishName']; ?> - ชื่อผู้วิจัย <?php echo $row['Firstname'] .' '.$row['Lastname']; ?></h4>   
                                    <br>               
                                    <table class="table table-striped" >  
                                        <tbody>
                                            <tr>  
                                                <td width="300px">ปก</td>  
                                                <td>
                                                    <?php if ($row['01_cov'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['01_cov']; ?>/edit">01_cov</a>
                                                    <?php endif ?>
                                                    <?php if ($row['01_cov'] == ''): ?>
                                                        <span>01_cov</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>ใบอนุมัติลายเซ็นต์คณะกรรมการสอบ </td>  
                                                <td>
                                                    <?php if ($row['02_app'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['01_cov']; ?>/edit">02_app</a>
                                                    <?php endif ?>
                                                    <?php if ($row['02_app'] == ''): ?>
                                                        <span>01_cov</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>บทคัดย่อ</td>  
                                                <td>
                                                    <?php if ($row['03_absen'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['03_absen']; ?>/edit">03_absen</a>
                                                    <?php endif ?>
                                                    <?php if ($row['03_absen'] == ''): ?>
                                                        <span>01_cov</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>กิตติกรรมประกาศ</td>  
                                                <td>
                                                    <?php if ($row['04_ack'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['01_cov']; ?>/edit">04_ack</a>
                                                    <?php endif ?>
                                                    <?php if ($row['04_ack'] == ''): ?>
                                                        <span>04_ack</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>สารบัญ</td>  
                                                <td>
                                                    <?php if ($row['05_tbc'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['05_tbc']; ?>/edit">05_tbc</a>
                                                    <?php endif ?>
                                                    <?php if ($row['05_tbc'] == ''): ?>
                                                        <span>05_tbc</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>บทที่ 1</td>  
                                                <td>
                                                    <?php if ($row['06_ch1'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['06_ch1']; ?>/edit">06_ch1</a>
                                                    <?php endif ?>
                                                    <?php if ($row['06_ch1'] == ''): ?>
                                                        <span>06_ch1</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>บทที่ 2</td>  
                                                <td>
                                                    <?php if ($row['07_ch2'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['07_ch2']; ?>/edit">07_ch2</a>
                                                    <?php endif ?>
                                                    <?php if ($row['07_ch2'] == ''): ?>
                                                        <span>07_ch2</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>บทที่ 3</td>  
                                                <td>
                                                    <?php if ($row['08_ch3'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['08_ch3']; ?>/edit">08_ch3</a>
                                                    <?php endif ?>
                                                    <?php if ($row['08_ch3'] == ''): ?>
                                                        <span>08_ch3</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>บทที่ 4</td>  
                                                <td>
                                                    <?php if ($row['09_ch4'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['09_ch4']; ?>/edit">09_ch4</a>
                                                    <?php endif ?>
                                                    <?php if ($row['09_ch4'] == ''): ?>
                                                        <span>09_ch4</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>บทที่ 5</td>  
                                                <td>
                                                    <?php if ($row['10_ch5'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['10_ch5']; ?>/edit">10_ch5</a>
                                                    <?php endif ?>
                                                    <?php if ($row['10_ch5'] == ''): ?>
                                                        <span>10_ch5</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>บรรณานุกรม</td>  
                                                <td>
                                                    <?php if ($row['11_ref'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['11_ref']; ?>/edit">11_ref</a>
                                                    <?php endif ?>
                                                    <?php if ($row['11_ref'] == ''): ?>
                                                        <span>11_ref</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>ภาคผนวก</td>  
                                                <td>
                                                    <?php if ($row['12_app'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['12_app']; ?>/edit">12_app</a>
                                                    <?php endif ?>
                                                    <?php if ($row['12_app'] == ''): ?>
                                                        <span>12_app</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                            <tr>  
                                                <td>ประวัติผู้เขียน</td>  
                                                <td>
                                                    <?php if ($row['13_vit'] != ''): ?>
                                                        <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['13_vit']; ?>/edit">13_vit</a>
                                                    <?php endif ?>
                                                    <?php if ($row['13_vit'] == ''): ?>
                                                        <span>13_vit</span>
                                                    <?php endif ?>
                                                </td>                                                           
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="share"></div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>