<?php include '../_Master/_header.php'; ?>
<?php 
    include '../../PHP/ConnectDB.php';

    $sql = "SELECT *
        FROM vwsearchannounce 
        WHERE Id = ".$_GET['Announce']."";  
    $result = mysqli_query($con,$sql); 
    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);

    mysqli_close($con);

?>
<style type="text/css">
    .job-post-box .fa{
        width:16px;
        text-align:center;
        color:#f60;
    }
</style>
<script type="text/javascript">
    document.title = "<?php echo $row['Announce']; ?>";
</script>
<script type="text/javascript">
    function print_specific_div_content(){
        var win = window.open('','','left=0,top=0,width=1280,height=766,toolbar=0,scrollbars=0,status =0');

        var content = "<html>";
        content += "<body onload=\"window.print(); window.close();\">";
        content += document.getElementById("divToPrint").innerHTML ;
        content += "</body>";
        content += "</html>";
        win.document.write(content);
        win.document.close();
    }
    $(function () {
        $('#favoriteUpdate').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'yes'){
                        $('#btnFavorite').attr('class', 'btn purple');
                    }else if(response.status == 'no'){
                        $('#btnFavorite').attr('class', 'btn default');                           
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'Save failed';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }           
                }
                
            });
        
        });
    });
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="container">
                    <div class="page-content">
                        <div class="" style="height: 200px;background: url(../../assets/pages/img/login/wall.jpg);background-size:100%">
                            <div class="col-md-12" style="">
                                <h3 style="font-weight: bold"><?php echo $row['Name']; ?></h3>
                                <br>
                                <span style="line-height:40px">
                                     ฝึกงาน <?php echo $row['PROVINCE_NAME']; ?>  <?php echo $row['CategoryName']; ?> : <?php echo $row['PositionName']; ?>
                                </span>
                                <br>
                                <span style="line-height:40px">
                                    ฝึกงาน <?php if($row['Pay'] > 0){ echo "ค่าตอบแทน";} ?>  <span>
                                        <a class="btn btn-xs green" href="javascript:void(0)" onclick="print_specific_div_content();"><i class="fa fa-print"></i> Print</a>
                                        </span>
                                </span>
                            </div>
                        </div>
                        <div class="" style="margin-top: 2%;margin-bottom: 2%">    
                            <div class="col-md-8" style="background-color:white">
                                <div class="col-md-12" style="padding-top: 2%">
                                    <form id="favoriteUpdate">
                                           
                                        <input type="hidden" name="AnnounceId" value="<?php echo $row['Id']?>">
                                        <input type="hidden" name="StudentId" value="<?php echo base64_decode($Id) ?>">
                                        <input type="hidden" name="func" value="favoriteUpdate">
                                        <?php if (isset($Id)): ?>
                                            <?php 
                                                include '../../PHP/ConnectDB.php';

                                                $sqlFavorite = "SELECT * FROM favorite WHERE AnnounceId = ".$row['Id']." AND StudentId = ".base64_decode($Id)."";  
                                                $validNumFavorite = mysqli_num_rows(mysqli_query($con, $sqlFavorite));
                                                mysqli_close($con);

                                            ?>
                                            <?php if ($validNumFavorite == ''): ?>
                                                <button id="btnFavorite" style="float: right;" type="submit" class="btn default"><i class="fa fa-heart-o"></i>Favorite</button>
                                            <?php endif ?>
                                            <?php if ($validNumFavorite != ''): ?>
                                                <button id="btnFavorite" style="float: right;" type="submit" class="btn purple"><i class="fa fa-heart-o"></i>Favorite</button>
                                            <?php endif ?>                 
                                        <?php endif ?>            
                                    </form>
                                </div>
                                <div id="divToPrint" style="padding-bottom: 2%">
                                    <h4 style="font-weight: bold">
                                        <?php echo $row['EstablishName'].' - '.$row['Name']; ?>
                                        
                                    </h4>
                                    <br>
                                    <div style="text-align: center">
                                        <img height="auto" width="40%" src="../../Logo/<?php echo $row['Path_Logo'] ?>">
                                    </div>
                                    <br>
                                    <h4 style="font-weight: bold">ตำแหน่ง</h4>
                                    <span>
                                        <?php echo $row['PositionName']; ?> จำนวน <b><?php echo $row['Unit']; ?></b> อัตรา 
                                    </span>
                                    <hr> 
                                   <!--  <h4 style="font-weight: bold">จำนวน</h4>
                                    <span>
                                        2 อัตรา
                                    </span> -->
                                    <h4 style="font-weight: bold">รายละเอียดงาน</h4>
                                    <span>
                                        <?php echo $row['Description']; ?>
                                    </span>
                                    <hr>  
                                     <h4 style="font-weight: bold">ทักษะ</h4>
                                    <span>
                                        <?php echo $row['Skill']; ?>
                                    </span>
                                    <hr>    
                                    <h4 style="font-weight: bold">ช่วงเวลาเปิดรับสมัคร</h4>
                                    <span>
                                        เปิดรับระหว่างวันที่ <?php echo date('d/m/y', strtotime($row['DateFrom'])); ?> - <?php echo date('d/m/y', strtotime($row['DateTo'])); ?>
                                    </span>
                                    <hr>    
                                    <h4 style="font-weight: bold">เอกสารสำคัญ</h4>
                                    <span>
                                        <?php echo $row['Document']; ?>
                                    </span>
                                    <hr>   
                                    <h4 style="font-weight: bold">สถานที่ปฏิบัติงาน</h4>
                                    <span>
                                        <?php echo $row['Workplace']; ?>
                                    </span>
                                    <hr> 
                                    <h4 style="font-weight: bold">การเดินทาง</h4>
                                    <div class="col-md-6">
                                        <div class="mt-checkbox-list">
                                            <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวอ่อน (สายสุขุมวิท)
                                                <input type="checkbox" value="1" name="Bts_Sukumvit" <?php if(isset($row['Id']) != ''){if($row['Bts_Sukumvit'] == true){echo "checked";}else{echo "";} }?>>
                                                <span></span>
                                            </label>
                                            <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีน้ำเงิน
                                                <input type="checkbox" value="1" name="Mrt_Blue" <?php if(isset($row['Id']) != ''){if($row['Mrt_Blue'] == true){echo "checked";}else{echo "";} }?>>
                                                <span></span>
                                            </label>
                                            <label class="mt-checkbox mt-checkbox-outline"> ARL
                                                <input type="checkbox" value="1" name="ARL" <?php if(isset($row['Id']) != ''){if($row['ARL'] == true){echo "checked";}else{echo "";} }?>>
                                                <span></span>
                                            </label>
                                        </div>
                                     </div>
                                        
                                     <div class="col-md-6">
                                        <div class="mt-checkbox-list">
                                            <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวเข้ม (สายสีลม)
                                                <input type="checkbox" value="1" name="Bts_Seelom" <?php if(isset($row['Id']) != ''){if($row['Bts_Seelom'] == true){echo "checked";}else{echo "";} }?>>
                                                <span></span>
                                            </label>
                                            <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีม่วง 
                                                <input type="checkbox" value="1" name="Mrt_Purple" <?php if(isset($row['Id']) != ''){if($row['Mrt_Purple'] == true){echo "checked";}else{echo "";} }?>>
                                                <span></span>
                                            </label>
                                            <label class="mt-checkbox mt-checkbox-outline"> BRT
                                                <input type="checkbox" value="1" name="BRT" <?php if(isset($row['Id']) != ''){if($row['BRT'] == true){echo "checked";}else{echo "";} }?>>
                                                <span></span>
                                            </label>
                                        </div>
                                     </div>
                                       
                                   <!--  <h4 style="font-weight: bold">สวัสดิการ</h4>
                                    <span>
                                        Allowance 5,000 THB per month
                                    </span> -->

                                    <h4 style="font-weight: bold">ติดต่อ</h4>
                                     <span>
                                        <b>Contract</b> : <?php echo $row['EstablishName']; ?>
                                    </span>
                                    <br>
                                    <span>
                                        <b>Phone No.</b> : <?php echo $row['Tel']; ?>
                                    </span>
                                    <br>
                                    <span>
                                        <b>Email</b> : <?php echo $row['Email']; ?>
                                    </span> 
                                    <div id="map" style="height: 300px ;margin-top: 3%"></div>                                               
                                </div>
                                
                                <div style="padding-top: 5px" id="share"></div>      
                                <div style="">
                                    <hr>
                                </div>
                            </div>     
                            <div class="col-md-4" style="padding-right: 0px">
                                <div id="" class="col-md-12" style="background-color:#ebeeef;padding-bottom: 2%;padding-top: 3%">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <img width="150px" src="../../Logo/<?php echo $row['Path_Logo'] ?>">
                                        </div>
                                        <div class="col-md-6">
                                             <span>
                                                <?php echo $row['EstablishName']; ?>
                                            </span>
                                        </div>
                                    </div>
                                    <br>
                                    <span>
                                        <?php echo $row['EstablishDesc']; ?>
                                    </span>
                                    <br>
                                    <h4 style="font-weight: bold">
                                        ติดต่อเรา
                                    </h4>
                                    <span>
                                        <b>Phone No.</b> : <?php echo $row['Tel']; ?>
                                    </span>
                                    <br>
                                    <span>
                                        <b>Email</b> : <?php echo $row['Email']; ?>
                                    </span> 

                                </div>
                                <script>
                                    function initMap() {
                                        var uluru = {lat: <?php echo $row['Lat']; ?>, lng:   <?php echo $row['Lng']; ?>};
                                        var map = new google.maps.Map(document.getElementById('map'), {
                                          zoom: 13,
                                          center: uluru
                                        });

                                        var marker = new google.maps.Marker({
                                          position: uluru,
                                          map: map,
                                          draggable:true,
                                          label: 'We Are Here'

                                        });

                                        google.maps.event.addListener(marker, 'dragend', function() 
                                        {
                                            $('#Lng').val(marker.getPosition().lat());
                                            $('#Lat').val(marker.getPosition().lng());

                                        });
                                    }
                                </script>
                                <div id="" class="col-md-12" style="background-color:white;padding-top: 3%;margin-top: 5%">
                                    <h4 style="font-weight: bold">
                                         ตำแหน่งงานที่เกี่ยวข้อง
                                     </h4>
                                         <?php 
                                            include '../../PHP/ConnectDB.php';

                                            $sql2 = "SELECT *
                                                FROM vwsearchannounce 
                                                WHERE Id != ".$_GET['Announce']." AND CategoryId = ".$row['CategoryId']." ORDER BY RAND() LIMIT 5";  
                                            $result2 = mysqli_query($con,$sql2); 

                                            mysqli_close($con);

                                        ?>
                                        <?php  $i = 1;
                                            while ($row2 = mysqli_fetch_assoc($result2)) {  
                                        ?>
                                        <div class="row" style="background-color: white;padding-bottom: 10px;padding-top: 10px">
                                            <div class="col-md-6">
                                                <img width="100%" src="../../Logo/<?php echo $row2['Path_Logo'] ?>">
                                            </div>
                                            <div class="col-md-6" style="padding: 0px">
                                                 <span>
                                                    <a href="../Home/AnnounceDetail.php?Announce=<?php echo $row2['Id'] ?>"><?php echo $row2['Name']; ?></a>   
                                                </span>
                                                <br>
                                                <span class="job-post-box">
                                                    <i class="fa fa-building"></i> 
                                                    <?php echo $row2['EstablishName']; ?>
                                                </span>
                                                <br>
                                                <span class="job-post-box">
                                                     <i class="fa fa-map-marker"></i> 
                                                    ฝึกงาน <?php echo $row2['PROVINCE_NAME']; ?>
                                                </span>
                                            </div>
                                        </div>
                                        <?php  
                                            $i++;};  
                                        ?>
                                </div>
                            </div>                      
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<script>
    $("#share").jsSocials({
        shareIn: "popup",
        shares: ["twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon"]
    });
</script>
<?php include '../_Master/_footer.php'; ?>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwKZ7KKf8AXTnHqyN3UaJRuLnvOZdArQA&callback=initMap">
</script>