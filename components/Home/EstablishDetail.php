<?php include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];
        $query = mysqli_query($con,"SELECT *
                                    FROM vwsearchestablish
                                    WHERE Id = '$Id' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
        
    }
    
    mysqli_close($con);
?>
<script type="text/javascript">
    document.title = "แก้ไขสถานประกอบการ"
</script>
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false, 
                success: function (response) {
                     if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }     
                }
                
            });
        
        });
    });
    function selectProvince(provinceId) {
        if (provinceId != "-1") {
            loadData('amphur',provinceId);
            $("#amphur_dropdown").html("<option value='-1'>");
            $("#district_dropdown").html("<option value='-1'>Select District</option>");
        }
    };
    function selectAmphur(amphurId) {
        if (amphurId != "-1") {
            loadData('district',amphurId);
            $("#district_dropdown").html("<option value='-1'>");
        }
    };
    function loadData(loadType,YearId) {
        var dataString ='loadType='+loadType+'&loadId='+YearId;
        $.ajax({
            type: "POST",
            url:'./loadData.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                จัดการสถานประกอบการ         
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="./Search.php">จัดการสถานประกอบการ</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>แก้ไขสถานประกอบการ</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">แก้ไขข้อมูล</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="col-md-4 col-md-offset-3" style="text-align: center">
                                                        <?php if ($row['Path_Logo'] != ''): ?>
                                                            <img src="../../Logo/<?php echo $row['Path_Logo']; ?>" width="200px" alt="" />
                                                        <?php endif ?>
                                                        <?php if ($row['Path_Logo'] == ''): ?>
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อสถานประกอบการ</label>
                                                    <input type="hidden" name="func" class="form-control" value="Edit">
                                                    <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                                                    <div class="col-md-4">
                                                        <label class="control-label"><?php if(isset($_GET['Id']) != ''){echo $row['Name']; }?></lebel>
                                                    </div>
                                                    <input type="hidden" name="func" class="form-control" value="Edit">
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">รายละเอียด</label>
                                                    <div class="col-md-4 ">
                                                        <label class="control-label" style="text-align: left"><?php if(isset($_GET['Id']) != ''){echo $row['Description']; }?></lebel>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">ที่อยู่</label>
                                                    <div class="col-md-4">
                                                        <label class="control-label" style="text-align: left"><?php if(isset($_GET['Id']) != ''){echo $row['Address']; }?></lebel>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">อีเมล</label>
                                                    <div class="col-md-4">
                                                        <label class="control-label"><?php if(isset($_GET['Id']) != ''){echo $row['Email']; }?></lebel>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">เบอร์โทรศัพท์</label>
                                                    <div class="col-md-4">
                                                        <label class="control-label"><?php if(isset($_GET['Id']) != ''){echo $row['Tel']; }?></lebel>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">แฟ็กซ์</label>
                                                    <div class="col-md-4">
                                                        <label class="control-label"><?php if(isset($_GET['Id']) != ''){echo $row['Fax']; }?></lebel>
                                                    </div>
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM provinces order by PROVINCE_ID ASC ";  
                                                    $listprovince = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">จังหวัด</label>
                                                    <div class="col-md-4">
                                                       <label class="control-label"><?php if(isset($_GET['Id']) != ''){echo $row['PROVINCE_NAME']; }?></lebel>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single" class="col-md-3 control-label">อำเภอ</label>
                                                    <div class="col-md-4">
                                                         <label class="control-label"><?php if(isset($_GET['Id']) != ''){echo $row['AMPHUR_NAME']; }?></lebel>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">แขวง</label>
                                                    <div class="col-md-4">
                                                        <label class="control-label"><?php if(isset($_GET['Id']) != ''){echo $row['DISTRICT_NAME']; }?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">แฟ็กซ์</label>
                                                    <div class="col-md-4">
                                                        <label><?php if(isset($_GET['Id']) != ''){echo $row['Address']; }?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ปักหมุด</label>
                                                    <div class="col-md-8">
                                                        <div id="map" style="height: 300px"></div>
                                                        <input type="hidden" name="Lng" id="Lng" class="form-control" placeholder="Phone No." value="<?php echo $row['Lng'] ?>" required>
                                                        <input type="hidden" name="Lat" id="Lat" class="form-control" placeholder="Phone No." value="<?php echo $row['Lat'] ?>" required>
                                                        <input type="hidden" name="Path_Logo" class="form-control" value="<?php echo $row['Path_Logo'] ?>" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function initMap() {
                                var uluru = {lat: <?php echo $row['Lat']; ?>, lng:   <?php echo $row['Lng']; ?>};
                                var map = new google.maps.Map(document.getElementById('map'), {
                                  zoom: 13,
                                  center: uluru
                                });

                                var marker = new google.maps.Marker({
                                  position: uluru,
                                  map: map,
                                  draggable:true,
                                  label: 'We Are Here'

                                });

                                google.maps.event.addListener(marker, 'dragend', function() 
                                {
                                    $('#Lng').val(marker.getPosition().lat());
                                    $('#Lat').val(marker.getPosition().lng());

                                });
                            }
                        </script>
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">งานที่ลงประกาศ</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <?php
                                            //header('Content-type: application/json; charset=utf-8');
                                            include '../../PHP/ConnectDB.php';

                                            //Search
                                            $sql = "SELECT v1.Id ,v1.Name as Title , v1.Description,v1.Position ,v1.Skill,v1.Workplace , v1.DateFrom,v1.DateTo,v1.IsActive,v2.Name as Establish ,v1.Created
                                                     FROM announce as v1 INNER JOIN establish as v2 ON v1.EstablishId = v2.Id WHERE v1.IsActive = 1 AND v1.EstablishId = ".$_GET['Id']."";  
                                                $result = mysqli_query($con,$sql);  
                                            
                                            mysqli_close($con);
                                            //exit(json_encode($response_array));
                                        ?>
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th> หัวข้อ </th>
                                                    <th> รายละเอียด </th>
                                                    <th> วันที่เริ่มรับสมัคร </th>
                                                    <th> วันที่สิ้นสุดการรับสมัคร </th>
                                                    <!-- <th> สถานะการใช้งาน </th> -->
                                                    <th> วันที่สร้างข้อมูล </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php  $i = 1;
                                                    while ($row = mysqli_fetch_assoc($result)) {  
                                                ?>
                                                <tr>  
                                                    <td><a href="../Home/AnnounceDetail.php?Announce=<?php echo $row['Id'] ?>"><?php echo $row["Title"]; ?></a></td> 
                                                    <td><?php echo $row["Description"];?></td>
                                                    <td><?php echo $row["DateFrom"];?></td>
                                                    <td><?php echo $row["DateTo"];?></td>
                                                    <!-- <td><?php echo $row["IsActive"] == 1 ? 'ใช้งาน':'ไม่ใช้งาน';?></td> -->
                                                    <td><?php echo $row["Created"];?></td>                                              
                                                </tr>
                                                <?php  
                                                    };  
                                                ?>   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwKZ7KKf8AXTnHqyN3UaJRuLnvOZdArQA&callback=initMap">
</script>