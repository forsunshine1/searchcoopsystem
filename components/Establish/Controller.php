<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Create':
			Create();
			break;
		case 'Delete':
			Delete();
			break;
		case 'Edit':
			Edit();
			break;	
		default:
			# code...
			break;
	}

	function Create()
	{
		include '../../PHP/ConnectDB.php';
		$AccountId = base64_decode($_POST['AccountId']);
		$Name = $_POST['Name'];
		$Description = $_POST['Description'];
		$Address = $_POST['Address'];
		$Email = $_POST['Email'];
		$Tel = $_POST['Tel'];
		$Fax = $_POST['Fax'];
		$Lat = $_POST['lat'];
		$Lng = $_POST['lng'];
		$PROVINCE_ID = $_POST['PROVINCE_ID'];
		$AMPHUR_ID = $_POST['AMPHUR_ID'];
		$DISTRICT_ID = $_POST['DISTRICT_ID'];
		$IsActive = $_POST['IsActive'];
		if (isset($_FILES["img_member"])) {
			$temp = explode(".", $_FILES["img_member"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			move_uploaded_file($_FILES["img_member"]["tmp_name"], "../../Logo/" . $newfilename);
			$imgpath = $newfilename ;

		}else{
			$imgpath = "";
		}
		if(isset($_POST['Name']))
		{
			$sql = "INSERT INTO establish (AccountId,Name,Description,Address,Email,Tel,Fax,Lat,Lng,IsActive,PROVINCE_ID,AMPHUR_ID,DISTRICT_ID,Path_Logo) VALUES ('$AccountId','$Name','$Description','$Address','$Email','$Tel','$Fax','$Lat','$Lng','$IsActive','$PROVINCE_ID','$AMPHUR_ID','$DISTRICT_ID','$imgpath')";

			mysqli_query($con, $sql);
			$response_array['status'] = 'success';  

		}else{
		    
		    $response_array['status'] = 'error';
		} 
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$Name = $_POST['Name'];
		$Description = $_POST['Description'];
		$Address = $_POST['Address'];
		$Email = $_POST['Email'];
		$Tel = $_POST['Tel'];
		$Fax = $_POST['Fax'];
		$Lat = $_POST['Lat'];
		$Lng = $_POST['Lng'];
		$PROVINCE_ID = $_POST['PROVINCE_ID'];
		$AMPHUR_ID = $_POST['AMPHUR_ID'];
		$DISTRICT_ID = $_POST['DISTRICT_ID'];
		$IsActive = $_POST['IsActive'];
		$Id = $_POST['Id'];
		if (!empty($_FILES["img_member"]) AND $_FILES["img_member"]["name"] != '') {
			$temp = explode(".", $_FILES["img_member"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			move_uploaded_file($_FILES["img_member"]["tmp_name"], "../../Logo/" . $newfilename);
			$imgpath = $newfilename ;
			if (!empty($_FILES["img_member"]["name"]) AND $_FILES["img_member"]["name"] != '') {
				
				$valid =mysqli_query($con, "SELECT * FROM establish WHERE Id = '$Id'");
				$validImage = mysqli_fetch_array($valid,MYSQLI_ASSOC);

				if ($validImage['Path_Logo'] != '' || $validImage['Path_Logo'] != null) {
					unlink("../../Logo/" .$_POST['Path_Logo']);
				}

			}
		}else{
			$imgpath = "";
		}
		if (isset($_POST['Name'])) {

			$sql = mysqli_query($con,"UPDATE establish SET Name = '$Name' , Description = '$Description' , Address = '$Address', Email = '$Email', Tel = '$Tel', Fax = '$Fax', Lat = '$Lat' ,Lng = '$Lng', IsActive = '$IsActive',PROVINCE_ID = '$PROVINCE_ID',AMPHUR_ID = '$AMPHUR_ID',DISTRICT_ID = '$DISTRICT_ID', Path_Logo = '$imgpath' WHERE Id='$Id'");

			$response_array['status'] = 'success';  		

		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Delete()
	{
		include '../../PHP/ConnectDB.php';
		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = "DELETE FROM establish WHERE Id='$Id'";

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	exit(json_encode($response_array));
 ?>