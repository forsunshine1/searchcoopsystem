<?php include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];
        $query = mysqli_query($con,"SELECT ES.Id,
                                            ES.AccountId,
                                            ES.Name,
                                            ES.Description,
                                            ES.Address,
                                            ES.Email,
                                            ES.Tel,
                                            ES.Fax,
                                            ES.Lat,
                                            ES.Lng,
                                            ES.IsActive,
                                            ES.Created,
                                            ES.PROVINCE_ID,
                                            ES.AMPHUR_ID,
                                            ES.DISTRICT_ID,
                                            ES.Path_Logo,
                                            P.PROVINCE_NAME,
                                            A.AMPHUR_NAME,
                                            D.DISTRICT_NAME
                                    FROM establish as ES
                                    LEFT JOIN provinces as P ON ES.PROVINCE_ID = p.PROVINCE_ID
                                    LEFT JOIN amphures as A ON ES.AMPHUR_ID = A.AMPHUR_ID
                                    LEFT JOIN districts AS D ON ES.DISTRICT_ID = D.DISTRICT_ID
                                    WHERE Id = '$Id' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
        
    }
    
    mysqli_close($con);
?>
<script type="text/javascript">
    document.title = "แก้ไขสถานประกอบการ"
</script>
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false, 
                success: function (response) {
                     if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }     
                }
                
            });
        
        });
    });
    function selectProvince(provinceId) {
        if (provinceId != "-1") {
            loadData('amphur',provinceId);
            $("#amphur_dropdown").html("<option value='-1'>");
            $("#district_dropdown").html("<option value='-1'>Select District</option>");
        }
    };
    function selectAmphur(amphurId) {
        if (amphurId != "-1") {
            loadData('district',amphurId);
            $("#district_dropdown").html("<option value='-1'>");
        }
    };
    function loadData(loadType,YearId) {
        var dataString ='loadType='+loadType+'&loadId='+YearId;
        $.ajax({
            type: "POST",
            url:'./loadData.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                จัดการสถานประกอบการ         
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="./Search.php">จัดการสถานประกอบการ</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>แก้ไขสถานประกอบการ</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">แก้ไขข้อมูล</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อสถานประกอบการ</label>
                                                    <input type="hidden" name="func" class="form-control" value="Edit">
                                                    <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                                                    <div class="col-md-4">
                                                        <input type="text" name="Name" id="Name" class="form-control" placeholder="ชื่อสถานประกอบการ" value="<?php if(isset($_GET['Id']) != ''){echo $row['Name']; }?>" required>
                                                    </div>
                                                    <input type="hidden" name="func" class="form-control" value="Edit">
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">รายละเอียด</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Description" name="Description" class="form-control" placeholder="รายละเอียด" value="<?php if(isset($_GET['Id']) != ''){echo $row['Description']; }?>" required>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">ที่อยู่</label>
                                                    <div class="col-md-4">
                                                        <textarea type="textarea" rows="3" id="Address" name="Address" class="form-control" placeholder="ที่อยู่" required><?php if(isset($_GET['Id']) != ''){echo $row['Address']; }?></textarea>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">อีเมล</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Email" name="Email" class="form-control" placeholder="อีเมล" value="<?php if(isset($_GET['Id']) != ''){echo $row['Email']; }?>" required>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">เบอร์โทรศัพท์</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Tel" name="Tel" class="form-control" placeholder="เบอร์โทรศัพท์" value="<?php if(isset($_GET['Id']) != ''){echo $row['Tel']; }?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">แฟ็กซ์</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Fax" name="Fax" class="form-control" placeholder="แฟ็กซ์" value="<?php if(isset($_GET['Id']) != ''){echo $row['Fax']; }?>" required>
                                                    </div>
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM provinces order by PROVINCE_ID ASC ";  
                                                    $listprovince = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">จังหวัด</label>
                                                    <div class="col-md-4">
                                                        <select onchange="selectProvince(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="PROVINCE_ID">
                                                             <option value='<?php if(isset($_GET['Id']) != ''){echo $row['PROVINCE_ID']; }?>'><?php if(isset($_GET['Id']) != ''){echo $row['PROVINCE_NAME']; }?></option>
                                                            <?php while ($row2=mysqli_fetch_assoc($listprovince)) { ?>
                                                                <option value="<?php echo $row2['PROVINCE_ID']?>"><?php echo $row2['PROVINCE_NAME']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single" class="col-md-3 control-label">อำเภอ</label>
                                                    <div class="col-md-4">
                                                        <select onchange="selectAmphur(this.options[this.selectedIndex].value)" id="amphur_dropdown" class="form-control select2" name="AMPHUR_ID">
                                                            <option value='<?php if(isset($_GET['Id']) != ''){echo $row['AMPHUR_ID']; }?>'><?php if(isset($_GET['Id']) != ''){echo $row['AMPHUR_NAME']; }?></option>
                                                            <span id="amphur_loader"></span>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">แขวง</label>
                                                    <div class="col-md-4">
                                                        <select id="district_dropdown" class="form-control select2" name="DISTRICT_ID">
                                                            <option value='<?php if(isset($_GET['Id']) != ''){echo $row['DISTRICT_ID']; }?>'><?php if(isset($_GET['Id']) != ''){echo $row['DISTRICT_NAME']; }?></option>
                                                            <span id="district_loader"></span>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">แฟ็กซ์</label>
                                                    <div class="col-md-4">
                                                        <textarea type="text" rows="3" id="Address" name="Address" class="form-control" placeholder="ที่อยู่" ><?php if(isset($_GET['Id']) != ''){echo $row['Address']; }?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ปักหมุด</label>
                                                    <div class="col-md-8">
                                                        <div id="map" style="height: 300px"></div>
                                                        <input type="hidden" name="Lng" id="Lng" class="form-control" placeholder="Phone No." value="<?php echo $row['Lng'] ?>" required>
                                                        <input type="hidden" name="Lat" id="Lat" class="form-control" placeholder="Phone No." value="<?php echo $row['Lat'] ?>" required>
                                                        <input type="hidden" name="Path_Logo" class="form-control" value="<?php echo $row['Path_Logo'] ?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Logo Upload</label>
                                                    <div class="col-md-4">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <?php if ($row['Path_Logo'] != ''): ?>
                                                                    <img src="../../Logo/<?php echo $row['Path_Logo']; ?>" alt="" />
                                                                <?php endif ?>
                                                                <?php if ($row['Path_Logo'] == ''): ?>
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                                <?php endif ?>
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                                            </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> เลือกรูป </span>
                                                                    <span class="fileinput-exists"> เปลี่ยน </span>
                                                                    <input type="file" name="img_member"> 
                                                                </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> ลบ 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานะการใช้งาน</label>
                                                    <div class="col-md-4">
                                                        <div class="md-radio-inline">
                                                            <div class="md-radio">
                                                                <input type="radio" value="1" name="IsActive" id="radio6" name="radio2" class="md-radiobtn" <?php if(isset($_GET['Id']) != ''){if($row['IsActive'] == true){echo "checked";} }?> >
                                                                <label for="radio6">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ใช้งาน </label>
                                                            </div>
                                                            <div class="md-radio">
                                                                <input type="radio" value="0" name="IsActive" id="radio7" name="radio2" class="md-radiobtn" <?php if(isset($_GET['Id']) != ''){if($row['IsActive'] == false){echo "checked";} }?>>
                                                                <label for="radio7">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ไม่ใช้ใช้งาน </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">บันทึก</button>
                                                        <a type="button" href="Search.php" class="btn default">กลับ</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            function initMap() {
                                var uluru = {lat: <?php echo $row['Lat']; ?>, lng:   <?php echo $row['Lng']; ?>};
                                var map = new google.maps.Map(document.getElementById('map'), {
                                  zoom: 13,
                                  center: uluru
                                });

                                var marker = new google.maps.Marker({
                                  position: uluru,
                                  map: map,
                                  draggable:true,
                                  label: 'We Are Here'

                                });

                                google.maps.event.addListener(marker, 'dragend', function() 
                                {
                                    $('#Lng').val(marker.getPosition().lat());
                                    $('#Lat').val(marker.getPosition().lng());

                                });
                            }
                        </script>
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">งานที่ลงประกาศ</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <?php
                                            //header('Content-type: application/json; charset=utf-8');
                                            include '../../PHP/ConnectDB.php';

                                            //Search
                                            $sql = "SELECT v1.Id ,v1.Name as Title , v1.Description,v1.Position ,v1.Skill,v1.Workplace , v1.DateFrom,v1.DateTo,v1.IsActive,v2.Name as Establish ,v1.Created
                                                     FROM announce as v1 INNER JOIN establish as v2 ON v1.EstablishId = v2.Id WHERE v1.IsActive = 1 AND v1.EstablishId = ".$_GET['Id']."";  
                                                $result = mysqli_query($con,$sql);  
                                            
                                            mysqli_close($con);
                                            //exit(json_encode($response_array));
                                        ?>
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th> หัวข้อ </th>
                                                    <th> รายละเอียด </th>
                                                    <th> วันที่เริ่มรับสมัคร </th>
                                                    <th> วันที่สิ้นสุดการรับสมัคร </th>
                                                    <th> สถานะการใช้งาน </th>
                                                    <th> วันที่สร้างข้อมูล </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php  $i = 1;
                                                    while ($row = mysqli_fetch_assoc($result)) {  
                                                ?>
                                                <tr>  
                                                    <td><a href="../Home/AnnounceDetail.php?Announce=<?php echo $row['Id'] ?>"><?php echo $row["Title"]; ?></a></td> 
                                                    <td><?php echo $row["Description"];?></td>
                                                    <td><?php echo $row["DateFrom"];?></td>
                                                    <td><?php echo $row["DateTo"];?></td>
                                                    <td><?php echo $row["IsActive"] == 1 ? 'ใช้งาน':'ไม่ใช้งาน';?></td>
                                                    <td><?php echo $row["Created"];?></td>                                              
                                                </tr>
                                                <?php  
                                                    };  
                                                ?>   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwKZ7KKf8AXTnHqyN3UaJRuLnvOZdArQA&callback=initMap">
</script>