<?php include '../_Master/_header.php'; ?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false, 
                success: function (response) {
                     if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }           
                }
                
            });
        
        });
    });
    function selectProvince(provinceId) {
        if (provinceId != "-1") {
            loadData('amphur',provinceId);
            $("#amphur_dropdown").html("<option value='-1'>");
        }
    };
    function selectAmphur(amphurId) {
        if (amphurId != "-1") {
            loadData('district',amphurId);
            $("#district_dropdown").html("<option value='-1'>");
        }
    };
    function loadData(loadType,YearId) {
        var dataString ='loadType='+loadType+'&loadId='+YearId;
        $.ajax({
            type: "POST",
            url:'./loadData.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<script type="text/javascript">
    document.title = "เพิ่มสถานประกอบการ"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                จัดการสถานประกอบการ        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="./Search.php">จัดการสถานประกอบการ</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>เพิ่มสถานประกอบการ </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">สร้างข้อมูล </span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อสถานประกอบการ</label>
                                                    <input type="hidden" name="func" class="form-control" value="Create">
                                                    <input type="hidden" name="AccountId" class="form-control" value="<?php echo $Id ?>">
                                                    <div class="col-md-4">
                                                        <input type="text" name="Name" id="Name" class="form-control" placeholder="ชื่อสถานประกอบการ" required>
                                                    </div>
                                                </div>
                                               <div class="form-group">
                                                    <label class="col-md-3 control-label">รายละเอียด</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Description" name="Description" class="form-control" placeholder="รายละเอียด" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">อีเมล</label>
                                                    <div class="col-md-4">
                                                        <input type="email" name="Email" class="form-control" placeholder="อีเมล">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">เบอร์โทรศัพท์</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Tel" class="form-control" placeholder="เบอร์โทรศัพท์">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">แฟ็กซ์</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Fax" class="form-control" placeholder="แฟ็กซ์">
                                                    </div>
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM provinces order by PROVINCE_ID ASC ";  
                                                    $listprovince = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">จังหวัด</label>
                                                    <div class="col-md-4">
                                                        <select onchange="selectProvince(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="PROVINCE_ID">
                                                             <option value='-1'>Select Province</option>
                                                            <?php while ($row2=mysqli_fetch_assoc($listprovince)) { ?>
                                                                <option value="<?php echo $row2['PROVINCE_ID']?>"><?php echo $row2['PROVINCE_NAME']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single" class="col-md-3 control-label">อำเภอ</label>
                                                    <div class="col-md-4">
                                                        <select onchange="selectAmphur(this.options[this.selectedIndex].value)" id="amphur_dropdown" class="form-control select2" name="AMPHUR_ID">
                                                            <option value="-1">Select Amphur</option>
                                                            <span id="amphur_loader"></span>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">แขวง</label>
                                                    <div class="col-md-4">
                                                        <select id="district_dropdown" class="form-control select2" name="DISTRICT_ID">
                                                            <option value="-1">Select District</option>
                                                            <span id="district_loader"></span>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ที่อยู่</label>
                                                    <div class="col-md-4">
                                                        <textarea type="textarea" rows="3" name="Address" class="form-control" placeholder="ที่อยู่"></textarea> 
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ปักหมุด</label>
                                                    <div class="col-md-8">
                                                        <div id="map" style="height: 300px"></div>
                                                        <input type="hidden" name="lng" id="lng" class="form-control" placeholder="lng" required>
                                                        <input type="hidden" name="lat" id="lat" class="form-control" placeholder="lat" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Logo Upload </label>
                                                    <div class="col-md-4">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                                            </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="img_member"> </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานะการใช้งาน</label>
                                                    <div class="col-md-4">
                                                        <div class="md-radio-inline">
                                                            <div class="md-radio">
                                                                <input type="radio" value="1" name="IsActive" id="radio6" name="radio2" class="md-radiobtn" checked>
                                                                <label for="radio6">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ใช้งาน </label>
                                                            </div>
                                                            <div class="md-radio">
                                                                <input type="radio" value="0" name="IsActive" id="radio7" name="radio2" class="md-radiobtn" >
                                                                <label for="radio7">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ไม่ใช้ใช้งาน </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">บันทึก</button>
                                                        <a type="button" href="Search.php" class="btn default">ยกเลิก</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<script>
    function initMap() {
        var uluru = {lat: 13.7563, lng:  100.5018};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: uluru
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          draggable:true,
          label: 'We Are Here'

        });

        google.maps.event.addListener(marker, 'dragend', function() 
        {
            $('#lat').val(marker.getPosition().lat());

            $('#lng').val(marker.getPosition().lng());

        });
    }
</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwKZ7KKf8AXTnHqyN3UaJRuLnvOZdArQA&callback=initMap">
</script>
<?php include '../_Master/_footer.php'; ?>