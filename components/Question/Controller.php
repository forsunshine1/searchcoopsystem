<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Create':
			Create();
			break;
		case 'Delete':
			Delete();
			break;
		case 'Edit':
			Edit();
			break;	
		default:
			# code...
			break;
	}

	function Create()
	{
		include '../../PHP/ConnectDB.php';
		$KitId = $_POST['KitId'];
		$No = $_POST['No'];
		$Description = $_POST['Description'];
		$IsActive = $_POST['IsActive'];

		if(isset($_POST['No']))
		{
			$valid = "SELECT * FROM question WHERE KitId = '$KitId' AND No= '$No' ";

			$validNum = mysqli_num_rows(mysqli_query($con, $valid));
			 
			if($validNum == ''){
				$sql = "INSERT INTO question (KitId,No,Description,IsActive) VALUES ('$KitId','$No','$Description','$IsActive')";

				mysqli_query($con, $sql);
				$response_array['status'] = 'success';  

			}else{
				
				$response_array['status'] = 'duplicate';
			}							

		}else{
		    
		    $response_array['status'] = 'error';
		} 
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$KitId = $_POST['KitId'];
		$No = $_POST['No'];
		$Description = $_POST['Description'];
		$IsActive = $_POST['IsActive'];
		//UPDATE
		if (isset($_POST['Id'])) {

			$Id = $_POST['Id'];
			$valid = "SELECT * FROM question WHERE KitId = '$KitId' AND No= '$No' ";

			$validNum = mysqli_num_rows(mysqli_query($con, $valid));

			$query = "SELECT * FROM question WHERE KitId='$KitId' AND No = '$No'";
			$row = mysqli_fetch_array(mysqli_query($con, $query));

			if($validNum == '' || $row['Id'] == $Id){
				
				// $sql = "INSERT INTO branch (FacultyId,BranchCode,BranchName,BranchDesc) VALUES ('$FacultyId','$BranchCode','$BranchName','$BranchDesc')";
				$sql = mysqli_query($con,"UPDATE question SET KitId = '$KitId',No = '$No',Description = '$Description' , IsActive = '$IsActive' WHERE Id='$Id'");
				$response_array['status'] = 'success';  

			}else{
				
				$response_array['status'] = 'duplicate';
			}		

		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Delete()
	{
		include '../../PHP/ConnectDB.php';
		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = "DELETE FROM question WHERE Id='$Id'";

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	exit(json_encode($response_array));
 ?>