<?php 
    include '../_Master/_header.php';
?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Username'])) {
        $StudentCode = base64_decode($_GET['Username']);
        $query = mysqli_query($con,"SELECT * FROM coopproject WHERE StudentCode = '$StudentCode' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
    }
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<!-- POST  -->
<script type="text/javascript">
    document.title = "ข้อมูลโครงงานสหกิจศึกษา"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ข้อมูลโครงงานสหกิจศึกษา 
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ข้อมูลโครงงานสหกิจศึกษา </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">ข้อมูลโครงงานสหกิจศึกษา</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form" id="View">
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">ปก</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['01_cov'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['01_cov']; ?>/edit">01_cov</a>
                                                <?php endif ?>
                                                <?php if ($row['01_cov'] == ''): ?>
                                                    <span>01_cov</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">ใบอนุมัติลายเซ็นต์คณะกรรมการสอบ</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['02_app'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['02_app']; ?>/edit">02_app</a>
                                                <?php endif ?>
                                                <?php if ($row['02_app'] == ''): ?>
                                                    <span>02_app</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">บทคัดย่อ</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['03_absen'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['03_absen']; ?>/edit">03_absen</a>
                                                <?php endif ?>
                                                <?php if ($row['03_absen'] == ''): ?>
                                                    <span>03_absen</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">กิตติกรรมประกาศ </label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['04_ack'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['04_ack']; ?>/edit">04_ack</a>
                                                <?php endif ?>
                                                <?php if ($row['04_ack'] == ''): ?>
                                                    <span>04_ack</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">สารบัญ </label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['05_tbc'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['05_tbc']; ?>/edit">05_tbc</a>
                                                <?php endif ?>
                                                <?php if ($row['05_tbc'] == ''): ?>
                                                    <span>05_tbc</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">บทที่ 1</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['06_ch1'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['06_ch1']; ?>/edit">06_ch1</a>
                                                <?php endif ?>
                                                <?php if ($row['06_ch1'] == ''): ?>
                                                    <span>06_ch1</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">บทที่ 2</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['07_ch2'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['07_ch2']; ?>/edit">07_ch2</a>
                                                <?php endif ?>
                                                <?php if ($row['07_ch2'] == ''): ?>
                                                    <span>07_ch2</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">บทที่ 3</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['08_ch3'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['08_ch3']; ?>/edit">08_ch3</a>
                                                <?php endif ?>
                                                <?php if ($row['08_ch3'] == ''): ?>
                                                    <span>08_ch3</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">บทที่ 4</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['09_ch4'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['09_ch4']; ?>/edit">09_ch4</a>
                                                <?php endif ?>
                                                <?php if ($row['09_ch4'] == ''): ?>
                                                    <span>09_ch4</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">บทที่ 5</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['10_ch5'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['10_ch5']; ?>/edit">10_ch5</a>
                                                <?php endif ?>
                                                <?php if ($row['10_ch5'] == ''): ?>
                                                    <span>10_ch5</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">บรรณานุกรม </label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['11_ref'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['11_ref']; ?>/edit">11_ref</a>
                                                <?php endif ?>
                                                <?php if ($row['11_ref'] == ''): ?>
                                                    <span>11_ref</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">ภาคผนวก</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['12_app'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['12_app']; ?>/edit">12_app</a>
                                                <?php endif ?>
                                                <?php if ($row['12_app'] == ''): ?>
                                                    <span>12_app</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-md-3 control-label text-right">ประวัติผู้เขียน</label>
                                            <div class="col-md-5 text-left">
                                                <?php if ($row['13_vit'] != ''): ?>
                                                    <a target="_blank" href="https://drive.google.com/file/d/<?php echo $row['13_vit']; ?>/edit">13_vit</a>
                                                <?php endif ?>
                                                <?php if ($row['13_vit'] == ''): ?>
                                                    <span>13_vit</span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9" style="margin-top: 2%">
                                                <a class="btn green" href="./UploadDocumentCoop.php?Id=<?php echo $Id;?>"> 
                                                    แก้ไข
                                                </a>                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>