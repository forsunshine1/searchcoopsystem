<?php
header('Content-type: application/json; charset=utf-8');
include '../../PHP/ConnectDB.php';

if ($_FILES["DocFile"]["name"] != "") {
  $temp = explode(".", $_FILES["DocFile"]["name"]);
  $newfilename = round(microtime(true)) . '.' . end($temp);
  move_uploaded_file($_FILES["DocFile"]["tmp_name"], './DocumentFile/' .$newfilename);
  $Doc = $newfilename ;

}else{
  $Doc = "";
}
$No = $_POST['No'];
$StudentId = $_POST['StudentId'];


if ($_FILES["DocFile"]["name"] != "") {
  require_once '../../vendor/autoload.php';

define('APPLICATION_NAME', 'Drive API PHP Quickstart');
define('CREDENTIALS_PATH', __DIR__ . '/xxx.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/drive-php-quickstart.json
define('SCOPES', implode(' ', array(
  Google_Service_Drive::DRIVE)
));

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient() {
  $client = new Google_Client();
  $client->setApplicationName(APPLICATION_NAME);
  $client->setScopes(SCOPES);
  $client->setAuthConfig(CLIENT_SECRET_PATH);
  $client->setAccessType('offline');

  // Load previously authorized credentials from a file.
  $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
  if (file_exists($credentialsPath)) {
    $accessToken = json_decode(file_get_contents($credentialsPath), true);
  } else {
    // Request authorization from the user.
    $authUrl = $client->createAuthUrl();
    printf("Open the following link in your browser:\n%s\n", $authUrl);
    print 'Enter verification code: ';
    $authCode = trim(fgets(STDIN));

    // Exchange authorization code for an access token.
    $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

    // Store the credentials to disk.
    if(!file_exists(dirname($credentialsPath))) {
      mkdir(dirname($credentialsPath), 0700, true);
    }
    file_put_contents($credentialsPath, json_encode($accessToken));
    printf("Credentials saved to %s\n", $credentialsPath);
  }
  $client->setAccessToken($accessToken);

  // Refresh the token if it's expired.
  if ($client->isAccessTokenExpired()) {
    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
    file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
  }
  return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path) {
  $homeDirectory = getenv('HOME');
  if (empty($homeDirectory)) {
    $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
  }
  return str_replace('~', realpath($homeDirectory), $path);
}

// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Drive($client);

$fileMetadata = new Google_Service_Drive_DriveFile(array(
    'name' => $No));
$content = file_get_contents( __DIR__ . '\\DocumentFile\\'.$Doc);
$file = $service->files->create($fileMetadata, array(
    'data' => $content,
    'mimeType' => mime_content_type(__DIR__ . '\\DocumentFile\\'.$Doc),
    'uploadType' => 'multipart',
    'fields' => 'id'));

}

if (isset($file) AND $_FILES["DocFile"]["name"] != "") {
  //printf("File ID: %s\n", $file->id);
  $IdFile = $file->id;
  switch ($No) {
  case '01_cov':
    $sql = mysqli_query($con,"UPDATE coopproject SET 01_cov = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '02_app':
    $sql = mysqli_query($con,"UPDATE coopproject SET 02_app = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '03_absen':
    $sql = mysqli_query($con,"UPDATE coopproject SET 03_absen = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '04_ack':
    $sql = mysqli_query($con,"UPDATE coopproject SET 04_ack = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '05_tbc':
    $sql = mysqli_query($con,"UPDATE coopproject SET 05_tbc = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '06_ch1':
    $sql = mysqli_query($con,"UPDATE coopproject SET 06_ch1 = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '07_ch2':
    $sql = mysqli_query($con,"UPDATE coopproject SET 07_ch2 = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '08_ch3':
    $sql = mysqli_query($con,"UPDATE coopproject SET 08_ch3 = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '09_ch4':
    $sql = mysqli_query($con,"UPDATE coopproject SET 09_ch4 = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '10_ch5':
    $sql = mysqli_query($con,"UPDATE coopproject SET 10_ch5 = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '11_ref':
    $sql = mysqli_query($con,"UPDATE coopproject SET 11_ref = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '12_app':
    $sql = mysqli_query($con,"UPDATE coopproject SET 12_app = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;
  case '13_vit':
    $sql = mysqli_query($con,"UPDATE coopproject SET 13_vit = '$IdFile' WHERE StudentCode = '$StudentId'");
    break;  
  default:
    # code...
    break;
  }
   $response_array['status'] = 'Success';
}else{
  $response_array['status'] = 'Fail';

}
exit(json_encode($response_array));

?>
