<?php include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        if (isset($_GET['Id'])) {

            $Id = $_GET['Id'];
            $query = mysqli_query($con,"SELECT v1.Id ,
                                               v1.Name as Title ,
                                               v1.Description,
                                               v1.Skill,
                                               v1.Workplace ,
                                               v1.DateFrom,
                                               v1.DateTo,
                                               v1.Document,
                                               v1.Pay,
                                               v1.Unit,
                                               v1.IsActive,
                                               v2.Name as Establish ,v1.Created,
                                               v1.Bts_Sukumvit,
                                               v1.Bts_Seelom,
                                               v1.Mrt_Blue,
                                               v1.Mrt_Purple,
                                               v1.ARL,
                                               v1.BRT,
                                               v1.CategoryId,
                                               V1.PositionId,
                                               v3.Name As CategoryName,
                                               v4.Name AS PositionName
            FROM announce as v1
            LEFT JOIN establish as v2 ON v1.EstablishId = v2.Id
            LEFT JOIN category as v3 ON v1.CategoryId = v3.Id
            LEFT JOIN position as v4 ON v1.PositionId = v4.Id
            WHERE v1.Id = '$Id' ");
            
            // $result = mysqli_query($con,$sql); 
            $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
            
        }
        
    }
    
    mysqli_close($con);
?>
<script type="text/javascript">
    document.title = "แก้ไขข้อมูลประกาศงาน"
</script>
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }               
                }
                
            });
        
        });
    });
     function selectYear(CategoryId) {
        if (CategoryId != "-1") {
            loadTerm('position',CategoryId);
            $("#position_dropdown").html("<option value='-1'>");
        }
    };
    function loadTerm(loadType,CategoryId) {
        var dataString ='loadType='+loadType+'&loadId='+CategoryId;
        $.ajax({
            type: "POST",
            url:'./loadData.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="font-size: 18px">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                จัดการข้อมูลประกาศงาน         
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                             <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="./Search.php">จัดการข้อมูลประกาศงาน</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>แก้ไขข้อมูลประกาศงาน </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">แก้ไขข้อมูล</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานประกอบการ</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="EstablishId" id="EstablishId" class="form-control" placeholder="Establish" value="<?php if(isset($_GET['Id']) != ''){echo $row['Establish']; }?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">หัวข้อ</label>
                                                    <input type="hidden" name="func" class="form-control" value="Edit">
                                                    <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                                                    <div class="col-md-4">
                                                        <input type="text" name="Name" id="Name" class="form-control" placeholder="หัวข้อ" value="<?php if(isset($_GET['Id']) != ''){echo $row['Title']; }?>" required>
                                                    </div>
                                                    <input type="hidden" name="func" class="form-control" value="Edit">
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">รายละเอียด</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Description" name="Description" class="form-control" placeholder="รายละเอียด" value="<?php if(isset($_GET['Id']) != ''){echo $row['Description']; }?>" required>
                                                    </div>
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM category where IsActive = 1";  
                                                    $listcategory = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">ประเภทงาน</label>
                                                    <div class="col-md-4">
                                                        <select onchange="selectYear(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="CategoryId">
                                                            <?php if ($row['CategoryId'] != ''): ?>
                                                                <option value='<?php echo $row['CategoryId']?>'><?php echo $row['CategoryName']?></option>
                                                            <?php endif ?>
                                                            <?php if ($row['CategoryId'] == ''): ?>
                                                                <option value="-1">Select category</option>
                                                            <?php endif ?>
                                                            <?php while ($row3=mysqli_fetch_assoc($listcategory)) { ?>
                                                                <option value="<?php echo $row3['Id']?>"><?php echo $row3['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">ตำแหน่ง</label>
                                                    <div class="col-md-4">
                                                        <select id="position_dropdown" class="form-control select2" name="PositionId">
                                                             <?php if ($row['PositionId'] != ''): ?>
                                                                <option value='<?php echo $row['PositionId']?>'><?php echo $row['PositionName']?></option>      
                                                            <?php endif ?>  
                                                            <?php if ($row['PositionId'] == ''): ?>
                                                                <option value="-1">Select position</option>
                                                            <?php endif ?>
                                                            <span id="position_loader"></span>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ทักษะ</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Skill" name="Skill" class="form-control" placeholder="ทักษะ" value="<?php if(isset($_GET['Id']) != ''){echo $row['Skill']; }?>" required>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานที่ปฏิบัติงาน</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Workplace" name="Workplace" class="form-control" placeholder="สถานที่ปฏิบัติงาน" value="<?php if(isset($_GET['Id']) != ''){echo $row['Workplace']; }?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">การเดินทาง</label>
                                                    <div class="col-md-6">
                                                        <div class="row" style="padding-top:7px">
                                                            <div class="col-md-6">
                                                                <div class="mt-checkbox-list">
                                                                    <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวอ่อน (สายสุขุมวิท)
                                                                        <input type="checkbox" value="1" name="Bts_Sukumvit" <?php if(isset($_GET['Id']) != ''){if($row['Bts_Sukumvit'] == true){echo "checked";}else{echo "";} }?>>
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีน้ำเงิน
                                                                        <input type="checkbox" value="1" name="Mrt_Blue" <?php if(isset($_GET['Id']) != ''){if($row['Mrt_Blue'] == true){echo "checked";}else{echo "";} }?>>
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> ARL
                                                                        <input type="checkbox" value="1" name="ARL" <?php if(isset($_GET['Id']) != ''){if($row['ARL'] == true){echo "checked";}else{echo "";} }?>>
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                             </div>
                                                                
                                                             <div class="col-md-6">
                                                                <div class="mt-checkbox-list">
                                                                    <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวเข้ม (สายสีลม)
                                                                        <input type="checkbox" value="1" name="Bts_Seelom" <?php if(isset($_GET['Id']) != ''){if($row['Bts_Seelom'] == true){echo "checked";}else{echo "";} }?>>
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีม่วง 
                                                                        <input type="checkbox" value="1" name="Mrt_Purple" <?php if(isset($_GET['Id']) != ''){if($row['Mrt_Purple'] == true){echo "checked";}else{echo "";} }?>>
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> BRT
                                                                        <input type="checkbox" value="1" name="BRT" <?php if(isset($_GET['Id']) != ''){if($row['BRT'] == true){echo "checked";}else{echo "";} }?>>
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                             </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">อัตรา</label>
                                                    <div class="col-md-4">
                                                        <input type="number" id="Unit" name="Unit" class="form-control" placeholder="อัตรา" value="<?php if(isset($_GET['Id']) != ''){echo $row['Unit']; }?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">วันที่เริ่มรับสมัคร</label>
                                                    <div class="col-md-4">
                                                        <input type="date" id="DateFrom" name="DateFrom" class="form-control" placeholder="วันที่เริ่มรับสมัคร" value="<?php if(isset($_GET['Id']) != ''){echo $row['DateFrom']; }?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">วันที่สิ้นสุดการรับสมัคร</label>
                                                    <div class="col-md-4">
                                                        <input type="date" id="DateTo" name="DateTo" class="form-control" placeholder="วันที่สิ้นสุดการรับสมัคร" value="<?php if(isset($_GET['Id']) != ''){echo $row['DateTo']; }?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ค่าตอบแทน</label>
                                                    <div class="col-md-4">
                                                        <input type="number" id="Pay" name="Pay" class="form-control" placeholder="ค่าตอบแทน" value="<?php if(isset($_GET['Id']) != ''){echo $row['Pay']; }?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">เอกสาร</label>
                                                    <div class="col-md-4">
                                                        <textarea type="text" rows="3" id="Document" name="Document" class="form-control" placeholder="เอกสาร" required><?php if(isset($_GET['Id']) != ''){echo $row['Document']; }?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานะการใช้งาน</label>
                                                    <div class="col-md-4">
                                                        <div class="md-radio-inline">
                                                            <div class="md-radio">
                                                                <input type="radio" value="1" name="IsActive" id="radio6" name="radio2" class="md-radiobtn" <?php if(isset($_GET['Id']) != ''){if($row['IsActive'] == true){echo "checked";} }?> >
                                                                <label for="radio6">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ใช้งาน </label>
                                                            </div>
                                                            <div class="md-radio">
                                                                <input type="radio" value="0" name="IsActive" id="radio7" name="radio2" class="md-radiobtn" <?php if(isset($_GET['Id']) != ''){if($row['IsActive'] == false){echo "checked";} }?>>
                                                                <label for="radio7">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ไม่ใช้งาน </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">บันทึก</button>
                                                        <a type="button" href="Search.php" class="btn default">กลับ</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>