<?php include '../_Master/_header.php'; ?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php");
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }           
                }
                
            });
        
        });
    });
    function selectYear(CategoryId) {
        if (CategoryId != "-1") {
            loadTerm('position',CategoryId);
            $("#position_dropdown").html("<option value='-1'>");
        }
    };
    function loadTerm(loadType,CategoryId) {
        var dataString ='loadType='+loadType+'&loadId='+CategoryId;
        $.ajax({
            type: "POST",
            url:'./loadData.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<script type="text/javascript">
    document.title = "เพิ่มข้อมูลประกาศงาน"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                จัดการข้อมูลประกาศงาน       
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                             <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="./Search.php">จัดการข้อมูลประกาศงาน</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>เพิ่มข้อมูลประกาศงาน </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">สร้างข้อมูล</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';
                                                    $Ac_Id = base64_decode($Id);
                                                    if ($Role == 0) {
                                                        $sql = "SELECT * FROM establish Where IsActive = 1 order by Name ASC ";  
                                                    }else{
                                                        $sql = "SELECT * FROM establish Where IsActive = 1 AND AccountId = '$Ac_Id' order by Name ASC ";  
                                                    }
                                                    $listfaculty = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">สถานประกอบการ</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="EstablishId">
                                                            <?php while ($row2=mysqli_fetch_assoc($listfaculty)) { ?>
                                                                <option value=<?php echo $row2['Id']?>><?php echo $row2['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">หัวข้อ</label>
                                                    <input type="hidden" name="func" class="form-control" value="Create">
                                                    <div class="col-md-4">
                                                        <input type="text" name="Name" id="Name" class="form-control" placeholder="หัวข้อ" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">รายละเอียด</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Description" name="Description" class="form-control" placeholder="รายละเอียด" required>
                                                    </div>
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM category where IsActive = 1";  
                                                    $listcategory = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">ประเภทงาน</label>
                                                    <div class="col-md-4">
                                                        <select onchange="selectYear(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="CategoryId">
                                                            <option value="-1">Select Category</option>
                                                            <?php while ($row3=mysqli_fetch_assoc($listcategory)) { ?>
                                                                <option value="<?php echo $row3['Id']?>"><?php echo $row3['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">ตำแหน่ง</label>
                                                    <div class="col-md-4">
                                                        <select id="position_dropdown" class="form-control select2" name="PositionId">
                                                            <option value="-1">Select Position</option>
                                                            <span id="position_loader"></span>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ทักษะ</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Skill" name="Skill" class="form-control" placeholder="ทักษะ" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานที่ปฏิบัติงาน</label>
                                                    <div class="col-md-4">
                                                        <input type="text" id="Workplace" name="Workplace" class="form-control" placeholder="สถานที่ปฏิบัติงาน" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">การเดินทาง</label>
                                                    <div class="col-md-6">
                                                        <div class="row" style="padding-top:7px">
                                                            <div class="col-md-6">
                                                                <div class="mt-checkbox-list">
                                                                    <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวอ่อน (สายสุขุมวิท)
                                                                        <input type="checkbox" value="1" name="Bts_Sukumvit">
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีน้ำเงิน
                                                                        <input type="checkbox" value="1" name="Mrt_Blue">
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> ARL
                                                                        <input type="checkbox" value="1" name="ARL">
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                             </div>
                                                                
                                                             <div class="col-md-6">
                                                                <div class="mt-checkbox-list">
                                                                    <label class="mt-checkbox mt-checkbox-outline"> BTS สายสีเขียวเข้ม (สายสีลม)
                                                                        <input type="checkbox" value="1" name="Bts_Seelom">
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> MRT สายสีม่วง 
                                                                        <input type="checkbox" value="1" name="Mrt_Purple">
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="mt-checkbox mt-checkbox-outline"> BRT
                                                                        <input type="checkbox" value="1" name="BRT">
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                             </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">อัตรา</label>
                                                    <div class="col-md-4">
                                                        <input type="number" min="0" id="Unit" name="Unit" class="form-control" placeholder="อัตรา" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">วันที่เริ่มรับสมัคร</label>
                                                    <div class="col-md-4">
                                                        <input type="date" id="DateFrom" name="DateFrom" class="form-control" placeholder="วันที่เริ่มรับสมัคร" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">วันที่สิ้นสุดการรับสมัคร</label>
                                                    <div class="col-md-4">
                                                        <input type="date" id="DateTo" name="DateTo" class="form-control" placeholder="วันที่สิ้นสุดการรับสมัคร" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ค่าตอบแทน</label>
                                                    <div class="col-md-4">
                                                        <input type="number" min="0" id="Pay" name="Pay" class="form-control" placeholder="ค่าตอบแทน" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">เอกสาร</label>
                                                    <div class="col-md-4">
                                                        <textarea type="text" rows="3" id="Unit" name="Document" class="form-control" placeholder="Document" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานะการใช้งาน</label>
                                                    <div class="col-md-4">
                                                        <div class="md-radio-inline">
                                                            <div class="md-radio">
                                                                <input type="radio" value="1" name="IsActive" id="radio6" name="radio2" class="md-radiobtn" checked>
                                                                <label for="radio6">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ใช้งาน </label>
                                                            </div>
                                                            <div class="md-radio">
                                                                <input type="radio" value="0" name="IsActive" id="radio7" name="radio2" class="md-radiobtn" >
                                                                <label for="radio7">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ไม่ใช้งาน </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">บันทึก</button>
                                                        <a type="button" href="Search.php" class="btn default">ยกเลิก</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>