<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';

    //Search
    if ( (isset($_GET['Name']) != '' || isset($_GET['EstablishId']) != '')) {
        if (isset($_GET['Name']) != '' ) {
            $Name = "v1.Name LIKE '%" . $_GET['Name'] . "%'";
        }else{
            $Name = "";
        }
        if (isset($_GET['EstablishId']) != "") {
            $EstablishId = "v1.EstablishId LIKE'%" . $_GET['EstablishId'] . "%'";
        }
        else{
            $EstablishId = "";
        }
        $sql = "SELECT v1.Id ,v1.Name as Title , v1.Description,v1.Position ,v1.Skill,v1.Workplace , v1.DateFrom,v1.DateTo,v1.IsActive,v2.Name as Establish ,v1.Created
             FROM announce as v1 INNER JOIN establish as v2 ON v1.EstablishId = v2.Id WHERE $EstablishId AND $Name";  
        $result = mysqli_query($con,$sql); 
    }else{
        $sql = "SELECT v1.Id ,v1.Name as Title , v1.Description , v1.Position ,v1.Skill,v1.Workplace , v1.DateFrom,v1.DateTo,v1.IsActive,v2.Name as Establish ,v1.Created
             FROM announce as v1 INNER JOIN establish as v2 ON v1.EstablishId = v2.Id ";  
        $result = mysqli_query($con,$sql); 
    }   
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<script type="text/javascript">
    document.title = "จัดการข้อมูลประกาศงาน"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                จัดการข้อมูลประกาศงาน     
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>จัดการข้อมูลประกาศงาน</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                         <!-- BEGIN PAGE SEARCH INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-search font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">ค้นหา</span>
                                        </div>
                                    <div class="actions">
                                        <a class="btn yellow" href="Create.php">สร้างข้อมูล</a>
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';
                                                    $Ac_Id = base64_decode($Id);
                                                    if ($Role == 0) {
                                                        $sql = "SELECT * FROM establish Where IsActive = 1 order by Name ASC ";  
                                                    }else{
                                                        $sql = "SELECT * FROM establish Where IsActive = 1 AND AccountId = '$Ac_Id' order by Name ASC ";  
                                                    }
                                                    $listfaculty = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">สถานประกอบการ</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="EstablishId">
                                                            <?php while ($row2=mysqli_fetch_assoc($listfaculty)) { ?>
                                                                <option value=<?php echo $row2['Id']?>><?php echo $row2['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">หัวข้อ</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Name" class="form-control" placeholder="Title">
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn blue">ค้นหา</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                           
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE SEARCH INNER -->
                        <!-- BEGIN PAGE TABLE INNER -->
                        <div class="page-content-inner">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-table font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">ผลการค้นหา</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> สถานประกอบการ </th>
                                                <th> หัวข้อ </th>
                                                <th> รายละเอียด </th>
                                                <th> วันที่เริ่มรับสมัคร </th>
                                                <th> วันที่สิ้นสุดการรับสมัคร </th>
                                                <th> สถานะการใช้งาน </th>
                                                <th> วันที่สร้างข้อมูล </th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  $i = 1;
                                                while ($row = mysqli_fetch_assoc($result)) {  
                                            ?>
                                            <tr>  
                                                <td><?php echo $row["Establish"]; ?></td> 
                                                <td><?php echo $row["Title"]; ?></td> 
                                                <td><?php echo $row["Description"];?></td>
                                                <td><?php echo $row["DateFrom"];?></td>
                                                <td><?php echo $row["DateTo"];?></td>
                                                <td><?php echo $row["IsActive"] == 1 ? 'ใช้งาน':'ไม่ใช้งาน';?></td>
                                                <td><?php echo $row["Created"];?></td>  
                                                <td>
                                                    <?php echo '<a class="btn-xs btn  green"  href="Edit.php?Id='. $row['Id'] .'" >แก้ไข</a>'; ?>
                                                    <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModal'. $row['Id'] .'"  >ลบ</a></td>'; ?>
                                                </td>                           
                                                <!-- Modal Delete-->
                                                <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog modal-sm" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header" style="background-color: #f9243f;">
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                          <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                          ลบ <?php echo $row['Title']; ?>
                                                        </div>
                                                        <div class="modal-footer">
                                                          <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Delete">ตกลง</a>
                                                          <a class="btn default" data-dismiss="modal">ยกเลิก</a>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal Delete-->
                                                </div>
                                                <!-- Modal Delete-->                     
                                            </tr>
                                            <?php  
                                                };  
                                            ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE TABLE INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>