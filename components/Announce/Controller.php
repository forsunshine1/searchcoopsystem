<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Create':
			Create();
			break;
		case 'Delete':
			Delete();
			break;
		case 'Edit':
			Edit();
			break;	
		default:
			# code...
			break;
	}

	function Create()
	{
		include '../../PHP/ConnectDB.php';
		$EstablishId = $_POST['EstablishId'];
		$Name = $_POST['Name'];
		$Description = $_POST['Description'];
		$Skill = $_POST['Skill'];
		$Workplace = $_POST['Workplace'];
		$DateFrom = $_POST['DateFrom'];
		$DateTo = $_POST['DateTo'];
		$IsActive = $_POST['IsActive'];
		$Unit = $_POST['Unit'];
		$Pay = $_POST['Pay'];
		$CategoryId = $_POST['CategoryId'];
		$PositionId = $_POST['PositionId'];
		$Bts_Sukumvit = isset($_POST['Bts_Sukumvit']) == '' ? 0 : $_POST['Bts_Sukumvit'];
		$Bts_Seelom = isset($_POST['Bts_Seelom']) == '' ? 0 : $_POST['Bts_Seelom'];
		$Mrt_Blue = isset($_POST['Mrt_Blue']) == '' ? 0 : $_POST['Mrt_Blue'];
		$Mrt_Purple = isset($_POST['Mrt_Purple']) == '' ? 0 : $_POST['Mrt_Purple'];
		$ARL = isset($_POST['ARL']) == '' ? 0 : $_POST['ARL'];
		$BRT = isset($_POST['BRT']) == '' ? 0 : $_POST['BRT'];
		$Document = $_POST['Document'];
		if(isset($_POST['Name']))
		{
			$sql = "INSERT INTO announce (EstablishId,
											Name,
											Description,
											Skill,
											Workplace,
											DateFrom,
											DateTo,
											CategoryId,
											PositionId,
											Bts_Sukumvit,
											Bts_Seelom,
											Mrt_Blue,
											Mrt_Purple,
											ARL,
											BRT,
											IsActive,
											Unit,
											Pay,
											Document)
									VALUES ('$EstablishId',
											'$Name',
											'$Description',
											'$Skill',
											'$Workplace',
											'$DateFrom',
											'$DateTo',
											'$CategoryId',
											'$PositionId',
											'$Bts_Sukumvit',
											'$Bts_Seelom',
											'$Mrt_Blue',
											'$Mrt_Purple',
											'$ARL',
											'$BRT',
											'$IsActive',
											'$Unit',
											'$Pay',
											'$Document')";

			mysqli_query($con, $sql);
			$response_array['status'] = 'success';  
		}else{
		    
		    $response_array['status'] = 'error';
		} 
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$Name = $_POST['Name'];
		$Description = $_POST['Description'];
		$Skill = $_POST['Skill'];
		$Workplace = $_POST['Workplace'];
		$DateFrom = $_POST['DateFrom'];
		$DateTo = $_POST['DateTo'];
		$IsActive = $_POST['IsActive'];
		//UPDATE
		$Document = $_POST['Document'];
		$Unit = $_POST['Unit'];
		$Pay = $_POST['Pay'];
		$CategoryId = $_POST['CategoryId'];
		$PositionId = $_POST['PositionId'];
		
		
		
		if (isset($_POST['Bts_Sukumvit'])) {
			$Bts_Sukumvit = $_POST['Bts_Sukumvit'] == '' ? 0 : $_POST['Bts_Sukumvit'];
		}else{
			$Bts_Sukumvit= 0;
		}
		if (isset($_POST['Bts_Seelom'])) {
			$Bts_Seelom = $_POST['Bts_Seelom'] == '' ? 0 : $_POST['Bts_Seelom'];
		}else{
			$Bts_Seelom= 0;
		}
		if (isset($_POST['Mrt_Blue'])) {
			$Mrt_Blue = $_POST['Mrt_Blue'] == '' ? 0 : $_POST['Mrt_Blue'];
		}else{
			$Mrt_Blue= 0;
		}
		if (isset($_POST['Mrt_Purple'])) {
			$Mrt_Purple = $_POST['Mrt_Purple'] == '' ? 0 : $_POST['Mrt_Purple'];
		}else{
			$Mrt_Purple= 0;
		}
		if (isset($_POST['ARL'])) {
			$ARL = $_POST['ARL'] == '' ? 0 : $_POST['ARL'];
		}else{
			$ARL= 0;
		}
		if (isset($_POST['BRT'])) {
			$BRT = $_POST['BRT'] == '' ? 0 : $_POST['BRT'];
		}else{
			$BRT= 0;
		}

		if (isset($_POST['Id'])) {

			$Id = $_POST['Id'];
			$sql = mysqli_query($con,"UPDATE announce SET Name = '$Name',
														Description = '$Description',
														Skill = '$Skill' , 
														Workplace = '$Workplace' ,
														DateFrom = '$DateFrom',
														DateTo = '$DateTo', 
														CategoryId = '$CategoryId',
														PositionId = '$PositionId',
														Bts_Sukumvit = '$Bts_Sukumvit', 
														Bts_Seelom = '$Bts_Seelom', 
														Mrt_Blue = '$Mrt_Blue', 
														Mrt_Purple = '$Mrt_Purple', 
														ARL = '$ARL', 
														BRT = '$BRT', 
														IsActive = '$IsActive',
														Unit = '$Unit',
														Pay = '$Pay',
														Document = '$Document'
														WHERE Id='$Id'");
				$response_array['status'] = 'success';  

		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Delete()
	{
		include '../../PHP/ConnectDB.php';
		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = "DELETE FROM announce WHERE Id='$Id'";

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	exit(json_encode($response_array));
 ?>