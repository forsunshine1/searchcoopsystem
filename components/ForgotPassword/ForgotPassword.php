<?php include '../_Master/_header.php'; ?>
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'Save success';
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'Save failed';
                        $('#myModalAlert').modal('show');  
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'Duplicate';
                        $('#myModalAlert').modal('show'); 
                        $('$Link').attr("data-dismiss","modal");                         
                    }       
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "ลืมรหัสผ่าน"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ลืมรหัสผ่าน        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ลืมรหัสผ่าน</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                         <!-- BEGIN PAGE SEARCH INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-lock font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">ลืมรหัสผ่าน</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อผู้ใช้/รหัสนักศึกษา</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Username" class="form-control" placeholder="ชื่อผู้ใช้/รหัสนักศึกษา" required>
                                                    </div>
                                                    <input type="hidden" name="func" class="form-control" value="ResetPassword">
                                                </div>                                                          
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">อีเมล</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Email" class="form-control" placeholder="อีเมล" required>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-4 text-center">
                                                            <button type="submit" class="btn green">ลืมรหัสผ่าน</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                           
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE SEARCH INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>