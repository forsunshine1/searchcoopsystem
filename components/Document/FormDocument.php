<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';

    $sql = "SELECT * FROM document";  
    $result = mysqli_query($con,$sql); 
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<style type="text/css">
    .table>tbody>tr>td{
        line-height: 3 !important;
    }
</style>
<script type="text/javascript">
    document.title = "ดาวน์โหลดแบบฟอร์ม"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ดาวน์โหลดแบบฟอร์ม
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ดาวน์โหลดแบบฟอร์ม</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE TABLE INNER -->
                        <div class="page-content-inner">
                            <div class="portlet light bordered">
                                <div class="portlet-body">                                    
                                    <table class="table table-striped" >  
                                        <tbody>
                                            <?php  $i = 1;
                                                while ($row = mysqli_fetch_assoc($result)) {  
                                            ?>
                                           <tr>  
                                                <td style="display: none;" align="center" ><?php echo $i;$i++;; ?></td> 
                                                <td><?php echo $row["Code"]; ?></td>  
                                                <td><?php echo $row["Description"]; ?></td>
                                                <td><a target="_blank" href="<?php echo $row["Url"]; ?>">[ดาวน์โหลด]</a></td>                                                           
                                            </tr>
                                            <?php  
                                                };  
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="share"></div>
                            </div>
                        </div>
                        <!-- END PAGE TABLE INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<script>
    $("#share").jsSocials({
        shareIn: "popup",
        shares: ["twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon"]
    });
</script>
<?php include '../_Master/_footer.php'; ?>