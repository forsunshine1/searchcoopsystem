<?php 
    include '../_Master/_header.php';
?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    //Search
    if (isset($_GET['Username']) ) {

        $Username = base64_decode($_GET['Username']);
        $query = mysqli_query($con,"SELECT CO.Id ,
                                        CO.StudentCode,
                                        CO.EstablishId,
                                        CO.PositionId,
                                        position.Name AS Position,
                                        CO.MentorName,
                                        CO.MentorPosition,
                                        CO.MentorPhoneNo,
                                        CO.Pay,
                                        CO.TermId,
                                        ES.Name as EstablishName, 
                                        term.TermNo,
                                        listyear.YearNo,
                                        Co.YearId,
                                        CO.ProjectTitle
                                        
        FROM coopinfo AS CO 
        LEFT JOIN establish AS ES ON CO.EstablishId = ES.Id 
        LEFT JOIN term ON CO.TermId = term.Id
        LEFT JOIN position ON position.Id = CO.PositionId
        LEFT JOIN listyear ON CO.YearId = listyear.Id WHERE CO.StudentCode = '$Username'");
            
            // $result = mysqli_query($con,$sql); 
        $row = mysqli_fetch_array($query,MYSQLI_ASSOC); 
    }
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<!-- POST  -->
<script type="text/javascript">
    
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }        
                }
                
            });
        
        });
    });
    function showEdit() {
        $('#View').hide();
        $('#Edit').show();
    };

    function selectYear(YearId) {
        if (YearId != "-1") {
            loadTerm('term',YearId);
            $("#term_dropdown").html("<option value='-1'>");
        }
    };
    function loadTerm(loadType,YearId) {
        var dataString ='loadType='+loadType+'&loadId='+YearId;
        $.ajax({
            type: "POST",
            url:'./load_term.php',
            data:dataString,
            cache:false,
            success:function (result) {
                $("#"+loadType+"_loader").hide();
                $("#"+loadType+"_dropdown").html("<option value='-1'>Select "+ loadType +"</option>");
                $("#"+loadType+"_dropdown").append(result);
            }
        });
    }
</script>
<script type="text/javascript">
    document.title = "ข้อมูลสหกิจศึกษา"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ข้อมูลสหกิจศึกษา 
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ข้อมูลสหกิจศึกษา </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">ข้อมูลสหกิจศึกษา</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form" id="View">
                                        <!-- BEGIN FORM-->
                                        <div class="form-horizontal">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานประกอบการ</label>
                                                    <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['EstablishName']; ?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ปีการศึกษา</label>
                                                    <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['YearNo']; ?></label>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">ภาคการศึกษา</label>
                                                    <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['TermNo']; ?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ตำแหน่ง</label>
                                                    <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['Position']; ?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อพนักงานที่ปรึกษา</label>
                                                    <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['MentorName']; ?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ตำแหน่งพนักงานที่ปรึกษา</label>
                                                     <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label " style="text-align: left !important;"><?php echo $row['MentorPosition']; ?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">เบอร์โทรศัพท์พนักงานที่ปรึกษา</label>
                                                     <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['MentorPhoneNo']; ?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ค่าตอบแทน</label>
                                                      <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['Pay']; ?></label>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">หัวข้อโครงงานสหกิจศึกษา</label>
                                                      <div class="col-md-4 text-left">
                                                        <label class="col-md-12 control-label" style="text-align: left !important;"><?php echo $row['ProjectTitle']; ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button onclick="showEdit()" class="btn green">แก้ไข</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                    <div class="portlet-body form" id="Edit" style="display: none;">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM establish order by Name ASC ";  
                                                    $listes = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">สถานประกอบการ</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="EstablishId">
                                                            <option value=<?php echo $row['EstablishId']?>><?php echo $row['EstablishName']; ?></option >
                                                            <?php while ($row2=mysqli_fetch_assoc($listes)) { ?>
                                                                <?php if ($row['EstablishId'] != $row2['Id']): ?>
                                                                    <option value=<?php echo $row2['Id']?>><?php echo $row2['Name']?></option>
                                                                <?php endif ?>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM listyear order by YearNo ASC ";  
                                                    $listterm = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">ปีการศึกษา</label>
                                                    <div class="col-md-4">
                                                        <select onchange="selectYear(this.options[this.selectedIndex].value)" id="single" class="form-control select2" name="YearId">
                                                            <?php if ($row['YearId'] != ''): ?>
                                                                <option value='<?php echo $row['YearId']?>'><?php echo $row['YearNo']?></option>
                                                            <?php endif ?>
                                                            <?php if ($row['YearId'] == ''): ?>
                                                                <option value="-1">Select Year</option>
                                                            <?php endif ?>
                                                            <?php while ($row2=mysqli_fetch_assoc($listterm)) { ?>
                                                                <option value="<?php echo $row2['Id']?>"><?php echo $row2['YearNo']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">ภาคการศึกษา</label>
                                                    <div class="col-md-4">
                                                        <select id="term_dropdown" class="form-control select2" name="TermId">
                                                            <?php if ($row['TermId'] != ''): ?>
                                                                <option value='<?php echo $row['TermId']?>'><?php echo $row['TermNo']?></option>      
                                                            <?php endif ?>  
                                                            <?php if ($row['TermId'] == ''): ?>
                                                                <option value="-1">Select Term</option>
                                                            <?php endif ?>
                                                            
                                                            <span id="term_loader"></span>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT * FROM position order by Name ASC ";  
                                                    $listterm = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">ตำแหน่ง</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="PositionId">
                                                            <?php while ($row3=mysqli_fetch_assoc($listterm)) { ?>
                                                                <option value="<?php echo $row3['Id']?>"><?php echo $row3['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อพนักงานที่ปรึกษา</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="MentorName" class="form-control" placeholder="ชื่อพนักงานที่ปรึกษา" value="<?php echo $row['MentorName']; ?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">เบอร์โทรศัพท์พนักงานที่ปรึกษา</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="MentorPhoneNo" class="form-control" placeholder="เบอร์โทรศัพท์พนักงานที่ปรึกษา" value="<?php echo $row['MentorPhoneNo']; ?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ตำแหน่งพนักงานที่ปรึกษา</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="MentorPosition" class="form-control" placeholder="ตำแหน่งพนักงานที่ปรึกษา" value="<?php echo $row['MentorPosition']; ?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">เบี้ยเลี้ยง/วัน</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Pay" class="form-control" placeholder="เบี้ยเลี้ยง/วัน" value="<?php echo $row['Pay']; ?>" required>
                                                    </div>
                                                    <input type="hidden" name="func" class="form-control" value="Edit">
                                                    <input type="hidden" name="Id" class="form-control" value="<?php echo $row['Id']; ?>">
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">หัวข้อโครงงานสหกิจศึกษา</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="ProjectTitle" class="form-control" placeholder="หัวข้อโครงงานสหกิจศึกษา" value="<?php echo $row['ProjectTitle']; ?>" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">บันทึก</button>
                                                        <a type="button" href="./CoopInfo.php?Username=<?php echo base64_encode($Username);?>" class="btn default">ยกเลิก</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>