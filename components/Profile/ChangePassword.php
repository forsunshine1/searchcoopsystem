<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    //Search
    if (isset($_GET['Username']) ) {

        $Username = base64_decode($_GET['Username']);
            $query = mysqli_query($con,"SELECT AC.Id,AC.Username,AC.Firstname , AC.Lastname , AC.Gender,AC.Address,AC.Email,AC.Tel,AC.Gpa,AC.Avatar, BH.Name as Branch , FC.Name as Faculty ,AC.Fax
                                        FROM accounts AS AC
                                        LEFT JOIN  branch AS BH ON  AC.BranchId = BH.Id
                                        LEFT JOIN  faculty AS FC ON BH.FacultyId = FC.Id 
                                        WHERE  AC.Username = '$Username'");
            
            // $result = mysqli_query($con,$sql); 
        $row = mysqli_fetch_array($query,MYSQLI_ASSOC); 
    }
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<script type="text/javascript">
    $(function () {
        $('#veFormPassword').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');              
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'รหัสผ่านปัจจุบันหรือรหัสผ่านใหม่ไม่ถูกต้อง';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                             
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'รหัสผ่านปัจจุบันหรือรหัสผ่านใหม่ไม่ถูกต้อง';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                              
                    }            
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "เปลี่ยนรหัสผ่าน"
</script>
 <div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ข้อมูลสว่นตัว  
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                             <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>เปลี่ยนรหัสผ่าน</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                          <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE SIDEBAR -->
                                    <?php include './AccountSetting.php'; ?>
                                    <!-- END BEGIN PROFILE SIDEBAR -->
                                    <!-- BEGIN PROFILE CONTENT -->
                                   <div class="profile-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light ">
                                                    <div class="portlet-title tabbable-line">
                                                        <div class="caption caption-md">
                                                            <i class="icon-globe theme-font hide"></i>
                                                            <span class="caption-subject font-blue-madison bold uppercase">เปลี่ยนรหัสผ่าน </span>
                                                        </div>
                                                        <ul class="nav nav-tabs">
                                                            <li class="active">
                                                                <a href="#tab_1_1" data-toggle="tab">ข้อมูล</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="tab-content">
                                                            <!-- PERSONAL INFO TAB -->
                                                            <div class="tab-pane active" id="tab_1_1">
                                                                <form id="veFormPassword">
                                                                    <div class="form-group">
                                                                        <input type="hidden" name="func" class="form-control" value="Password">
                                                                        <label class="control-label">รหัสผ่านเดิม</label>
                                                                        <input type="password" id="oldPassword" name="oldPassword" class="form-control" required/> </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">รหัสผ่านใหม่</label>
                                                                        <input type="password" id="newPassword" name="newPassword" class="form-control" required/> </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">ยืนยันรหัสผ่านใหม่</label>
                                                                        <input type="password" id="newPassword2" name="newPassword2" class="form-control" required/> 
                                                                    </div>
                                                                        <input type="hidden" name="Id" class="form-control" value="<?php echo $row['Id'] ?>" required>
                                                                        <input type="hidden" name="Username" value="<?php echo $Username; ?>">
                                                                    <div class="margin-top-10">
                                                                         <button class="btn green"> เปลี่ยนรหัสผ่าน </button>
                                                                    </div>
                                                                </form>       
                                                            </div>
                                                            <!-- END PERSONAL INFO TAB -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>