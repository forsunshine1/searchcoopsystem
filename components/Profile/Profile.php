<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    //Search
    if (isset($_GET['Username']) ) {

        $Username = base64_decode($_GET['Username']);
            $query = mysqli_query($con,"SELECT AC.Id,AC.Username,AC.Firstname , AC.Lastname , AC.Gender,AC.Address,AC.Email,AC.Tel,AC.Gpa,AC.Avatar, BH.Name as Branch , FC.Name as Faculty ,AC.Fax
                                        FROM accounts AS AC
                                        LEFT JOIN  branch AS BH ON  AC.BranchId = BH.Id
                                        LEFT JOIN  faculty AS FC ON BH.FacultyId = FC.Id 
                                        WHERE  AC.Username = '$Username'");
            
            // $result = mysqli_query($con,$sql); 
        $row = mysqli_fetch_array($query,MYSQLI_ASSOC); 
    }
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<script type="text/javascript">
    document.title = "ข้อมูลส่วนตัว"
</script>
 <div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>ข้อมูลส่วนตัว
                               
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ข้อมูลส่วนตัว</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                          <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE SIDEBAR -->
                                    <?php include './AccountSetting.php'; ?>
                                    <!-- END BEGIN PROFILE SIDEBAR -->
                                    <!-- BEGIN PROFILE CONTENT -->
                                    <div class="profile-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light ">
                                                    <div class="portlet-title tabbable-line">
                                                        <div class="caption caption-md">
                                                            <i class="icon-globe theme-font hide"></i>
                                                            <span class="caption-subject font-blue-madison bold uppercase">ข้อมูลส่วนตัว</span>
                                                        </div>
                                                        <ul class="nav nav-tabs">
                                                            <li class="active">
                                                                <a href="#tab_1_1" data-toggle="tab">ข้อมูล</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="tab-content">
                                                            <!-- PERSONAL INFO TAB -->
                                                            <div class="tab-pane active" id="tab_1_1">               
                                                                <form role="form" id="veForm">
                                                                    <div class="form-group">
                                                                        <label class="control-label"><label class="control-label"><?php if($Role == 1){echo "Student Code";}else{echo "Username";} ?></label></label>
                                                                        <div class="">
                                                                            <input type="text" class="form-control" placeholder="<?php echo $row['Username'] ?>" value="<?php echo $row['Username'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">ชื่อ</label>
                                                                        <div class="">
                                                                            <input type="text" class="form-control" name="Firstname" placeholder="<?php echo $row['Firstname'] ?>" value="<?php echo $row['Firstname'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">นามสกุล</label>
                                                                        <div class="">
                                                                            <input type="text" name="Lastname" class="form-control" placeholder="<?php echo $row['Lastname'] ?>" value="<?php echo $row['Lastname'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                     <div class="form-group">
                                                                        <label class="control-label">เพส</label>
                                                                        <div class="md-radio-list">
                                                                            <div class="">
                                                                                <input type="radio" value="0" id="radio1" name="Gender" class="md-radiobtn" <?php if($row['Gender'] == 0){echo "checked";}?> readonly>
                                                                                <label for="radio1">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> ชาย </label>
                                                                            </div>
                                                                            <div class="">
                                                                                <input type="radio" value="1" id="radio2" name="Gender" class="md-radiobtn" <?php if($row['Gender'] == 1){echo "checked";}?> readonly>
                                                                                <label for="radio2">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> หญิง </label>
                                                                            </div>         
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">ที่อยู่</label>
                                                                        <div class="">
                                                                            <textarea type="text" name="Address" rows="4" class="form-control" readonly><?php echo $row['Address']; ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">อีเมล</label>
                                                                        <div class="">
                                                                            <input type="email" name="Email" class="form-control" placeholder="<?php echo $row['Email'] ?>" value="<?php echo $row['Email'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">เบอร์โทรศัพท์</label>
                                                                        <div class="">
                                                                            <input type="text" name="PhoneNo" class="form-control" placeholder="<?php echo $row['Tel'] ?>" value="<?php echo $row['Tel'] ?>" readonly>
                                                                            <input type="hidden" name="Id" class="form-control" value="<?php echo $row['Id'] ?>" required>
                                                                            <input type="hidden" name="ve" class="form-control" value="profile" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">แฟ็กซ์</label>
                                                                        <div class="">
                                                                            <input type="text" name="Fax" class="form-control" placeholder="<?php echo $row['Fax'] ?>" value="<?php echo $row['Fax'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">GPA</label>
                                                                        <div class="">
                                                                            <input type="text" name="Gpa" class="form-control" placeholder="<?php echo $row['Gpa'] ?>" value="<?php echo $row['Gpa'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <a class="btn blue" href="../Home/index.php">หน้าหลัก</a>
                                                                       <a class="btn green" href="../Profile/Edit.php?Username=<?php echo base64_encode($row['Username']);?>">แก้ไขข้อมูลส่วนตัว</a>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!-- END PERSONAL INFO TAB -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>