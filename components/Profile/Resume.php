<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    //Search
    if (isset($_GET['Username']) ) {

        $Username = base64_decode($_GET['Username']);
            $query = mysqli_query($con,"SELECT AC.Id,AC.Username,AC.Firstname , AC.Lastname , AC.Gender,AC.Address,AC.Email,AC.Tel,AC.Gpa,AC.Avatar, BH.Name as Branch , FC.Name as Faculty ,AC.Fax
                                        FROM accounts AS AC
                                        LEFT JOIN  branch AS BH ON  AC.BranchId = BH.Id
                                        LEFT JOIN  faculty AS FC ON BH.FacultyId = FC.Id 
                                        WHERE  AC.Username = '$Username'");
            
            // $result = mysqli_query($con,$sql); 
        $row = mysqli_fetch_array($query,MYSQLI_ASSOC); 
    }
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<script type="text/javascript">
    $(function () {
        $('#editResume').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }               
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "เรซูเม่"
</script>
 <div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>ข้อมูลส่วนตัว
                                
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>เรซูเม่</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                          <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE SIDEBAR -->
                                    <?php include './AccountSetting.php'; ?>
                                    <!-- END BEGIN PROFILE SIDEBAR -->
                                    <!-- BEGIN PROFILE CONTENT -->
                                    <div class="profile-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light ">
                                                    <div class="portlet-title tabbable-line">
                                                        <div class="caption caption-md">
                                                            <i class="icon-globe theme-font hide"></i>
                                                            <span class="caption-subject font-blue-madison bold uppercase">เรซูเม่</span>
                                                        </div>
                                                        <ul class="nav nav-tabs">
                                                            <li class="active">
                                                                <a href="#tab_1_1" data-toggle="tab">ข้อมูล</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="tab-content">
                                                            <!-- PERSONAL INFO TAB -->
                                                            <div class="tab-pane active" id="tab_1_1">
                                                                    <?php
                                                                        include '../../PHP/ConnectDB.php';
                                                                        if (isset($_GET['Username']) ) {

                                                                            $Username = base64_decode($_GET['Username']);
                                                                                $queryResume = mysqli_query($con,"SELECT * FROM resume WHERE StudentCode = '$Username'");
                                                                                
                                                                                // $result = mysqli_query($con,$sql); 
                                                                            $Resume = mysqli_fetch_array($queryResume,MYSQLI_ASSOC); 
                                                                        }
                                                                        
                                                                        mysqli_close($con);
                                                                        //exit(json_encode($response_array));
                                                                    ?>           
                                                                <form id="editResume" class="form-horizontal">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="form-group last">
                                                                                <label class="control-label col-md-3">วัตถุประสงค์</label>
                                                                                <div class="col-md-9">
                                                                                    <textarea name="Objective" class="wysihtml5 form-control" rows="6"><?php echo $Resume['Objective']; ?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="form-group last">
                                                                                <label class="control-label col-md-3">ประสบการณ์</label>
                                                                                <div class="col-md-9">
                                                                                    <textarea name="Experience" class="wysihtml5 form-control" rows="6"><?php echo $Resume['Experience']; ?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="form-group last">
                                                                                <label class="control-label col-md-3">ทักษะ</label>
                                                                                <div class="col-md-9">
                                                                                    <textarea name="TechnicalSkills" class="wysihtml5 form-control" rows="6"><?php echo $Resume['TechnicalSkills']; ?></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="form-group last">
                                                                                <label class="control-label col-md-3">การศึกษา</label>
                                                                                <div class="col-md-9">
                                                                                    <textarea name="Education" class="wysihtml5 form-control" rows="6"><?php echo $Resume['Education']; ?></textarea>
                                                                                    <input type="hidden" name="StudentCode" value="<?php echo $Resume['StudentCode']; ?>">
                                                                                    <input type="hidden" name="func" class="form-control" value="SaveResume">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green">บันทึก</button>
                                                                                   <a type="button" href="Profile.php?Username=<?php echo base64_encode($Username);?>" class="btn default">กลับ</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>     
                                                                </form>
                                                                 <hr>
                                                                    <a target="blank" href="../../Reports/ResumeReport.php?StudentCode=<?php echo $Username; ?>" class="btn red"><i class="icon-printer icons"></i> พิมพ์</a>
                                                                    <a target="blank" href="../../assets/pages/img/example.pdf" class="btn green"><i class="icon-eye icons"></i> ตัวอย่างเรซูเม่</a> 
                                                            </div>
                                                            <!-- END PERSONAL INFO TAB -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>