<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    //Search
    if (isset($_GET['Username']) ) {

        $Username = base64_decode($_GET['Username']);
            $query = mysqli_query($con,"SELECT AC.Id,AC.Username,AC.Firstname , AC.Lastname , AC.Gender,AC.Address,AC.Email,AC.Tel,AC.Gpa,AC.Avatar, BH.Name as Branch , FC.Name as Faculty ,AC.Fax,LJ.LookingDesc,LJ.Id AS LookingJobId
                                        FROM accounts AS AC
                                        LEFT JOIN  branch AS BH ON  AC.BranchId = BH.Id
                                        LEFT JOIN  faculty AS FC ON BH.FacultyId = FC.Id 
                                        LEFT JOIN lookingjob AS LJ ON LJ.StudentCode = AC.Username
                                        WHERE  AC.Username = '$Username'");
            
            // $result = mysqli_query($con,$sql); 
        $row = mysqli_fetch_array($query,MYSQLI_ASSOC); 
    }
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<script type="text/javascript">
    
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }              
                }
                
            });
        
        });
    });
    function showEdit() {
        $('#View').hide();
        $('#addForm').show();
    };
</script>
<script type="text/javascript">
    document.title = "มองหางาน"
</script>
 <div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>ข้อมูลส่วนตัว
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>มองหางาน</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                          <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE SIDEBAR -->
                                    <?php include './AccountSetting.php'; ?>
                                    <!-- END BEGIN PROFILE SIDEBAR -->
                                    <!-- BEGIN PROFILE CONTENT -->
                                    <div class="profile-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light ">
                                                    <div class="portlet-title tabbable-line">
                                                        <div class="caption caption-md">
                                                            <i class="icon-globe theme-font hide"></i>
                                                            <span class="caption-subject font-blue-madison bold uppercase">มองหางาน</span>
                                                        </div>
                                                        <ul class="nav nav-tabs">
                                                            <li class="active">
                                                                <a href="#tab_1_1" data-toggle="tab">ข้อมูล</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="tab-content">
                                                            <!-- PERSONAL INFO TAB -->
                                                            <div class="tab-pane active" id="tab_1_1">               
                                                                <div id="Edit">
                                                                    <div class="form-horizontal" id="View">
                                                                        <div class="form-body" style="padding-top: 5%">
                                                                            <div class="form-group">
                                                                                <label for="select2-disabled-inputs-multiple" class="col-md-3 control-label"><b>รายละเอียด</b></label>
                                                                                <div class="col-md-6">
                                                                                    <label class="col-md-12 control-label " style="text-align: left !important;"><?php echo $row['LookingDesc']; ?></label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">                                               
                                                                                <div class="col-sm-12 text-center">
                                                                                     <button onclick="showEdit()" class="btn green">Edit</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <form class="form-horizontal" id="addForm" style="display: none;">
                                                                        <div class="form-body" style="padding-top: 5%">
                                                                            <div class="form-group">
                                                                                <label for="select2-disabled-inputs-multiple" class="col-md-3 control-label"><b>มองหางาน</b></label>
                                                                                <div class="col-md-6">
                                                                                    <input type="text" class="form-control" name="LookingDesc" placeholder="<?php echo $row['LookingDesc'] ?>" value="<?php echo $row['LookingDesc'] ?>" >
                                                                                </div>
                                                                                <input type="hidden" name="Id" class="form-control" value="<?php echo $row['LookingJobId'] ?>" required>
                                                                                <input type="hidden" name="func" class="form-control" value="Looking">
                                                                            </div>
                                                                            <div class="form-group">                                               
                                                                                <div class="col-sm-12 text-center">
                                                                                    <button type="submit" class="btn green">บัทึก</button>
                                                                                    <a type="button" href="./Looking.php?Username=<?php echo base64_encode($Username);?>" class="btn default">ยกเลิก</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <!-- END PERSONAL INFO TAB -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>