<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Edit':
			Edit();
			break;	
		case 'Password':
			Password();
			break;
		case 'Avatar':
			Avatar();
			break;	
	 	case 'SaveResume':
			SaveResume();
			break;
		case 'Looking':
			Looking();
			break;		
		default:
			# code...
			break;
	}

	function Looking()
	{
		include '../../PHP/ConnectDB.php';

		$LookingDesc = $_POST['LookingDesc'];

		//UPDATE
		if (isset($_POST['Id'])) {
			$Id = $_POST['Id'];
			$sql = mysqli_query($con,"UPDATE lookingjob SET LookingDesc = '$LookingDesc' WHERE Id='$Id'");
				$response_array['status'] = 'success';  
		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Password()
	{
		include '../../PHP/ConnectDB.php';
		if (isset($_POST['Id'])) {

			$Id = $_POST['Id'];
			$oldPassword= base64_encode($_POST['oldPassword']);
			$newPassword= base64_encode($_POST['newPassword']);
			$newPassword2= base64_encode($_POST['newPassword2']);

			if ($newPassword == $newPassword2) {
				$query = mysqli_query($con,"select * from accounts where Id='$Id' AND Password='$oldPassword'");
				$rows = mysqli_num_rows($query);

				if ($rows == 1) {
					$sql = mysqli_query($con,"UPDATE accounts SET Password = '$newPassword' 
						WHERE Id='$Id'");
					$response_array['status'] = 'success';
				}else{
					$response_array['status'] = 'error';
				}

			}else{
				$response_array['status'] = 'error';
			}

		}else{
			$response_array['status'] = 'error';
		}	
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Avatar()
	{
		include '../../PHP/ConnectDB.php';
		$Id=$_POST['Id'];

		if (!empty($_FILES["img_member"]) AND $_FILES["img_member"]["name"] != '') {
			if (isset($_FILES["img_member"])) {
				$temp = explode(".", $_FILES["img_member"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($temp);
				move_uploaded_file($_FILES["img_member"]["tmp_name"], "../../Avatar/" . $newfilename);
				$imgpath = $newfilename ;
				if (!empty($_FILES["img_member"]["name"]) AND $_FILES["img_member"]["name"] != '') {
					
					$valid =mysqli_query($con, "SELECT * FROM accounts WHERE Id = '$Id'");
					$validImage = mysqli_fetch_array($valid,MYSQLI_ASSOC);

					if ($validImage['Avatar'] != '' || $validImage['Avatar'] != null) {
						unlink("../../Avatar/" .$_POST['Avatar']);
					}

				}
			}else{
				$imgpath = "";
			}
			
			//ADD
			if(isset($_POST['Id']))
			{				
				 
				$sql = mysqli_query($con,"UPDATE accounts SET Avatar ='$imgpath' WHERE Id='$Id'");
				$response_array['status'] = 'success';  	

			}else{
			    
			    $response_array['status'] = 'error';
			} 
		}else{
			$response_array['status'] = 'success';  	
		}
		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$Firstname = $_POST['Firstname'];
		$Lastname = $_POST['Lastname'];
		$Gender = $_POST['Gender'];
		if (isset($_POST['Address'])) {
			$Address = $_POST['Address'];
		}
		if (isset($_POST['Gpa'])) {
			$Gpa = $_POST['Gpa'];
		}
		$Email = $_POST['Email'];
		$Tel = $_POST['Tel'];
		$Fax = $_POST['Fax'];
		//UPDATE
		if (isset($_POST['Id'])) {

			$Id = $_POST['Id'];
			$sql = mysqli_query($con,"UPDATE accounts SET Firstname = '$Firstname',Lastname = '$Lastname',Gender = '$Gender' , Address = '$Address', Gpa = '$Gpa', Email = '$Email', Tel = '$Tel', Fax = '$Fax' WHERE Id='$Id'");
				$response_array['status'] = 'success';  
		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function SaveResume()
	{
		include '../../PHP/ConnectDB.php';
		$Objective = $_POST['Objective'];
		$Experience = $_POST['Experience'];
		$TechnicalSkills = $_POST['TechnicalSkills'];
		$Education = $_POST['Education'];
		$StudentCode = $_POST['StudentCode'];
		//UPDATE
		if (isset($_POST['StudentCode'])) {

			$sql = mysqli_query($con,"UPDATE resume SET Objective = '$Objective',Experience = '$Experience',TechnicalSkills = '$TechnicalSkills' , Education = '$Education' WHERE StudentCode='$StudentCode'");
			$response_array['status'] = 'success';  
		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	exit(json_encode($response_array));
 ?>