<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';
    //Search
    if (isset($_GET['Username']) ) {

        $Username = base64_decode($_GET['Username']);
            $query = mysqli_query($con,"SELECT AC.Id,AC.Username,AC.Firstname , AC.Lastname , AC.Gender,AC.Address,AC.Email,AC.Tel,AC.Gpa,AC.Avatar, BH.Name as Branch , FC.Name as Faculty ,AC.Fax
                                        FROM accounts AS AC
                                        LEFT JOIN  branch AS BH ON  AC.BranchId = BH.Id
                                        LEFT JOIN  faculty AS FC ON BH.FacultyId = FC.Id 
                                        WHERE  AC.Username = '$Username'");
            
            // $result = mysqli_query($con,$sql); 
        $row = mysqli_fetch_array($query,MYSQLI_ASSOC); 
    }
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<style type="text/css">
    #main { 
    /* The image used */
        background-image: url("../../assets/pages/img/login/resume.jpg");

        /* Set a specific height */
        height: 500px; 

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;

    }
</style>
<script type="text/javascript">
    document.title = "เมนูหลัก"
</script>
 <div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>เมนูหลัก
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>เมนูหลัก</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                          <!-- BEGIN PAGE CONTENT INNER -->
                        <div class="page-content-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PROFILE SIDEBAR -->
                                    <?php include './AccountSetting.php'; ?>
                                    <!-- END BEGIN PROFILE SIDEBAR -->
                                    <!-- BEGIN PROFILE CONTENT -->
                                    <div class="profile-content">
                                        <div class="row">
                                            <?php if ($Role == 1): ?>
                                                <div class="col-md-12">
                                                    <div class="portlet light ">
                                                        <div class="portlet-body">
                                                            <div id="main">                                       
                                                                <div style="padding-top: 25%;text-align: center">
                                                                    <h2 style="background: white"><span style="color: #ffcc03">จัดการ</span> เรซูเม่</h2>
                                                                    <a href="../ResumePage/ResumePage.php" class="btn btn-lg yellow" style="background-color:#ffcc03;border-color: #ffcc03">เรซูเม่</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>