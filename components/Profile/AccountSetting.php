<div class="profile-sidebar">
    <!-- PORTLET MAIN -->
    <div class="portlet light profile-sidebar-portlet ">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
            <img style="height: 150px !important;" src="../../Avatar/<?php echo $row['Avatar']; ?>" class="img-responsive" alt=""> </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-usertitle-name"> <?php echo $row['Username'].'<br>'.$row['Firstname'] .' '.$row['Lastname']; ?> </div>
           <div class="profile-usertitle-job"> <?php if($row['Faculty'] != ''){echo $row['Faculty'] .' : '.$row['Branch'];} ?> </div>
        </div>
        <div class="profile-usermenu">
            <ul class="nav">
                <li >
                    <a href="../Profile/Main.php?Username=<?php echo base64_encode($Username);?>">
                        <i class="fa fa-tachometer"></i> เมนูหลัก </a>
                </li>
                <li >
                    <a href="../Profile/Profile.php?Username=<?php echo base64_encode($Username);?>">
                        <i class="icon-user"></i> ข้อมูลส่วนตัว </a>
                </li>
                <?php if ($Role == 1): ?>
                <li >
                    <a href="../Profile/Resume.php?Username=<?php echo base64_encode($Username);?>">
                        <i class="icon-note"></i> เรซูเม่ </a>
                </li>
                <?php endif ?>
                <li >
                    <a href="../Profile/ChangeAvatar.php?Username=<?php echo base64_encode($Username);?>">
                        <i class="icon-picture"></i> เปลี่ยนรูปโปรไฟล์ </a>
                </li> 
                <li >
                    <a href="../Profile/ChangePassword.php?Username=<?php echo base64_encode($Username);?>">
                        <i class="icon-lock"></i> เปลี่ยนรหัสผ่าน </a>
                </li>
                <?php if ($Role == 1): ?>
                <li >
                    <a href="../Profile/Looking.php?Username=<?php echo base64_encode($Username);?>">
                        <i class="icon-eye"></i> มองหางาน </a>
                </li>
                <li >
                    <a href="../Favorite/Search.php?Username=<?php echo base64_encode($Username);?>">
                        <i class="icon-heart"></i> งานที่สนใจ </a>
                </li>
                <?php endif ?>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
    <!-- END PORTLET MAIN -->
</div>