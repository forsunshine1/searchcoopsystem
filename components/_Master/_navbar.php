<?php 
     include '../../PHP/ConnectDB.php';
    
    if (isset($Role) == '1') {
        $sqlCountFavorite = "SELECT Id FROM favorite WHERE StudentId = ".base64_decode($Id)."";  
        $CountFavorite = mysqli_num_rows(mysqli_query($con, $sqlCountFavorite));
    }
    
    mysqli_close($con);
?>
<style type="text/css">
    .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../../assets/layouts/layout3/img/loading-spinner-blue.gif') 50% 50% no-repeat rgb(249,249,249);
  }
</style>
<script type="text/javascript">
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    })
</script>
<div class="loader"></div>
<!-- BEGIN HEADER -->
    <div class="page-header">
        <!-- BEGIN HEADER TOP -->
        <div class="page-header-top">
            <div class="container">
                <!-- BEGIN LOGO -->
               <div class="page-logo">
                    <a href="index.html">
                        <img src="../../assets/pages/img/login/logosiam.png" alt="logo" class="" style="height: 80px;">
                    </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler"></a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <?php if (isset($State) != 'Login'): ?>
                           <!--  <a class=" btn info" data-toggle="modal" href="#SignIn" style="margin-top: 5%;margin-bottom: 10px">
                                <i class="fa fa-sign-in" style="margin-left: 0px"></i> Sign IN
                            </a>
                            <a class=" btn green" data-toggle="modal" href="#Register" style="margin-top: 5%;margin-bottom: 10px">
                                <i class="fa fa-key" style="margin-left: 0px"></i> Register
                            </a> -->
                        <?php endif ?>
                        <!-- <?php if (isset($State) != 'Login'): ?>
                            <a class=" btn default" href="../Account/Login.php" style="margin-top: 5%;margin-bottom: 10px">
                                <i class="fa fa-sign-in" style="margin-left: 0px"></i> Sign IN
                            </a>
                            <a class=" btn green" href="../Account/AccountCreate.php" style="margin-top: 5%;margin-bottom: 10px">
                                <i class="fa fa-key" style="margin-left: 0px"></i> Register
                            </a>
                        <?php endif ?> -->
                        <?php if (isset($State) == 'Login'): ?>
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <?php if (isset($Avatar) != ''): ?>
                                        <img alt="" height="" width="45px" class="img-circle" src="../../Avatar/<?php echo $Avatar; ?>">
                                    <?php endif ?>
                                    <span class="username username-hide-mobile" style="color: white"><?php echo $FullName; ?></span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="../Profile/Main.php?Username=<?php echo base64_encode($Username);?>">
                                            <i class="fa fa-tachometer  "></i> เมนูหลัก </a>
                                    </li>
                                    <li>
                                        <a href="../Profile/Profile.php?Username=<?php echo base64_encode($Username);?>">
                                            <i class="icon-user"></i> ข้อมูลส่วนตัว </a>
                                    </li>
                                    <?php if ($Role == 1): ?>
                                    <li>
                                        <a href="../Profile/Resume.php?Username=<?php echo base64_encode($Username);?>">
                                        <i class="icon-note"></i> เรซูเม่ </a>
                                    </li>
                                    <?php endif ?>
                                    <li>
                                        <a href="../Profile/ChangeAvatar.php?Username=<?php echo base64_encode($Username);?>">
                                        <i class="icon-picture"></i> เปลี่ยนรูปโปรไฟล์ </a>
                                    </li>
                                    <li>
                                        <a href="../Profile/ChangePassword.php?Username=<?php echo base64_encode($Username);?>">
                                            <i class="icon-lock"></i> เปลี่ยนรหัสผ่าน </a>
                                    </li>
                                    <?php if ($Role == 1): ?>
                                    <li>
                                        <a href="../Profile/Looking.php?Username=<?php echo base64_encode($Username);?>">
                                            <i class="icon-eye"></i> มองหางาน </a>
                                    </li>
                                    <li>
                                        <a href="../Favorite/Search.php?Username=<?php echo base64_encode($Username);?>">
                                            <i class="icon-heart"></i> งานที่สนใจ
                                            <span class="badge badge-success"> <?php echo $CountFavorite; ?> </span>
                                        </a>
                                    </li>    
                                    <?php endif ?>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="../../PHP/logout.php">
                                            <i class="icon-key"></i> ออกจากระบบ </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        <?php endif ?>
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- END HEADER TOP -->
        <!-- BEGIN HEADER MENU -->


        <div class="page-header-menu">
            <div class="container">
                <?php if (isset($State) != 'Login'): ?>
                    <div class="hor-menu " style="float: right">
                       <ul class="nav navbar-nav"> 
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="../Home/Register.php" class="nav-link  ">
                                     ลงทะเบียน
                                </a>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="../Home/Login.php" class="nav-link  ">
                                    เข้าสู่ระบบ
                                </a>
                            </li>
                        </ul>
                    </div>     
                <?php endif ?>
                <div class="hor-menu ">
                   <ul class="nav navbar-nav">
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../Home/index.php"> หน้าหลัก
                            </a>                                            
                        </li>
                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> ค้นหา
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="../Search/Search.php" class="nav-link  "> ค้นหาสถานที่ฝึกสหกิจ </a>
                                </li>
                                <li class=" ">
                                    <a href="../Search/SearchEstablish.php" class="nav-link  "> ค้นหาสถานะประกอบการ </a>
                                </li>
                                <li class=" ">
                                    <a href="../Search/SearchStudent.php" class="nav-link  "> ค้นหานักศึกษา </a>
                                </li>
                                <li class=" ">
                                    <a href="../Search/SearchCoopProject.php" class="nav-link  "> ค้นหาโครงานสหกิจ </a>
                                </li>
                            </ul>
                        </li>   
                        <?php if (isset($Role) != '' && $Role == 2): ?>
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../ToDo/ToDo.php"> 
                                To Do
                            </a>                                            
                        </li>       
                        <?php endif ?>   
                        <?php if (isset($Role) != ''): ?>
                            <?php if ($Role == 0 || $Role == 2 || $Role == 3): ?>
                                <?php if ($Role == 0 || $Role == 2 ): ?>
                                    <li class="menu-dropdown mega-menu-dropdown  mega-menu-full">
                                        <a href="javascript:;"> ข้อมูลหลัก
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu" style="min-width: ">
                                            <li>
                                                <div class="mega-menu-content">
                                                    <div class="row">
                                                        <?php if ($Role == 0): ?>
                                                        <div class="col-md-4">
                                                            <ul class="mega-menu-submenu">
                                                                <li>
                                                                    <h3>มหาวิทยาลัย</h3>
                                                                </li>
                                                                <li>
                                                                    <a href="../Faculty/Search.php">- คณะ </a>
                                                                </li>
                                                                <li>
                                                                    <a href="../Branch/Search.php">- สาขา </a>
                                                                </li>
                                                                <li>
                                                                    <a href="../Year/Search.php">- ปีการศึกษา </a>
                                                                </li>
                                                                <li>
                                                                    <a href="../Term/Search.php">- ภาคการศึกษา </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <?php endif ?>
                                                        <?php if ($Role == 0 || $Role == 2): ?>
                                                        <div class="col-md-4">
                                                            <ul class="mega-menu-submenu">
                                                                <li>
                                                                    <h3>บัญชี</h3>
                                                                </li>
                                                                <li>
                                                                    <a href="../Account/Search.php">- บัญชี </a>
                                                                </li>
                                                                <li>
                                                                    <h3>ประเภท</h3>
                                                                </li>
                                                                <li>
                                                                    <a href="../Category/Search.php">- ประเภทงาน </a>
                                                                </li>
                                                                <li>
                                                                    <h3>ตำแหน่ง</h3>
                                                                </li>
                                                                <li>
                                                                    <a href="../Position/Search.php">- ตำแหน่งงาน </a>
                                                                </li>
                                                            </ul>
                                                        </div>    
                                                        <?php endif ?>
                                                        <?php if ($Role == 0): ?>
                                                        <div class="col-md-4">
                                                            <ul class="mega-menu-submenu">
                                                                 <li>
                                                                    <h3>ประเมิน</h3>
                                                                </li>
                                                                <li>
                                                                    <a href="../Evaluate/Search.php">- ชุดประเมิน </a>
                                                                </li>
                                                                <li>
                                                                    <a href="../Question/Search.php">- หัวข้อประเมิน </a>
                                                                </li> 
                                                                 <li>
                                                                    <h3>เอกสาร</h3>
                                                                </li>
                                                                <li>
                                                                    <a href="../Document/Search.php">- เอกสาร </a>
                                                                </li>                                                 
                                                            </ul>
                                                        </div>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>    
                                <?php endif ?>
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="../Establish/Search.php"> จัดการสถานประกอบการ </a>                                         
                            </li>   
                            <li class="menu-dropdown classic-menu-dropdown">
                                <a href="../Announce/Search.php"> ลงประกาศ </a>          
                            </li>             
                            <?php endif ?>
                        <?php endif ?>                        
                        <?php if (isset($Role) != '' && $Role == 1): ?>
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../Profile/Resume.php?Username=<?php echo base64_encode($Username);?>"> 
                                เรซูเม่
                            </a>                                            
                        </li> 
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../CoopInfo/CoopInfo.php?Username=<?php echo base64_encode($Username);?>"> 
                                ข้อมูลสหกิจ
                            </a>                                            
                        </li>
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../UploadDocumentCoop/ViewUploadDocumentCoop.php?Username=<?php echo base64_encode($Username);?>"> 
                                โครงงาน
                            </a>                                            
                        </li>
                         <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../Evaluation/EvaluationKit.php"> 
                                ประเมิน
                            </a>                                            
                        </li>                       
                        <?php endif ?>          
                        <!-- <li class="menu-dropdown classic-menu-dropdown">
                            <a href="#"> Upload Document Project Coop 
                            </a>                                            
                        </li>  -->
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../Document/FormDocument.php"> ดาวน์โหลดแบบฟอร์ม
                            </a>                                            
                        </li>  
                        <li class="menu-dropdown classic-menu-dropdown">
                            <a href="../Contact/Contact.php?Id=1"> ติดต่อ
                            </a>                                            
                        </li>  
                    </ul>
                </div>                           
                <!-- END MEGA MENU -->
            </div>
        </div>
        <!-- END HEADER MENU -->
    </div>
    <!-- END HEADER --> 

 <!--    <div id="SignIn" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
        <div class="modal-dialog" style="border-radius:5px;width: 380px">
            <div class="modal-content" >
                <div class="modal-header" style="background-color: #ebeeef;border-top-left-radius:2px;border-top-right-radius:2px">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-center"><b>Sign In</b></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="Login">
                        <div class="form-body" style="padding-top: 5%">                                           
                            <div class="form-group">                                               
                                <div class="col-md-12">
                                    <input type="text" name="Username" id="Username" class="form-control" placeholder="Username" required>
                                </div>
                            </div>                                          
                            <div class="form-group">                                               
                                <div class="col-md-12">
                                    <input type="password" name="Password" class="form-control" placeholder="Password">
                                </div>
                            </div> 
                            <div class="md-checkbox md-checkbox-inline">
                                <input type="checkbox" id="checkbox113" class="md-check">
                                <label for="checkbox113">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Remember me </label>
                            </div>
                            <hr>                                         
                            <div class="form-group">                                               
                                <div class="col-sm-12 text-center">
                                    <input type="submit" name="SaveRegister" class="btn yellow-crusta" value="Sign In">
                                </div>
                            </div>
                            <div class="form-group">                                               
                                <div class="col-sm-12 text-center">
                                    <a class="" data-toggle="modal" href="../ForgotPassword/ForgotPassword.php" >
                                        Forgot Password
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">                                               
                                <div class="col-sm-12 text-center">
                                    <a class="" data-toggle="modal" href="#Register">
                                        Register
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>                                
            </div>
        </div>
    </div> -->

    <!-- <div id="Register" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
        <div class="modal-dialog" style="border-radius:5px;width: 380px">
            <div class="modal-content" >
                <div class="modal-header" style="background-color: #ebeeef;border-top-left-radius:2px;border-top-right-radius:2px">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title text-center"><b>Register</b></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="">
                        <div class="form-body" style="padding-top: 5%">                                           
                            <div class="form-group">                                               
                                <div class="col-md-12">
                                    <input type="text" name="StudentCode" id="StudentCode" class="form-control" placeholder="Student Code" required>
                                </div>
                            </div>                            
                            <div class="form-group">                                               
                                <div class="col-md-12">
                                    <input type="password" name="Password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">                                               
                                <div class="col-md-12">
                                    <input type="password" name="ConfirmPassword" class="form-control" placeholder="Confirm Password">
                                </div>
                            </div>
                            <div class="form-group">                                                
                                <div class="col-md-12">
                                    <input type="Email" name="Email" class="form-control" placeholder="Email" required>
                                </div>
                            </div>
                            <hr>                           
                            <div class="form-group">                
                                <div class="col-sm-12 text-center">
                                    <input type="button" name="SaveRegister" class="btn yellow-crusta" value="Sign Up">
                                </div>
                            </div>
                             <div class="form-group">                                               
                                <div class="col-sm-12 text-center">
                                    <p>Register Is Establish <a href="Click">Click</a></p>        
                                </div>
                            </div>
                        </div>
                    </form>
                </div>                                
            </div>
        </div>
    </div> -->