<?php include '../_Master/_header.php'; ?>
<script type="text/javascript">
    document.title = "Master Branch"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>
                        System        
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>System</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12 page-500">
                            <br/> </p>
                            <div class=" number font-red"> Permission Denied </div>
                            <div class="row">
                                <div class=" details">
                                    <p>
                                        <a href="../Home/index.php" class="btn red btn-outline"> Return home </a>
                                        <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>