<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Create':
			Create();
			break;
		case 'Delete':
			Delete();
			break;
		case 'Edit':
			Edit();
			break;	
		default:
			# code...
			break;
	}

	function Create()
	{
		include '../../PHP/ConnectDB.php';

		$Name = $_POST['Name'];
		$Code = $_POST['Code'];
		$IsActive = $_POST['IsActive'];

		if(isset($_POST['Name']))
		{
			$valid = "SELECT * FROM faculty WHERE Code = '$Code'";

			$validNum = mysqli_num_rows(mysqli_query($con, $valid));
			 
			if($validNum == ''){
				$sql = "INSERT INTO faculty (Code,Name,IsActive) VALUES ('$Code','$Name','$IsActive')";

				mysqli_query($con, $sql);
				$response_array['status'] = 'success';  

			}else{
				
				$response_array['status'] = 'duplicate';
			}							

		}else{
		    
		    $response_array['status'] = 'error';
		} 
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$Code = $_POST['Code'];
		$Name = $_POST['Name'];
		$IsActive = $_POST['IsActive'];

		if (isset($_POST['Id'])) {

			$Id=$_POST['Id'];
			$valid = "SELECT * FROM faculty WHERE Code = '$Code'";

			$validNum = mysqli_num_rows(mysqli_query($con, $valid));

			$query = "SELECT * FROM faculty WHERE Code = '$Code'";
			$row = mysqli_fetch_array(mysqli_query($con, $query));

			if($validNum == '' || $row['Id'] == $Id){
					
				$sql = mysqli_query($con,"UPDATE faculty SET Name = '$Name' , IsActive = '$IsActive' 
				WHERE Id='$Id'");

				$response_array['status'] = 'success';  

			}else{
				
				$response_array['status'] = 'duplicate';
			}		

		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Delete()
	{
		include '../../PHP/ConnectDB.php';
		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = "DELETE FROM faculty WHERE Id='$Id'";

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	exit(json_encode($response_array));
 ?>