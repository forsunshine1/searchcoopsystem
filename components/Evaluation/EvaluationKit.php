<?php include '../_Master/_header.php'; ?>
<?php
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';

    $sql = "SELECT * FROM evaluate WHERE IsActive = '1'";  
    $result = mysqli_query($con,$sql); 
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<style type="text/css">
    .table>tbody>tr>td{
        line-height: 3 !important;
    }
</style>
<script type="text/javascript">
    document.title = "แบบประเมิน"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                แบบประเมิน
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>แบบประเมิน</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE TABLE INNER -->
                        <div class="page-content-inner">
                            <div class="portlet light bordered">
                                <div class="portlet-body">                                    
                                    <table class="table table-striped" >  
                                        <thead>
                                            <tr>
                                                <th>แบบประเมิน</th>
                                                <th>รายละเอียด</th>
                                                <th>สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  $i = 1;
                                                while ($row = mysqli_fetch_assoc($result)) {  
                                            ?>
                                           <tr>
                                                <td><?php echo $row["Kit"]; ?></td>  
                                                <td><a href="./Evaluation.php?Id=<?php echo $row["Id"]; ?>"><?php echo $row["Description"]; ?></a></td>
                                                <td><?php echo $row["IsActive"] == 1 ? 'เปิดให้ประเมิน' : 'ปิดให้ประเมิน'; ?></td>                                
                                            </tr>
                                            <?php  
                                                };  
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE TABLE INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>