<?php 
    include '../_Master/_header.php'; 
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];

        $sql = "SELECT * FROM question WHERE KitId = '$Id'";

        $result = mysqli_query($con,$sql); 
        // $result = mysqli_query($con,$sql); 
        
        
    }
    
    mysqli_close($con);
?>
<script type="text/javascript">
    document.title = "ประเมิน"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                ประเมิน         
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                             <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>ประเมิน </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">ประเมิน</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="form-horizontal" action="./Save.php" method="POST">
                                            <?php $i = 1;
                                                while ($row = mysqli_fetch_assoc($result)) {  
                                            ?>
                                                <input type="hidden" name="QDesc<?php echo $i; ?>" value="<?php echo $row['Description']; ?>">
                                                <input type="hidden" name="QId<?php echo $i; ?>" value="<?php echo $row['Id']; ?>">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <td>แบบประเมิน</td>
                                                            <td>มากที่สุด</td>
                                                            <td>มาก</td>
                                                            <td>พอใช้</td>
                                                            <td>น้อย</td>
                                                            <td>น้อยที่สุด</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php echo $row['Description']; ?></td>
                                                            <td>
                                                                <input type="radio" value="1" name="Chk<?php echo $i; ?>" id="radio6" class="md-radiobtn" checked> 
                                                            </td>
                                                            <td>
                                                                <input type="radio" value="2" name="Chk<?php echo $i; ?>" id="radio6" class="md-radiobtn">
                                                            </td>
                                                            <td>
                                                                <input type="radio" value="3" name="Chk<?php echo $i; ?>" id="radio6"  class="md-radiobtn">
                                                            </td>
                                                            <td>
                                                                <input type="radio" value="4" name="Chk<?php echo $i; ?>" id="radio6" class="md-radiobtn">
                                                            </td>
                                                            <td>
                                                                <input type="radio" value="5" name="Chk<?php echo $i; ?>" id="radio6" class="md-radiobtn">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <?php  
                                                $i++;};  
                                            ?>
                                             <input type="hidden" name="KitId" value="<?php echo $_GET['Id']; ?>">
                                             <input type="hidden" name="QLength" value="<?php echo $i-1; ?>">
                                             <input type="hidden" name="StudentId" value="<?php echo $_SESSION['login_user']; ?>">
                                            <div class="form-group">
                                                <div class="col-md-4 col-md-offset-4 text-center">
                                                    <input type="submit" class="btn green" value="ส่งแบบประเมิณ">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>