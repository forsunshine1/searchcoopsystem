<?php 
    include '../_Master/_header.php'; 
    if(isset($Role)){
        if ($Role != 0) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Home/Index.php';</script>";  
    }
    include '../../PHP/ConnectDB.php';
    if (isset($_GET['Id'])) {

        $Id = $_GET['Id'];
        $query = mysqli_query($con,"SELECT * FROM category WHERE Id = '$Id' ");
        
        // $result = mysqli_query($con,$sql); 
        $row=mysqli_fetch_array($query,MYSQLI_ASSOC);
        
    }
    
    mysqli_close($con);
?>
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: $(this).serialize(),
                success: function (response) {
                     if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลสำเร็จ';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", window.location);
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'บันทึกข้อมูลไม่สำเร็จ';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'มีข้อมูลแล้ว';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }              
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "แก้ไขข้อมูลประเภทงาน"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                จัดกาข้อมูลประเภทงาน         
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="../Home/index.php">หน้าหลัก</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="./Search.php">จัดกาข้อมูลประเภทงาน</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>แก้ไขข้อมูลประเภทงาน </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">แก้ไขข้อมูล</span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                               <div class="form-group">
                                                    <label class="col-md-3 control-label">ชื่อ</label>
                                                    <div class="col-md-4">
                                                        <input type="hidden" name="func" class="form-control" value="Edit">
                                                        <input type="hidden" value="<?php echo $row['Id'] ?>" name="Id">
                                                        <input type="text" id="Name" name="Name" class="form-control" placeholder="Name" value="<?php if(isset($_GET['Id']) != ''){echo $row['Name']; }?>" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">สถานะการใช้งาน</label>
                                                    <div class="col-md-4">
                                                        <div class="md-radio-inline">
                                                            <div class="md-radio">
                                                                <input type="radio" value="1" name="IsActive" id="radio6" name="radio2" class="md-radiobtn" <?php if(isset($_GET['Id']) != ''){if($row['IsActive'] == true){echo "checked";} }?> >
                                                                <label for="radio6">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ใช้งาน </label>
                                                            </div>
                                                            <div class="md-radio">
                                                                <input type="radio" value="0" name="IsActive" id="radio7" name="radio2" class="md-radiobtn" <?php if(isset($_GET['Id']) != ''){if($row['IsActive'] == false){echo "checked";} }?>>
                                                                <label for="radio7">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> ไม่ใช้งาน </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">บันทึก</button>
                                                        <a type="button" href="Search.php" class="btn default">กลับ</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>