<?php include '../_Master/_header.php'; ?>
<?php
    if(isset($Role)){
        if ($Role != 0) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Home/Index.php';</script>";  
    }
    //header('Content-type: application/json; charset=utf-8');
    include '../../PHP/ConnectDB.php';

    //Search
    if ( (isset($_GET['BranchId']) != '' || isset($_GET['Name']) != '' || isset($_GET['Role']) != '')) {
        if ($_GET['BranchId'] != '-1') {
            $BranchId = "accounts.BranchId LIKE'%" . $_GET['BranchId'] . "%'";
        }else{
            $BranchId ="accounts.BranchId IS NOT NULL";
        }
        if ($_GET['Name'] != "") {
            $Name = "accounts.Firstname LIKE'%" . $_GET['Name'] . "%'";
        }
        else{
            $Name = "accounts.Firstname IS NOT NULL";
        }
        if($_GET['Role'] != '-1') {
            $Role = "accounts.Role LIKE '%" . $_GET['Role'] . "%'";
        }else{
            $Role = "accounts.Role IS NOT NULL";
        }
        
        $sql = "SELECT accounts.Id,accounts.Username,accounts.Firstname,accounts.Lastname,faculty.Name As Faculty,branch.Name As Branch,accounts.Created,accounts.tel,accounts.Email,accounts.Fax , accounts.Address , accounts.gpa  ,accounts.Role ,accounts.BranchId
             FROM accounts LEFT JOIN branch ON accounts.BranchId = branch.Id LEFT JOIN faculty ON branch.FacultyId = faculty.Id  WHERE $Name AND $BranchId AND $Role";  
        $result = mysqli_query($con,$sql); 
    }else{
       $sql = "SELECT accounts.Id,accounts.Username,accounts.Firstname,accounts.Lastname,faculty.Name As Faculty,branch.Name As Branch,accounts.Created,accounts.tel,accounts.Email,accounts.Fax , accounts.Address , accounts.gpa  ,accounts.Role ,accounts.BranchId
             FROM accounts LEFT JOIN branch ON accounts.BranchId = branch.Id LEFT JOIN faculty ON branch.FacultyId = faculty.Id";  
        $result = mysqli_query($con,$sql); 
    }   
    
    mysqli_close($con);
    //exit(json_encode($response_array));
?>
<script type="text/javascript">
    document.title = "Master Account"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                Account     
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Account</span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                         <!-- BEGIN PAGE SEARCH INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-search font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Search</span>
                                        </div>
                                    <div class="actions">
                                        <a class="btn yellow" href="Create.php">Create</a>
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT Branch.Id ,branch.Name ,faculty.Name as facultyName FROM faculty INNER JOIN Branch ON Branch.FacultyId = faculty.Id order by FacultyName ASC ";  
                                                    $listbranch = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">Faculty/Branch</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="BranchId">
                                                            <option value='-1'>Please Select</option>
                                                            <?php while ($row2=mysqli_fetch_assoc($listbranch)) { ?>
                                                                <option value=<?php echo $row2['Id']?>><?php echo $row2['facultyName'].' '.$row2['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Name" class="form-control" placeholder="Name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">Role</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="Role">
                                                            <option value='-1'>Please Select</option>
                                                            <option value='0'>Admin</option>
                                                            <option value='1'>Student</option>
                                                            <option value='2'>Teacher</option>
                                                            <option value='3'>Establish</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn blue">Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                           
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE SEARCH INNER -->
                        <!-- BEGIN PAGE TABLE INNER -->
                        <div class="page-content-inner">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-table font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">Results</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th> Username </th>
                                                <th> Firstname </th>
                                                <th> Lastname </th>
                                                <th> Faculty </th>
                                                <th> Branch  </th>
                                                <th> Role </th>
                                                <th> Created Date </th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php  $i = 1;
                                                while ($row = mysqli_fetch_assoc($result)) {  
                                            ?>
                                            <tr>
                                                <th><?php echo $i; $i++; ?></th>  
                                                <td><?php echo $row["Username"]; ?></td> 
                                                <td><?php echo $row["Firstname"]; ?></td> 
                                                <td><?php echo $row["Lastname"];?></td>
                                                <td><?php echo $row["Faculty"];?></td>
                                                <td><?php echo $row["Branch"];?></td>
                                                <td><?php switch ($row["Role"]) {
                                                    case '0':
                                                        echo "Admin";
                                                        break;
                                                    case '1':
                                                        echo "Student";
                                                        break;
                                                    case '2':
                                                        echo "Teacher";
                                                        break;
                                                    case '3':
                                                        echo "Establish";
                                                        break;
                                                } ?></td>
                                                <td><?php echo $row["Created"];?></td>  
                                                <td>
                                                    <?php echo '<a class="btn-xs btn  green"  href="View.php?Id='. $row['Id'] .'" >View</a>'; ?>
                                                    <?php echo '<a class="btn-xs btn  red" data-toggle="modal" data-id="'. $row['Id'] .'" href="#myModal'. $row['Id'] .'"  >Delete</a></td>'; ?>
                                                </td>                         
                                                <!-- Modal Delete-->
                                                <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['Id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog modal-sm" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header" style="background-color: #f9243f;">
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                          <h4 style="color:#fff;" class="modal-title" id="myModalLabel">Alert</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                          Delete <?php echo $row['Firstname'].' '.$row['Lastname']; ?>
                                                        </div>
                                                        <div class="modal-footer">
                                                          <a class="btn red" href="./Controller.php?Id=<?php echo $row['Id']?>&func=Delete">OK</a>
                                                          <a class="btn default" data-dismiss="modal">Cancel</a>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <!-- Modal Delete-->
                                                </div>
                                                <!-- Modal Delete-->                     
                                            </tr>
                                            <?php  
                                                };  
                                            ?>   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE TABLE INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>