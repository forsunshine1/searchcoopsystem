<?php 
    include '../_Master/_header.php';
    if(isset($Role)){
        if ($Role != 0) {
            echo "<script type='text/javascript'>window.location.href = '../PermissionDenied/PermissionDenied.php';</script>";  
        }
    }else{
        echo "<script type='text/javascript'>window.location.href = '../Home/Index.php';</script>";  
    } 
?>
<!-- POST  -->
<script type="text/javascript">
    $(function () {
        $('#addForm').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: './Controller.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false, 
                success: function (response) {
                    if(response.status == 'success'){
                        document.getElementById("alertMessage").innerHTML = 'Save success';
                        $('#Link').attr("data-dismiss",""); 
                        $('#Link').attr("href", "./Search.php")
                        $('#myModalAlert').modal('show');                
                    }else if(response.status == 'error'){
                        document.getElementById("alertMessage").innerHTML = 'Save failed';
                        $('#myModalAlert').modal('show'); 
                        $('#Link').attr("data-dismiss","modal");                           
                    }else if(response.status == 'duplicate'){
                        document.getElementById("alertMessage").innerHTML = 'Duplicate';
                        $('#myModalAlert').modal('show');
                        $('#Link').attr("data-dismiss","modal");                      
                    }          
                }
                
            });
        
        });
    });
</script>
<script type="text/javascript">
    document.title = "Create Account"
</script>
<div class="page-wrapper-row full-height">
    <div class="page-wrapper-middle">
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE HEAD-->
                <div class="page-head">
                    <div class="container">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                                Account        
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                </div>
                <!-- END PAGE HEAD-->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                     <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Account </span>
                            </li>
                        </ul>
                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE VIEWEDIT INNER -->
                        <div class="page-content-inner">
                            <div class="mt-content-body">
                                <div class="portlet light bordered" id="addPanel" >
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-plus font-red-sunglo"></i>
                                            <span class="caption-subject font-red-sunglo bold uppercase">Create </span>
                                        </div>
                                    <div class="actions">
                                    </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form class="form-horizontal" id="addForm">
                                            <div class="form-body">
                                                <?php 
                                                    include '../../PHP/ConnectDB.php';

                                                    $sql = "SELECT Branch.Id ,branch.Name ,faculty.Name as facultyName FROM faculty INNER JOIN Branch ON Branch.FacultyId = faculty.Id order by FacultyName ASC ";  
                                                    $listbranch = mysqli_query($con,$sql); 
                                                    
                                                    mysqli_close($con);
                                                ?>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">Faculty/Branch</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="BranchId">
                                                            <option value=' '>Please Select</option>
                                                            <?php while ($row2=mysqli_fetch_assoc($listbranch)) { ?>
                                                                <option value=<?php echo $row2['Id']?>><?php echo $row2['facultyName'].' '.$row2['Name']?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Username</label>
                                                    <div class="col-md-4">
                                                        <input type="hidden" name="func" class="form-control" value="Create">
                                                        <input type="text" name="Username" class="form-control" placeholder="Username" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Password</label>
                                                    <div class="col-md-4">
                                                        <input type="password" name="Password" class="form-control" placeholder="Password" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">First Name</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Firstname" class="form-control" placeholder=" First Name" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Last Name</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Lastname" class="form-control" placeholder="Last Name" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Gender</label>
                                                    <div class="col-md-4 md-radio-list">
                                                        <div class="">
                                                            <input type="radio" value="0" id="radio1" name="Gender" class="md-radiobtn" checked>
                                                            <label for="radio1">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> Male </label>
                                                        </div>
                                                        <div class="">
                                                            <input type="radio" value="1" id="radio2" name="Gender" class="md-radiobtn" >
                                                            <label for="radio2">
                                                                <span></span>
                                                                <span class="check"></span>
                                                                <span class="box"></span> Female </label>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Address</label>
                                                    <div class="col-md-4">
                                                        <textarea type="text" name="Address" rows="4" class="form-control" placeholder="Address" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Email</label>
                                                    <div class="col-md-4">
                                                        <input type="email" name="Email" class="form-control" placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Tel.</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Tel" class="form-control" placeholder="Tel.">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Fax.</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Fax" class="form-control" placeholder="Fax.">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">GPA</label>
                                                    <div class="col-md-4">
                                                        <input type="text" name="Gpa" class="form-control" placeholder="GPA">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Image Upload </label>
                                                    <div class="col-md-4">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                                            </div>
                                                            <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="img_member"> </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove 
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="single"  class="col-md-3 control-label">Role</label>
                                                    <div class="col-md-4">
                                                        <select id="single" class="form-control select2" name="Role">
                                                            <option value='0'>Admin</option>
                                                            <option value='1'>Student</option>
                                                            <option value='2'>Teacher</option>
                                                            <option value='3'>Establish</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">Save</button>
                                                        <a type="button" href="Search.php" class="btn default">Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE VIEWEDIT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
    </div>
</div>
<?php include '../_Master/_footer.php'; ?>