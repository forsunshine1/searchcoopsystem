<?php 
	header('Content-type: application/json; charset=utf-8');
		
	if (isset($_POST['func']) != '') {
		$func = $_POST['func'];
	}elseif (isset($_GET['func']) != '') {
		$func = $_GET['func'];
	}

	switch ($func) {
		case 'Create':
			Create();
			break;
		case 'Delete':
			Delete();
			break;
		case 'Edit':
			Edit();
			break;	
		default:
			# code...
			break;
	}

	function Create()
	{
		include '../../PHP/ConnectDB.php';
		$Username = $_POST['Username'];
		if (isset($_POST['Password'])) {
			$Password=base64_encode($_POST['Password']);
		}	
		$Firstname = $_POST['Firstname'];
		$Lastname = $_POST['Lastname'];
		$BranchId = $_POST['BranchId'];
		$Gender = $_POST['Gender'];
		if (isset($_POST['Address'])) {
			$Address = $_POST['Address'];
		}
		if (isset($_POST['Gpa'])) {
			$Gpa = $_POST['Gpa'];
		}
		$Email = $_POST['Email'];
		$Tel = $_POST['Tel'];
		$Fax = $_POST['Fax'];
		
		if (isset($_FILES["img_member"])) {
			$temp = explode(".", $_FILES["img_member"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			move_uploaded_file($_FILES["img_member"]["tmp_name"], "../../Avatar/" . $newfilename);
			$imgpath = $newfilename ;

		}else{
			$imgpath = "";
		}
		$Role = $_POST['Role'];

		if(isset($_POST['Username']))
		{
			$valid = "SELECT * FROM accounts WHERE Username = '$Username' ";

			$validNum = mysqli_num_rows(mysqli_query($con, $valid));
			 
			if($validNum == ''){
				$sql = "INSERT INTO accounts (Username,Password,Firstname,Lastname,BranchId,Gender,Address,Email,Tel,Fax,Gpa,Avatar,Role) VALUES ('$Username','$Password','$Firstname','$Lastname','$BranchId','$Gender','$Address','$Email','$Tel','$Fax','$Gpa','$imgpath','$Role')";
				mysqli_query($con, $sql);
				$response_array['status'] = 'success';  

			}else{
				
				$response_array['status'] = 'duplicate';
			}							

		}else{
		    
		    $response_array['status'] = 'error';
		} 
		mysqli_close($con);
		exit(json_encode($response_array));
	}

	function Edit()
	{
		include '../../PHP/ConnectDB.php';
		$KitId = $_POST['KitId'];
		$No = $_POST['No'];
		$Description = $_POST['Description'];
		$IsActive = $_POST['IsActive'];
		//UPDATE
		if (isset($_POST['Id'])) {

			$Id = $_POST['Id'];
			$valid = "SELECT * FROM question WHERE KitId = '$KitId' AND No= '$No' ";

			$validNum = mysqli_num_rows(mysqli_query($con, $valid));

			$query = "SELECT * FROM question WHERE KitId='$KitId' AND No = '$No'";
			$row = mysqli_fetch_array(mysqli_query($con, $query));

			if($validNum == '' || $row['Id'] == $Id){
				
				// $sql = "INSERT INTO branch (FacultyId,BranchCode,BranchName,BranchDesc) VALUES ('$FacultyId','$BranchCode','$BranchName','$BranchDesc')";
				$sql = mysqli_query($con,"UPDATE question SET KitId = '$KitId',No = '$No',Description = '$Description' , IsActive = '$IsActive' WHERE Id='$Id'");
				$response_array['status'] = 'success';  

			}else{
				
				$response_array['status'] = 'duplicate';
			}		

		}else{
			$response_array['status'] = 'error';
		}

		mysqli_close($con);
		exit(json_encode($response_array));
	}


	function Delete()
	{
		include '../../PHP/ConnectDB.php';
		if(isset($_GET['Id']))
		{
			$Id = $_GET['Id'];
			
			$sql = "DELETE FROM accounts WHERE Id='$Id'";

			mysqli_query($con, $sql);	

		}else{
		    echo "ERROR: Could not able to execute $sql. " . mysqli_error($con);
		} 

		mysqli_close($con);
		header('Location: ./Search.php');
	}

	exit(json_encode($response_array));
 ?>